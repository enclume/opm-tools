// return a string of the given digit of a given length (add "0" to reach the given length)
// if the given digit is greather thant the given length, will do nothing (just return the given digit as a string)
function int_to_string(digit, str_length)
{
	digit = digit.toString();

	while (digit.length < str_length) {
		digit = "0" + digit;
	}
	
	return digit;
}

// return True if the given string is a number, else return false
function is_digit(str)
{	
	if (isNaN(Number(str))) {return false;}
	return true;
}