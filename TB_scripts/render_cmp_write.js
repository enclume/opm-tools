/*
	Use the function "start_cmp_render" to launch the render write node dialogue (with confo_CMP_write_nodes and disable_nonCMP_write_node).
	author : Guillaume Geelen
	version : 1.4
*/

function get_CMP_write_node()
{
	var w_nodes = node.getNodes(["WRITE"]);
	var cmp_nodes = [];
	for (var i = 0 ; i < w_nodes.length ; i++) {
		var w_node = w_nodes[i];
		var w_name = node.getName(w_node);
		if (w_name.search("COMPO") == -1) {continue;}
		cmp_nodes.push(w_node);
	}
	return cmp_nodes;
}


function get_config()
{
	var config_path = "O:/_SCRIPTS/opm-tools/TB_scripts/config.json";
	var configfile = new File(config_path);
	configfile.open(FileAccess.ReadOnly);
	var content = configfile.read();
	configfile.close();
	
	try {
  		content = JSON.parse(content);
	}
	catch(err) {
		error = "Error when trying to JSON parse config.json\n -> " + err;
		MessageLog.trace(error);
 		MessageBox.critical(error);
		return NaN;
	}
	return content;
}


function set_node_attr(attrnode, config)
{
	for (var a = 0 ; a < Object.keys(config).length ; a++) {
		var attr_name = Object.keys(config)[a];
		var attr_value = config[attr_name][1];
		
		if (attr_value.toString().indexOf("{version_name}") != -1) {
			attr_value = attr_value.replace("{version_name}", scene.currentVersionName())
		}

		var attr = node.getAttr(attrnode, 1, attr_name);
		attr.setValue(attr_value);
	}
}


function confo_CMP_write_nodes()
{	
	scene.beginUndoRedoAccum("confo_CMP_write_nodes");
	var cmp_nodes = get_CMP_write_node();
	var config = get_config();
	for (var i = 0 ; i < cmp_nodes.length ; i++) {
		var cmp_node = cmp_nodes[i];
		var n_name = node.getName(cmp_node);

		if (n_name.search("COMPO") == -1) {continue;}

		if (n_name.search("W")!= -1 || n_name.search("CHECK")!= -1) {
			node.rename(cmp_node, "COMPO_CHECK");
			set_node_attr(cmp_node, config["COMPO_CHECK"]);
		}

		if (n_name.search("DEF")!= -1) {
			node.rename(cmp_node, "COMPO_DEF");
			set_node_attr(cmp_node, config["COMPO_DEF"]);
		}
	}
	scene.endUndoRedoAccum();
}


function disable_nonCMP_write_node()
{	
	scene.beginUndoRedoAccum("disable_nonCMP_write_node");
	var w_nodes = node.getNodes(["WRITE"]);

	for (var i = 0 ; i < w_nodes.length ; i++) {
		var w_node = w_nodes[i];
		var w_name = node.getName(w_node);
		if (w_name.search("COMPO") == -1) {
			node.setEnable(w_node, false);
		} else {
			node.setEnable(w_node, true);
		}
	}

	MessageLog.trace("COMP write nodes enabled, other write nodes disabled");
	scene.endUndoRedoAccum();
}


function checkSceneDialog(report)
{
	var checkSceneD = new Dialog();
	checkSceneD.title = "Warning";
	var bodyText = new Label();
	bodyText.text = report;
	checkSceneD.add(bodyText);
	checkSceneD.cancelButtonText = "Render anyway";
	checkSceneD.okButtonText = "Do not render";
	return !checkSceneD.exec(); //return True if render anyway
}


function check_scene_params()
{
	var config = get_config()["scene_check"];
	var fps = scene.getFrameRate();
	var res_x = scene.currentResolutionX();
	var res_y = scene.currentResolutionY();
	var report = [];
	
	if (fps != config["fps"]) {
		report.push("scene fps = " + fps + " (schould be \'" + config["fps"] + "\')");
	}
	if (res_x != config["res_x"]) {
		report.push("scene x resolution = " + res_x + " (schould be \'" + config["res_x"] + "\')");
	}
	if (res_y != config["res_y"]) {
		report.push("scene y resolution = " + res_y + " (schould be \'" + config["res_y"] + "\')");
	}

	report = report.join("\n");
	
	if (report != "") {
		report = "Scene check bad repport :\n\n" + report;
		report += "\n(Remet tes settings gamin !)";
		return checkSceneDialog(report);
	}
	return true;
}


function start_cmp_render()
{
	scene.setColorSpace("sRGB");
	disable_nonCMP_write_node();
	confo_CMP_write_nodes();
	var render = check_scene_params();
	if (!render) {
		MessageLog.trace("render canceled");
		return;
	}
	MessageLog.trace("start cmp render");
	Action.perform("onActionComposite()");
}