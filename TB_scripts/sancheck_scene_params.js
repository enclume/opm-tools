//
//This is a sandbox area where you can write scripts
// that can be run but never saved.
// Press run to execute the script in the editor or
// the selected part if a selection was made.
// Note: if the sandbox code is a complete function,
//  the function must be called in order to execute.

function get_config()
{
	var config_path = "O:/_SCRIPTS/opm-tools/TB_scripts/config.json";
	var configfile = new File(config_path);
	configfile.open(FileAccess.ReadOnly);
	var content = configfile.read();
	configfile.close();
	
	try {
  		content = JSON.parse(content);
	}
	catch(err) {
		error = "Error when trying to JSON parse config.json\n -> " + err;
		MessageLog.trace(error);
 		MessageBox.critical(error);
		return NaN;
	}
	return content;
}


function check_scene_params()
{
	scene.setColorSpace("sRGB");
	var config = get_config()["scene_check"];
	var fps = scene.getFrameRate();
	var res_x = scene.currentResolutionX();
	var res_y = scene.currentResolutionY();
	var report = [];
	
	if (fps != config["fps"]) {
		report.push("scene fps = " + fps + " (schould be \'" + config["fps"] + "\')");
	}
	if (res_x != config["res_x"]) {
		report.push("scene x resolution = " + res_x + " (schould be \'" + config["res_x"] + "\')");
	}
	if (res_y != config["res_y"]) {
		report.push("scene y resolution = " + res_y + " (schould be \'" + config["res_y"] + "\')");
	}

	report = report.join("\n");

	if (report != "") {
		report = "Scene check bad repport :\n\n" + report;
		MessageBox.warning(report);
	}
}