/*
	Use the function "sidonie" to disable all "lambique" nodes and to enable all "ombre-compo/lambique" nodes
	author : Guillaume Geelen
	version : 1.0
*/

function get_lambique_nodes(p_node)
{
	var c_nodes = node.subNodes(p_node);
	var node_list = [];

	if (c_nodes.length == 0) {return node_list;}
	
	for (var i = 0 ; i < c_nodes.length ; i++) {
		var c_node = c_nodes[i];
		
		if (node.getName(c_node) == "lambique") {
			node_list.push(c_node);
			continue;
		}	

		if (node.type(c_node) == "GROUP") {
			node_list = node_list.concat(get_lambique_nodes(c_node));
		}
	}
	return node_list;
}

function sidonie()
{	
	scene.beginUndoRedoAccum("sidonie");
	var nds = get_lambique_nodes(node.root());
	//MessageLog.trace(nds);

	for (var i = 0 ; i < nds.length ; i++) {
		var parentN = node.parentNode(nds[i]);
		node.setEnable(nds[i], false);
		if (node.getName(parentN) == "ombre-compo") {node.setEnable(nds[i], true);}
	}

	scene.endUndoRedoAccum();
}