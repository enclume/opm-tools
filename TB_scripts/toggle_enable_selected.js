/*
	Use the function "toggle_enable_selected" to toggle enable individually on each selected nodes.
	author : Guillaume Geelen
	version : 1.0
*/

function toggle_enable_selected()
{
	var selNodes = selection.selectedNodes();

	for (var i = 0 ; i < selNodes.length ; i++) {
		var selNode = selNodes[i];
		node.setEnable(selNode, !node.getEnable(selNode));
	}
}