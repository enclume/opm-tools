//
//This is a sandbox area where you can write scripts
// that can be run but never saved.
// Press run to execute the script in the editor or
// the selected part if a selection was made.
// Note: if the sandbox code is a complete function,
//  the function must be called in order to execute.


function get_prcmp_path() {
	var path = node.add(node.root(), "precomp", "WRITE", 0, 0, 0);
	MessageLog.trace(path);
	node.deleteNode(path);
	return path;
}


function get_cmp() {
	var wnodes = node.getNodes(["WRITE"]);
	
	for (var i = 0; i < wnodes.length; i++) {
		if (node.getName(wnodes[i])=="COMPO_DEF") {
			MessageLog.trace(wnodes[i]);
			return wnodes[i];
		}
	}
	return false;
}


function add_prcmp_write() {
	scene.beginUndoRedoAccum("add_prcmp_write");

	var cmp = get_cmp();
	if (cmp == false) {
		MessageLog.trace("can't find COMPO_DEF node, abort import add_prcmp_write");
		return;
	}
	var source = node.srcNode(cmp, 0);
	var group = node.parentNode(cmp);
	var prcmp = get_prcmp_path();
	var srcPath = "K:/_LIBRARY/pre comp/_export.tpl";
	copyPaste.pasteTemplateIntoGroup(srcPath, node.root(), 0);
	node.setCoord(prcmp, (node.coordX(cmp)+node.width(cmp)+10), node.coordY(cmp));
	node.link(source, 0, prcmp, 0);

	scene.endUndoRedoAccum();
}


MessageLog.trace("___start");

add_prcmp_write();

MessageLog.trace("[JOB_INFO]_[PROGRESS]_[START]_["+scene.getStartFrame()+"]")
MessageLog.trace("[JOB_INFO]_[PROGRESS]_[END]_["+scene.getStopFrame()+"]")

MessageLog.trace("___end\n");


