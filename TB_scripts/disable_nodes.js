/*
	Use the function "disable_nodes" to disables all nodes whose name is given in the config.json file
    config.json -> O:\_SCRIPTS\opm-tools\TB_scripts\config.json
	author : Guillaume Geelen
	version : 1.1
*/

function get_nodes(p_node, names)
{
	var c_nodes = node.subNodes(p_node);
	var node_list = [];

	if (c_nodes.length == 0) {return node_list;}
	
	for (var i = 0 ; i < c_nodes.length ; i++) {
		var c_node = c_nodes[i];
		
		if (names.indexOf(node.getName(c_node)) != -1) {
			node_list.push(c_node);
			continue;
		}	

		if (node.type(c_node) == "GROUP") {
			node_list = node_list.concat(get_nodes(c_node, names));
		}
	}
	return node_list;
}


function get_config()
{
	var config_path = "O:/_SCRIPTS/opm-tools/TB_scripts/config.json";
	var configfile = new File(config_path);
	configfile.open(FileAccess.ReadOnly);
	var content = configfile.read();
	configfile.close();
	
	try {
  		content = JSON.parse(content);
	}
	catch(err) {
		error = "Error when trying to JSON parse config.json\n -> " + err;
		MessageLog.trace(error);
 		MessageBox.critical(error);
		return NaN;
	}
	return content;
}


function isToAvoid(name, avoidList)
{
	for (var i = 0 ; i < avoidList.length ; i++) {
		if (name.indexOf(avoidList[i]) != -1) {return true;}
	}
	return false;
}


function disable_nodes()
{	
	scene.beginUndoRedoAccum("disable_nodes");
	var config = get_config();	
	var to_disable = config["nodes_to_disable"];
	var to_keep = config["nodes_to_keep"]

	var nds = get_nodes(node.root(), to_disable);
	
	for (var i = 0 ; i < nds.length ; i++) {
		var nodeName = node.getName(nds[i]);
		if (isToAvoid(nodeName, to_keep)) {continue;}
		node.setEnable(nds[i], false);
	}

	var info = "nodes disabled :\n" + to_disable.join(", ");
	MessageLog.trace(info);
	MessageBox.information(info);
	scene.endUndoRedoAccum();
}