/*
	Use the function "save_scene_new_version" to save a new version (incrementally) of the current scene.
	author : Guillaume Geelen
	version : 1.5
*/


function int_to_string(digit, str_length)
{
	digit = digit.toString();

	while (digit.length < str_length) {
		digit = "0" + digit;
	}
	
	return digit;
}


function get_basic_scene_name(ep, sh, after)
{
	if (after == undefined) {after = "";}
	ep = int_to_string(ep, 3);
	sh = int_to_string(sh, 3);

	var final_string = ["OPM", ep, "SH"+sh];
	return final_string.join("_") + after;
}


function get_full_scene_name(ep, sh, version, after)
{
	version = int_to_string(version, 2);

	var final_string = get_basic_scene_name(ep, sh, after);
	final_string = [final_string, "CMP", "V"+version];
	return final_string.join("_");
}


function is_digit(str)
{	
	if (isNaN(Number(str))) {return false;}
	return true;
}


function get_after(name, data) 
{
	//data : [ep, sh, v, after] -> all string
	var after = name;

	if (after.indexOf("_SH") == -1) {return "";}
	after = after.split("_SH"+data[1]).reverse()[0];

	if (after.indexOf("_CMP") != -1) {
		after = after.split("_CMP")[0];
		return after;
	}
	
	if (after.indexOf(".") == -1) {return after;}

	after = after.split(".")[0];
	return after;
}


function get_scene_name_data(name)
{
	var after = name;

	name = name.split("");

	var data = ["", "", "", ""]; //ep, sh, v, after
	
	var in_digit = false;
	var pointer = 0;

	for (var i = 0 ; i < name.length; i++) {
		var letter = name[i];
		if (is_digit(letter)) {
			if (in_digit == false) {in_digit = true;}
			data[pointer] += letter;
		} else {
			if (in_digit == true) {
				in_digit = false;
				pointer += 1;
				if (pointer > 2) {break;}
			}
		}
	}

	after = get_after(after, data);
	
	for (var i = 0 ; i < data.length; i++) {
		data[i] = Number(data[i]);
	}
	data[3] = after;
	return data;
}


function get_CMP_write_node()
{
	var w_nodes = node.getNodes(["WRITE"]);
	var cmp_nodes = [];
	for (var i = 0 ; i < w_nodes.length ; i++) {
		var w_node = w_nodes[i];
		var w_name = node.getName(w_node);
		if (w_name.search("COMPO") == -1) {continue;}
		cmp_nodes.push(w_node);
	}
	return cmp_nodes;
}


function get_config()
{
	var config_path = "O:/_SCRIPTS/opm-tools/TB_scripts/config.json";
	var configfile = new File(config_path);
	configfile.open(FileAccess.ReadOnly);
	var content = configfile.read();
	configfile.close();
	
	try {
  		content = JSON.parse(content);
	}
	catch(err) {
		error = "Error when trying to JSON parse config.json\n -> " + err;
		MessageLog.trace(error);
 		MessageBox.critical(error);
		return NaN;
	}
	return content;
}


function set_node_attr(attrnode, config)
{
	for (var a = 0 ; a < Object.keys(config).length ; a++) {
		var attr_name = Object.keys(config)[a];
		var attr_value = config[attr_name][1];
		
		if (attr_value.toString().indexOf("{version_name}") != -1) {
			attr_value = attr_value.replace("{version_name}", scene.currentVersionName())
		}

		var attr = node.getAttr(attrnode, 1, attr_name);
		attr.setValue(attr_value);
	}
}


function confo_CMP_write_nodes()
{	
	scene.beginUndoRedoAccum("confo_CMP_write_nodes");
	var cmp_nodes = get_CMP_write_node();
	var config = get_config();
	for (var i = 0 ; i < cmp_nodes.length ; i++) {
		var cmp_node = cmp_nodes[i];
		var n_name = node.getName(cmp_node);

		if (n_name.search("COMPO") == -1) {continue;}

		if (n_name.search("W")!= -1 || n_name.search("CHECK")!= -1) {
			node.rename(cmp_node, "COMPO_CHECK");
			set_node_attr(cmp_node, config["COMPO_CHECK"]);
		}

		if (n_name.search("DEF")!= -1) {
			node.rename(cmp_node, "COMPO_DEF");
			set_node_attr(cmp_node, config["COMPO_DEF"]);
		}
	}
	scene.endUndoRedoAccum();
}


function save_scene_new_version()
{	
	scene.beginUndoRedoAccum("save_scene_new_version");
	scene.setColorSpace("sRGB");
	var n_data = get_scene_name_data(scene.currentVersionName());
	var iter = 1;
	var new_scene_name = get_full_scene_name(n_data[0], n_data[1], (n_data[2]+iter), n_data[3]);

	var sceneDir = new Dir(scene.currentProjectPath());
	while (sceneDir.entryList("*").indexOf(new_scene_name + ".xstage") != -1) {
		iter += 1;
		new_scene_name = get_full_scene_name(n_data[0], n_data[1], (n_data[2]+iter), n_data[3]);
	}

	var is_saved = scene.saveAsNewVersion(new_scene_name, true);
	var txt = "error : new version not saved";
	if (is_saved) {
		txt = "new version saved : " + new_scene_name;
		confo_CMP_write_nodes();
	}

	scene.saveAll();

	MessageLog.trace(txt);
	MessageBox.information(txt);
	scene.endUndoRedoAccum();
}