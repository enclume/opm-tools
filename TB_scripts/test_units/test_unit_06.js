//
//This is a sandbox area where you can write scripts
// that can be run but never saved.
// Press run to execute the script in the editor or
// the selected part if a selection was made.
// Note: if the sandbox code is a complete function,
//  the function must be called in order to execute.


function print_sub_attr(pattr, lvl)
{
	if (!pattr.hasSubAttributes()) {return;}
	var before = "";
	if (lvl > 0) {
		for (var z = 0 ; z < lvl ; z++) {
			before += "__";
		}
		before += " ";
	}
	
	var sub_attr = pattr.getSubAttributes();
	for (var z = 0 ; z < sub_attr.length ; z++) {
		var sattr = sub_attr[z]
		MessageLog.trace(before + sattr.fullKeyword() + " : " + sattr.textValue());
		if (sattr.hasSubAttributes()) {
			var newLvl = lvl + 1
			print_sub_attr(sattr, newLvl)
		}
	}
}

function print_node_attr(dnode)
{
	
	var attr_list = node.getAttrList(dnode, 1)
	
	for (var i = 0 ; i < attr_list.length ; i++) {
		var attr = attr_list[i]
		MessageLog.trace(attr.fullKeyword() + " : " + attr.textValue());
		if (!attr.hasSubAttributes()) {continue;}
		print_sub_attr(attr, 1)
	}
}

function update_drawing(dnode)
{
	scene.beginUndoRedoAccum("update_drawing")
	

	//var elementid = node.getElementId(dnode);
	//MessageLog.trace(element.physicalName(elementid));
	
	// create a node
	var myNodeName = "testPsdDrawing";
  	var myColumnName = myNodeName;
  	var myNode = node.add("Top", myNodeName, "READ",0,0,0);
	
	// setup a column
  	var myColumn = column.add(myColumnName, "DRAWING");

	// setup an element
  	var myElement = element.add(myNodeName, "COLOR", 12, "PSD", "None");

	// add the element to the column
  	column.setElementIdOfDrawing(myColumnName, myElement);

	// add a column to the node
  	node.linkAttr(myNode, "DRAWING.ELEMENT", myColumnName);

	// create a drawing
	var MyDrawingName = "OPM101_BGC_SH051_chambreMalekInt_BGC_v003_CONFO";
  	Drawing.create(myElement, MyDrawingName, true, false);

	// get file source path and destination path
	var filepath_source = "O:/_COMPOSITING/_SCRIPTS/opm-tools/confo_bg_psd/OPM101_BGC_SH051_chambreMalekInt_BGC_v003_CONFO.psd";
	var filepath_final = Drawing.filename(myElement, MyDrawingName);
	MessageLog.trace(filepath_final);

	// copy file from source to destination
	var MyFile = new FileWrapper(filepath_source)
	MyFile.copy(filepath_final)
	
	// Set the added drawing as the current drawing on the element node's drawing column.
  	column.setEntry(myColumnName, 0, 1, MyDrawingName); // first num = 0 or 1 ?
	

	return;

	var nodePath = "Top/MyDrawing";
	var myElement = node.getElementId(nodePath); // myElement
	// Add the drawing to the scene. For now it's just a placeholder until we copy the drawing file into the temp or project folder.
	var MyDrawingName = "MyDrawingName"; // MyDrawingName exposure in the Xsheet
	Drawing.create(myElement, MyDrawingName, true);

	// Copy the external image file into the temp (if the file wasn't saved since the addition of the element node) // project (if the element was saved) folder
	var filename = sourceFileName(frameIndex, lowQuality);
	var sourceFile = "c:/myFolder/MyExternalImage.png";  // The extension of the file must match the file format of the element. @sa element::add
	var destinationFile = Drawing.filename(myElement, MyDrawingName);
	copyFile(sourceFile, destinationFile);  // @sa PermanentFile

	// get the rigth colunm
	var drawingColumn = node.linkedColumn(nodePath, "DRAWING.ELEMENT");
	// Set the added drawing as the current drawing on the element node's drawing column.
	column.setEntry(drawingColumn, 1, frame.current(), MyDrawingName);

	scene.endUndoRedoAccum()
}


MessageLog.trace("___START___");

var selected_node = selection.selectedNodes()[0];
//print_node_attr(selected_node)
update_drawing(selected_node);

MessageLog.trace("____END____\n");