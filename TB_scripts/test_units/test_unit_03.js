//
//This is a sandbox area where you can write scripts
// that can be run but never saved.
// Press run to execute the script in the editor or
// the selected part if a selection was made.
// Note: if the sandbox code is a complete function,
//  the function must be called in order to execute.

function get_all_nodes(only_top_lvl) 
{
	if (only_top_lvl) {return node.subNodes(node.root());}

	var node_list = []
	for (var i = 0 ; i < 1 ; i++) {
		break;
	}
	return node_list
}


function get_node_by_type(node_type)
{
	var node_list = []

	for (var i = 0 ; i < 1 ; i++) {
		break;
	}

	return node_list
}


function get_CMP_write_node()
{
	var w_nodes = node.getNodes(["WRITE"]);
	var cmp_nodes = [];
	for (var i = 0 ; i < w_nodes.length ; i++) {
		var w_node = w_nodes[i];
		var w_name = node.getName(w_node);
		if (w_name.search("COMPO") == -1) {continue;}
		cmp_nodes.push(w_node);
	}

	//MessageLog.trace("cmp_nodes : " + cmp_nodes);
	return cmp_nodes
}


function confo_CMP_write_nodes()
{	
	scene.beginUndoRedoAccum("confo_CMP_write_nodes")
	var cmp_nodes = get_CMP_write_node();
	for (var i = 0 ; i < cmp_nodes.length ; i++) {
		var cmp_node = cmp_nodes[i];
		var n_name = node.getName(cmp_node);
		//MessageLog.trace(n_name);
		if (n_name === "COMPO_W") {
			node.rename(cmp_node, "COMPO_CHECK");
		}
		/*
		if (n_name === "COMPO_W" || n_name === "COMPO_CHECK") {
			node.rename(cmp_node, "COMPO_W");
		}
		*/
		//if (n_name === "COMPO_DEF") {}
	}
	scene.endUndoRedoAccum()
}


function disable_nonCMP_write_node()
{	
	scene.beginUndoRedoAccum("disable_nonCMP_write_node")
	var w_nodes = node.getNodes(["WRITE"]);

	for (var i = 0 ; i < w_nodes.length ; i++) {
		var w_node = w_nodes[i];
		var w_name = node.getName(w_node);
		if (w_name.search("COMPO") == -1) {
			node.setEnable(w_node, false)
		} else {
			node.setEnable(w_node, true)
		}
	}

	MessageLog.trace("COMP write nodes enabled, other write nodes disabled");
	scene.endUndoRedoAccum()
}


function test(p_node)
{
	MessageLog.trace("testing node : " + node.getName(p_node));
	MessageLog.trace("(nbr of children : " + node.numberOfSubNodes(p_node) + ")");
	MessageLog.trace();
	var children = node.subNodes(p_node);
	for (var i = 0 ; i < children.length ; i++) {
		c_node = children[i]
		if (node.type(c_node) != "WRITE") {continue;}
		MessageLog.trace(c_node)
	}
}


function init_work_setup()
{
	disable_nonCMP_write_node();
	confo_CMP_write_nodes();
	Action.perform("onActionCleanPaletteLists()");
	MessageLog.trace("initial setup done");
}


function relink_psd_bg()
{
	var read_nodes = node.getNodes(["READ"]);
	var bg_nodes = [];

	for (var i = 0 ; i < read_nodes.length ; i++) {
		var read_node = read_nodes[i];
		if (read_node.search("BG/BG") == -1) {continue;}
		bg_nodes.push(read_node);
	}
	
	for (var i = 0 ; i < bg_nodes.length ; i++) {
		var bg_node = bg_nodes[i];
		var current_psd = node.getTextAttr(bg_node, 1, "drawing")
		
		var ep = 101;
		var sh = 3;
		var psd_name = ["OPM"+ep, "BGC", "SH"+sh, "chambreMalekInt", "BGC"];
		psd_name.push("v002");
		psd_name.push("CONFO.psd");

		psd_name = psd_name.join("_");

		var root_path = ["O:", "_COMPOSITING", "_BG COLO FINAUX", "OPM "+ep];
		root_path = root_path.join("/");

		var new_psd = root_path + "/" + psd_name;
		
		var file = new File(new_psd);
		if (file.exists) {MessageLog.trace("PSD file does not exist");}

		MessageLog.trace(current_psd);
	}
	//MessageLog.trace(bg_nodes);
	//MessageLog.trace(node.getTextAttr(selection.selectedNodes(), 1, "drawing"));
}


MessageLog.trace("___START___");
relink_psd_bg()

MessageLog.trace("____END____\n");



