//
//This is a sandbox area where you can write scripts
// that can be run but never saved.
// Press run to execute the script in the editor or
// the selected part if a selection was made.
// Note: if the sandbox code is a complete function,
//  the function must be called in order to execute.

function int_to_string(digit, str_length)
{
	digit = digit.toString();

	while (digit.length < str_length) {
		digit = "0" + digit;
	}
	
	return digit;
}

function send_scene_to_render_farm()
{
	var job = {
		name: "",
		script: "",
		variables: {},
		requirements: {},
		others: {}
	};
	
	job["variables"]["-TB_path"] = "C:\\Program Files (x86)\\Toon Boom Animation\\Toon Boom Harmony 22 Premium\\win64\\bin";
	job["variables"]["-scene_path"] = scene.currentProjectPathRemapped() + "\\" + scene.currentVersionName() + ".xstage";
	 
	var job_collector_path = "P:\\laFerme\\jobs\\collector";
	var job_collector = new Dir(job_collector_path);
	var listDir = job_collector.entryList("*");

	var job_name = "TB_render_";
	var nbr = 1;

	while (listDir.indexOf(job_name + int_to_string(nbr, 3) + ".json") != -1) {nbr += 1;}

	job["name"] = job_name + int_to_string(nbr, 3);
	

	var jobfile = new File(job_collector_path + "\\" + job["name"] + ".json")
	jobfile.open(FileAccess.WriteOnly);
	job = JSON.stringify(job);	
	jobfile.write(job);

	MessageLog.trace(job_collector.absPath);
}

send_scene_to_render_farm()