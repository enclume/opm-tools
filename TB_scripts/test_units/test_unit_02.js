//
//This is a sandbox area where you can write scripts
// that can be run but never saved.
// Press run to execute the script in the editor or
// the selected part if a selection was made.
// Note: if the sandbox code is a complete function,
//  the function must be called in order to execute.


function get_all_nodes(only_top_lvl) 
{
	if (only_top_lvl) {return node.subNodes(node.root());}

	var node_list = []
	for (var i = 0 ; i < 1 ; i++) {
		break;
	}
	return node_list
}


function get_node_by_type(node_type)
{
	var node_list = []

	for (var i = 0 ; i < 1 ; i++) {
		break;
	}

	return node_list
}


function get_CMP_write_node()
{
	var w_nodes = node.getNodes(["WRITE"]);
	var cmp_nodes = [];
	for (var i = 0 ; i < w_nodes.length ; i++) {
		var w_node = w_nodes[i];
		var w_name = node.getName(w_node);
		if (w_name.search("COMPO") == -1) {continue;}
		cmp_nodes.push(w_node);
	}

	//MessageLog.trace("cmp_nodes : " + cmp_nodes);
	return cmp_nodes
}


function confo_CMP_write_nodes()
{	
	scene.beginUndoRedoAccum("confo_CMP_write_nodes")
	var cmp_nodes = get_CMP_write_node();
	for (var i = 0 ; i < cmp_nodes.length ; i++) {
		var cmp_node = cmp_nodes[i];
		var n_name = node.getName(cmp_node);
		//MessageLog.trace(n_name);
		if (n_name === "COMPO_W") {
			node.rename(cmp_node, "COMPO_CHECK");
		}
		/*
		if (n_name === "COMPO_W" || n_name === "COMPO_CHECK") {
			node.rename(cmp_node, "COMPO_W");
		}
		*/
		//if (n_name === "COMPO_DEF") {}
	}
	scene.endUndoRedoAccum()
}


function disable_nonCMP_write_node()
{	
	scene.beginUndoRedoAccum("disable_nonCMP_write_node")
	var w_nodes = node.getNodes(["WRITE"]);

	for (var i = 0 ; i < w_nodes.length ; i++) {
		var w_node = w_nodes[i];
		var w_name = node.getName(w_node);
		if (w_name.search("COMPO") == -1) {
			node.setEnable(w_node, false)
		} else {
			node.setEnable(w_node, true)
		}
	}

	MessageLog.trace("COMP write nodes enabled, other write nodes disabled");
	scene.endUndoRedoAccum()
}


function test(p_node)
{
	MessageLog.trace("testing node : " + node.getName(p_node));
	MessageLog.trace("(nbr of children : " + node.numberOfSubNodes(p_node) + ")");
	MessageLog.trace();
	var children = node.subNodes(p_node);
	for (var i = 0 ; i < children.length ; i++) {
		c_node = children[i]
		if (node.type(c_node) != "WRITE") {continue;}
		MessageLog.trace(c_node)
	}
}


function start_cmp_render()
{
	disable_nonCMP_write_node();
	confo_CMP_write_nodes();
	MessageLog.trace("start cmp render");
	
	Action.perform("onActionComposite()");
}



MessageLog.trace("___START___");
start_cmp_render()

MessageLog.trace("____END____\n");



