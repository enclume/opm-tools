function int_to_string(digit, str_length)
{
	digit = digit.toString();

	while (digit.length < str_length) {
		digit = "0" + digit;
	}
	
	return digit;
}


function get_basic_scene_name(ep, sh)
{
	ep = int_to_string(ep, 3);
	sh = int_to_string(sh, 3);

	var final_string = ["OPM", ep, "SH"+sh];
	return final_string.join("_")
}


function get_full_scene_name(ep, sh, version)
{
	version = int_to_string(version, 2);

	var final_string = get_basic_scene_name(ep, sh);
	final_string = [final_string, "CMP", "V"+version];
	return final_string.join("_");
}


function is_digit(str)
{	
	if (isNaN(Number(str))) {return false;}
	return true;
}


function get_scene_name_data(name)
{
	name = name.split("")

	var data = ["", "", ""];
	var in_digit = false;
	var pointer = 0;

	for (var i = 0 ; i < name.length; i++) {
		var letter = name[i];
		if (is_digit(letter)) {
			if (in_digit == false) {in_digit = true;}
			data[pointer] += letter;
		} else {
			if (in_digit == true) {
				in_digit = false;
				pointer += 1;
				if (pointer > 2) {break;}
			}
		}
	}

	for (var i = 0 ; i < data.length; i++) {data[i] = Number(data[i]);}

	return data;
}


function update_filename_in_path(filepath, new_name)
{
	filepath = filepath.split("\\");
	filepath[filepath.length - 1] = new_name;
	filepath = filepath.join("\\");
	return filepath;
}


function save_scene_new_version()
{	
	var n_data = get_scene_name_data(scene.currentVersionName());
	var new_scene_name = get_full_scene_name(n_data[0], n_data[1], (n_data[2]+1));
	var is_saved = scene.saveAsNewVersion(new_scene_name, true);
	if (is_saved) {
		MessageLog.trace("saving new version : " + new_scene_name);
	} else {
		MessageLog.trace("error : new version not saved");
	}
}



MessageLog.trace("___START___");
/*
var current_scene_name = scene.currentScene();
MessageLog.trace(current_scene_name);

var n_data = get_scene_name_data(current_scene_name);
MessageLog.trace(n_data);

var new_scene_name = get_full_scene_name(n_data[0], n_data[1], n_data[2]);
MessageLog.trace(new_scene_name);

var old_filepath = scene.currentProjectPathRemapped();
MessageLog.trace(old_filepath);

var new_filepath = update_filename_in_path(old_filepath, new_scene_name);
MessageLog.trace(new_filepath);
*/
save_scene_new_version();

MessageLog.trace("____END____\n");