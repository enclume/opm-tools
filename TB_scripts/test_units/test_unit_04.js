//
//This is a sandbox area where you can write scripts
// that can be run but never saved.
// Press run to execute the script in the editor or
// the selected part if a selection was made.
// Note: if the sandbox code is a complete function,
//  the function must be called in order to execute.


function int_to_string(digit, str_length)
{
	digit = digit.toString();

	while (digit.length < str_length) {
		digit = "0" + digit;
	}
	
	return digit;
}


function get_CONFO_psd_files(ep)
{
	ep = int_to_string(ep, 3);
	var root_dirpath = "O:/_COMPOSITING/_BG COLO FINAUX/OPM "+ep;
	var root_dir = new Dir(root_dirpath);

	if (!root_dir.exists) {
		MessageBox.critical("error : directory \"" + root_dirpath + "\" does not exist ! ");
		return [];
	}

	var all_files = root_dir.entryList("*");
	var confo_files = [];
	
	for (var i = 0 ; i < all_files.length ; i++) {
		var filename = all_files[i];
		if (filename.search("CONFO") == -1) {continue;}
		confo_files.push(filename);
	}
	//MessageLog.trace(confo_files);
	return confo_files;
}


function get_all_backdrops(p_node)
{
	var bd_list = Backdrop.backdrops(p_node);
	
	
	//MessageLog.trace(bd_list.length + " -> node \""+node.getName(p_node)+"\" ("+node.type(p_node)+")");

	var c_nodes = node.subNodes(p_node);
	if (c_nodes.length == 0) {return bd_list;}

	for (var i = 0 ; i < c_nodes.length ; i++) {
		var c_node = c_nodes[i];
		if (node.type(c_node) != "GROUP") {continue;}
		var c_bd_list = get_all_backdrops(c_node);
		if (c_bd_list.length == 0) {continue;}
		bd_list = bd_list.concat(c_bd_list);
	}
	
	return bd_list;
}


function get_backdrops(names)
{	
	var bd_list = [];

	//MessageLog.trace(names.indexOf(bd.title.text))
	
	var all_bd = get_all_backdrops(node.root());
	//MessageLog.trace("all_bd : "+all_bd.length+" items\n")

	for (var i = 0 ; i < all_bd.length ; i++) {
		var bd = all_bd[i];
		
		if (names.indexOf(bd.title.text) == -1) {continue;}
		bd_list.push(bd);
		
	}
	
	return bd_list;
	/*
	MessageLog.trace("___backdrops ("+bd_list.length+") :");
	for (var i = 0 ; i < bd_list.length ; i++) {
		MessageLog.trace(bd_list[i].title.text);
	}
	return bd_list;*/
}


function setEnable_backdrop(bd, bool_enable) 
{
	var bd_nodes = Backdrop.nodes(bd);
	for (var i = 0 ; i < bd_nodes.length ; i++) {
		var bd_node = bd_nodes[i];
		node.setEnable(bd_node, bool_enable)
	}
}


function get_config()
{
	var config_path = "O:/_COMPOSITING/_SCRIPTS/opm-tools/TB_scripts/config.json";
	var configfile = new File(config_path);
	configfile.open(FileAccess.ReadOnly)
	var content = configfile.read();
	configfile.close()
	
	try {
  		content = JSON.parse(content);
	}
	catch(err) {
		error = "Error when try to JSON parse config.json\n -> " + err
		MessageLog.trace(error);
 		MessageBox.critical(error);
		return NaN;
	}
	return content;
}


function disable_backdrops()
{	
	scene.beginUndoRedoAccum("disable_backdrops");
	var to_disable = get_config();	
	to_disable = to_disable["backdrops_to_disable"];

	var bds = get_backdrops(to_disable);
	
	for (var b = 0 ; b < bds.length ; b++) {
		setEnable_backdrop(bds[b], false);
	}
	MessageBox.information("backdrops disabled :\n" + to_disable.join(", "));
	scene.endUndoRedoAccum();
}

function test_file()
{
	var root_dirpath = "O:/_COMPOSITING/_BG COLO FINAUX/OPM 101";
	var filename = "OPM101_BGC_SH002_chambreMalekInt_v03_CONFO.psd";
	var filepath = root_dir + "/" + filename;

	var testfile = new File(filepath);
	var root_dir = new Dir(root_dirpath);
	
	MessageLog.trace(root_dir.entryList("*"));
}

MessageLog.trace("___START___");
disable_backdrops();


MessageLog.trace("____END____\n");