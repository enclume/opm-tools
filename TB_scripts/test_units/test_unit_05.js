//
//This is a sandbox area where you can write scripts
// that can be run but never saved.
// Press run to execute the script in the editor or
// the selected part if a selection was made.
// Note: if the sandbox code is a complete function,
//  the function must be called in order to execute.


function get_all_nodes(only_top_lvl) 
{
	if (only_top_lvl) {return node.subNodes(node.root());}

	var node_list = []
	for (var i = 0 ; i < 1 ; i++) {
		break;
	}
	return node_list
}


function get_node_by_type(node_type)
{
	var node_list = []

	for (var i = 0 ; i < 1 ; i++) {
		break;
	}

	return node_list
}


function get_CMP_write_node()
{
	var w_nodes = node.getNodes(["WRITE"]);
	var cmp_nodes = [];
	for (var i = 0 ; i < w_nodes.length ; i++) {
		var w_node = w_nodes[i];
		var w_name = node.getName(w_node);
		if (w_name.search("COMPO") == -1) {continue;}
		cmp_nodes.push(w_node);
	}

	//MessageLog.trace("cmp_nodes : " + cmp_nodes);
	return cmp_nodes
}


function get_config()
{
	var config_path = "O:/_COMPOSITING/_SCRIPTS/opm-tools/TB_scripts/config.json";
	var configfile = new File(config_path);
	configfile.open(FileAccess.ReadOnly)
	var content = configfile.read();
	configfile.close()
	
	try {
  		content = JSON.parse(content);
	}
	catch(err) {
		error = "Error when try to JSON parse config.json\n -> " + err
		MessageLog.trace(error);
 		MessageBox.critical(error);
		return NaN;
	}
	return content;
}

function set_node_attr(attrnode, config)
{
	for (var a = 0 ; a < Object.keys(config).length ; a++) {
		var attr_name = Object.keys(config)[a];
		var attr_type = config[attr_name][0];
		var attr_value = config[attr_name][1];
		
		if (attr_value.indexOf("{version_name}") != -1) {
			attr_value = attr_value.replace("{version_name}", scene.currentVersionName())
		}

		var attr = node.getAttr(attrnode, 1, attr_name);
		attr.setValue(attr_value);
	}
}

function confo_CMP_write_nodes()
{	
	scene.beginUndoRedoAccum("confo_CMP_write_nodes")
	var cmp_nodes = get_CMP_write_node();
	var config = get_config();
	for (var i = 0 ; i < cmp_nodes.length ; i++) {
		var cmp_node = cmp_nodes[i];
		var n_name = node.getName(cmp_node);
		MessageLog.trace(n_name);
		if (n_name.search("COMPO") == -1) {continue;}

		if (n_name.search("W")!= -1 || n_name.search("CHECK")!= -1) {
			node.rename(cmp_node, "COMPO_CHECK");
			set_node_attr(cmp_node, config["COMPO_CHECK"])
		}

		if (n_name.search("DEF")!= -1) {
			node.rename(cmp_node, "COMPO_DEF");
			
			set_node_attr(cmp_node, config["COMPO_DEF"])

			var all_attr = node.getAllAttrKeywords(cmp_node)
			
			for (var a = 0 ; a < all_attr.length ; a++) {
				var attr = all_attr[a];
				var value = node.getAttr(cmp_node, 1, attr);
				//value = node.getElementId(cmp_node)
				var type = value.name();
				
				value = value.textValue();
				MessageLog.trace(attr.toString() + " -> " + type + ", " + value.toString());
			}
		}
	}
	scene.endUndoRedoAccum()
}


function disable_nonCMP_write_node()
{	
	scene.beginUndoRedoAccum("disable_nonCMP_write_node")
	var w_nodes = node.getNodes(["WRITE"]);

	for (var i = 0 ; i < w_nodes.length ; i++) {
		var w_node = w_nodes[i];
		var w_name = node.getName(w_node);
		if (w_name.search("COMPO") == -1) {
			node.setEnable(w_node, false)
		} else {
			node.setEnable(w_node, true)
		}
	}

	MessageLog.trace("COMP write nodes enabled, other write nodes disabled");
	scene.endUndoRedoAccum()
}


function test(p_node)
{
	MessageLog.trace("testing node : " + node.getName(p_node));
	MessageLog.trace("(nbr of children : " + node.numberOfSubNodes(p_node) + ")");
	MessageLog.trace();
	var children = node.subNodes(p_node);
	for (var i = 0 ; i < children.length ; i++) {
		c_node = children[i]
		if (node.type(c_node) != "WRITE") {continue;}
		MessageLog.trace(c_node)
	}
}


function init_work_setup()
{
	disable_nonCMP_write_node();
	confo_CMP_write_nodes();
	Action.perform("onActionCleanPaletteLists()");
	MessageLog.trace("initial setup done");
}



MessageLog.trace("___START___");
confo_CMP_write_nodes()

MessageLog.trace("____END____\n");



