function consoleWrite(message)
{
	MessageLog.trace(message);
	System.println(message);
}

function get_PRCMP_write_node()
{
	var w_nodes = node.getNodes(["WRITE"]);
	var prcmp_nodes = [];
	for (var i = 0 ; i < w_nodes.length ; i++) {
		var w_node = w_nodes[i];
		var w_name = node.getName(w_node);
		if (w_name.search("precomp") == -1) {continue;}
		prcmp_nodes.push(w_node);
	}
	return prcmp_nodes;
}


function get_config()
{
	var config_path = "O:/_SCRIPTS/opm-tools/TB_scripts/config.json";
	var configfile = new File(config_path);
	configfile.open(FileAccess.ReadOnly);
	var content = configfile.read();
	configfile.close();
	
	try {
  		content = JSON.parse(content);
	}
	catch(err) {
		var error = "Error when trying to JSON parse config.json\n -> " + err;
		consoleWrite(error);
 		//MessageBox.critical(error);
		return NaN;
	}
	return content;
}


function set_node_attr(attrnode, config)
{
	for (var a = 0 ; a < Object.keys(config).length ; a++) {
		var attr_name = Object.keys(config)[a];
		var attr_value = config[attr_name][1];
		
		if (attr_value.toString().indexOf("{version_name}") != -1) {
			attr_value = attr_value.replace("{version_name}", scene.currentVersionName());
		}
		if (attr_value.toString().indexOf("{version_name_short}") != -1) {
			var v_name_short = scene.currentVersionName(); //OPM_101_SHxxx
			v_name_short = v_name_short.split("_");
			while (v_name_short[v_name_short.length-1].indexOf("SH") == -1) {
				v_name_short.pop();
			}
			v_name_short = v_name_short.join("_")
			attr_value = attr_value.replace("{version_name_short}", v_name_short);
		}

		var attr = node.getAttr(attrnode, 1, attr_name);
		attr.setValue(attr_value);
	}
}


function confo_PRCMP_write_nodes()
{	
	var prcmp_nodes = get_PRCMP_write_node();
	var config = get_config();
	for (var i = 0 ; i < prcmp_nodes.length ; i++) {
		var prcmp_node = prcmp_nodes[i];
		var n_name = node.getName(prcmp_node);
		if (n_name.search("precomp") == -1) {continue;}
		set_node_attr(prcmp_node, config["precomp"]);
	}
}


function disable_nonPRCMP_write_node()
{	
	scene.beginUndoRedoAccum("disable_nonPRCMP_write_node");
	var w_nodes = node.getNodes(["WRITE"]);

	for (var i = 0 ; i < w_nodes.length ; i++) {
		var w_node = w_nodes[i];
		var w_name = node.getName(w_node);
		if (w_name.search("precomp") == -1) {
			node.setEnable(w_node, false);
		} else {
			node.setEnable(w_node, true);
		}
	}

	consoleWrite("precomp write nodes enabled, other write nodes disabled");
	scene.endUndoRedoAccum();
}


function print_data() {
    consoleWrite("[JOB_INFO]_[PROGRESS]_[START]_["+scene.getStartFrame()+"]")
    consoleWrite("[JOB_INFO]_[PROGRESS]_[END]_["+scene.getStopFrame()+"]")
}


consoleWrite("")
consoleWrite("start of prejob script")
print_data()
scene.setColorSpace("sRGB");
disable_nonPRCMP_write_node();
confo_PRCMP_write_nodes();

consoleWrite("end of prejob script")
consoleWrite("")