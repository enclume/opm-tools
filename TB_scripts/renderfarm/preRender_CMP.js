function consoleWrite(message)
{
	MessageLog.trace(message);
	//System.println(message);
}

function get_CMP_write_node()
{
	var w_nodes = node.getNodes(["WRITE"]);
	var cmp_nodes = [];
	for (var i = 0 ; i < w_nodes.length ; i++) {
		var w_node = w_nodes[i];
		var w_name = node.getName(w_node);
		if (w_name.search("COMPO") == -1) {continue;}
		cmp_nodes.push(w_node);
	}
	return cmp_nodes;
}


function get_config()
{
	var config_path = "O:/_SCRIPTS/opm-tools/TB_scripts/config.json";
	var configfile = new File(config_path);
	configfile.open(FileAccess.ReadOnly);
	var content = configfile.read();
	configfile.close();
	
	try {
  		content = JSON.parse(content);
	}
	catch(err) {
		var error = "Error when trying to JSON parse config.json\n -> " + err;
		consoleWrite(error);
 		//MessageBox.critical(error);
		return NaN;
	}
	return content;
}


function set_node_attr(attrnode, config)
{
	for (var a = 0 ; a < Object.keys(config).length ; a++) {
		var attr_name = Object.keys(config)[a];
		var attr_value = config[attr_name][1];
		
		if (attr_value.toString().indexOf("{version_name}") != -1) {
			attr_value = attr_value.replace("{version_name}", scene.currentVersionName())
		}

		var attr = node.getAttr(attrnode, 1, attr_name);
		attr.setValue(attr_value);
	}
}


function confo_CMP_write_nodes()
{	
	scene.beginUndoRedoAccum("confo_CMP_write_nodes");
	var cmp_nodes = get_CMP_write_node();
	var config = get_config();
	for (var i = 0 ; i < cmp_nodes.length ; i++) {
		var cmp_node = cmp_nodes[i];
		var n_name = node.getName(cmp_node);

		if (n_name.search("COMPO") == -1) {continue;}

		if (n_name.search("W")!= -1 || n_name.search("CHECK")!= -1) {
			node.rename(cmp_node, "COMPO_CHECK");
			set_node_attr(cmp_node, config["COMPO_CHECK"]);
		}

		if (n_name.search("DEF")!= -1) {
			node.rename(cmp_node, "COMPO_DEF");
			set_node_attr(cmp_node, config["COMPO_DEF"]);
		}
	}
	scene.endUndoRedoAccum();
}


function disable_nonCMP_write_node()
{	
	scene.beginUndoRedoAccum("disable_nonCMP_write_node");
	var w_nodes = node.getNodes(["WRITE"]);

	for (var i = 0 ; i < w_nodes.length ; i++) {
		var w_node = w_nodes[i];
		var w_name = node.getName(w_node);
		if (w_name.search("COMPO") == -1) {
			node.setEnable(w_node, false);
		} else {
			node.setEnable(w_node, true);
		}
	}

	consoleWrite("COMP write nodes enabled, other write nodes disabled");
	scene.endUndoRedoAccum();
}


function print_data() {
    consoleWrite("[JOB_INFO]_[PROGRESS]_[START]_["+scene.getStartFrame()+"]")
    consoleWrite("[JOB_INFO]_[PROGRESS]_[END]_["+scene.getStopFrame()+"]")
}


consoleWrite("");
consoleWrite("start of prejob script");
print_data();
scene.setColorSpace("sRGB");
disable_nonCMP_write_node();
confo_CMP_write_nodes();
consoleWrite("end of prejob script");
consoleWrite("");

//MessageLog.trace("TESTEST");