/*
	Use the function "init_work_setup" to launch the initial scene setup.
    Are called : disable_nonCMP_write_node, disable_backdrops, save_scene_new_version, confo_CMP_write_nodes and add_prcmp_write.
	author : Guillaume Geelen
	version : 1.12
*/

function get_CMP_write_node()
{
	var w_nodes = node.getNodes(["WRITE"]);
	var cmp_nodes = [];
	for (var i = 0 ; i < w_nodes.length ; i++) {
		var w_node = w_nodes[i];
		var w_name = node.getName(w_node);
		if (w_name.search("COMPO") == -1) {continue;}
		cmp_nodes.push(w_node);
	}
	return cmp_nodes;
}


function get_config()
{
	var config_path = "O:/_SCRIPTS/opm-tools/TB_scripts/config.json";
	var configfile = new File(config_path);
	configfile.open(FileAccess.ReadOnly);
	var content = configfile.read();
	configfile.close();
	
	try {
  		content = JSON.parse(content);
	}
	catch(err) {
		error = "Error when trying to JSON parse config.json\n -> " + err;
		MessageLog.trace(error);
 		MessageBox.critical(error);
		return NaN;
	}
	return content;
}


function set_node_attr(attrnode, config)
{
	for (var a = 0 ; a < Object.keys(config).length ; a++) {
		var attr_name = Object.keys(config)[a];
		var attr_value = config[attr_name][1];
		
		if (attr_value.toString().indexOf("{version_name}") != -1) {
			attr_value = attr_value.replace("{version_name}", scene.currentVersionName());
		}
		if (attr_value.toString().indexOf("{version_name_short}") != -1) {
			var v_name_short = scene.currentVersionName(); //OPM_101_SHxxx
			v_name_short = v_name_short.split("_");
			while (v_name_short[v_name_short.length-1].indexOf("SH") == -1) {
				v_name_short.pop();
			}
			v_name_short = v_name_short.join("_")
			attr_value = attr_value.replace("{version_name_short}", v_name_short);
		}

		var attr = node.getAttr(attrnode, 1, attr_name);
		attr.setValue(attr_value);
	}
}


function confo_CMP_write_nodes()
{	
	scene.beginUndoRedoAccum("confo_CMP_write_nodes");
	var cmp_nodes = get_CMP_write_node();
	var config = get_config();
	for (var i = 0 ; i < cmp_nodes.length ; i++) {
		var cmp_node = cmp_nodes[i];
		var n_name = node.getName(cmp_node);

		if (n_name.search("COMPO") == -1) {continue;}

		if (n_name.search("W")!= -1 || n_name.search("CHECK")!= -1) {
			node.rename(cmp_node, "COMPO_CHECK");
			set_node_attr(cmp_node, config["COMPO_CHECK"]);
		}

		if (n_name.search("DEF")!= -1) {
			node.rename(cmp_node, "COMPO_DEF");
			set_node_attr(cmp_node, config["COMPO_DEF"]);
		}
	}
	scene.endUndoRedoAccum();
}


function get_prcmp_path() {
	var path = node.add(node.root(), "precomp", "WRITE", 0, 0, 0);
	//MessageLog.trace(path);
	node.deleteNode(path);
	return path;
}


function get_cmp() {
	var wnodes = node.getNodes(["WRITE"]);
	
	for (var i = 0; i < wnodes.length; i++) {
		if (node.getName(wnodes[i])=="COMPO_DEF") {
			//MessageLog.trace(wnodes[i]);
			return wnodes[i];
		}
	}
	return false;
}


function get_PRCMP_write_node()
{
	var w_nodes = node.getNodes(["WRITE"]);
	var prcmp_nodes = [];
	for (var i = 0 ; i < w_nodes.length ; i++) {
		var w_node = w_nodes[i];
		var w_name = node.getName(w_node);
		if (w_name.search("precomp") == -1) {continue;}
		prcmp_nodes.push(w_node);
	}
	return prcmp_nodes;
}


function confo_PRCMP_write_nodes()
{	
	var prcmp_nodes = get_PRCMP_write_node();
	var config = get_config();
	for (var i = 0 ; i < prcmp_nodes.length ; i++) {
		var prcmp_node = prcmp_nodes[i];
		var n_name = node.getName(prcmp_node);
		if (n_name.search("precomp") == -1) {continue;}
		set_node_attr(prcmp_node, config["precomp"]);
	}
}

function add_prcmp_write() {
	scene.beginUndoRedoAccum("add_prcmp_write");

	var cmp = get_cmp();
	if (cmp == false) {
		var error = "can't find COMPO_DEF node, abort import add_prcmp_write";
		MessageLog.trace(error);
		MessageBox.warning(error);
		return false;
	}
	var source = node.srcNode(cmp, 0);
	var group = node.parentNode(cmp);
	var prcmp = get_prcmp_path();
	var srcPath = "K:/_LIBRARY/pre comp/_export.tpl";
	copyPaste.pasteTemplateIntoGroup(srcPath, group, 0);
	node.setCoord(prcmp, (node.coordX(cmp)+node.width(cmp)+10), node.coordY(cmp));
	node.link(source, 0, prcmp, 0);

	confo_PRCMP_write_nodes()
	
	scene.endUndoRedoAccum();
	return true;
}


function disable_nonCMP_write_node()
{	
	scene.beginUndoRedoAccum("disable_nonCMP_write_node");
	var w_nodes = node.getNodes(["WRITE"]);

	for (var i = 0 ; i < w_nodes.length ; i++) {
		var w_node = w_nodes[i];
		var w_name = node.getName(w_node);
		if (w_name.search("COMPO") == -1) {
			node.setEnable(w_node, false);
		} else {
			node.setEnable(w_node, true);
		}
	}

	MessageLog.trace("COMP write nodes enabled, other write nodes disabled");
	scene.endUndoRedoAccum();
}


function int_to_string(digit, str_length)
{
	digit = digit.toString();

	while (digit.length < str_length) {
		digit = "0" + digit;
	}
	
	return digit;
}


function get_basic_scene_name(ep, sh, after)
{
	if (after == undefined) {after = "";}
	ep = int_to_string(ep, 3);
	sh = int_to_string(sh, 3);

	var final_string = ["OPM", ep, "SH"+sh];
	return final_string.join("_") + after;
}


function get_full_scene_name(ep, sh, version, after)
{
	version = int_to_string(version, 2);

	var final_string = get_basic_scene_name(ep, sh, after);
	final_string = [final_string, "CMP", "V"+version];
	return final_string.join("_");
}


function is_digit(str)
{	
	if (isNaN(Number(str))) {return false;}
	return true;
}


function get_after(name, data) 
{
	//data : [ep, sh, v, after] -> all string
	var after = name;

	if (after.indexOf("_SH") == -1) {return "";}
	after = after.split("_SH"+data[1]).reverse()[0];

	if (after.indexOf("_CMP") != -1) {
		after = after.split("_CMP")[0];
		return after;
	}
	
	if (after.indexOf(".") == -1) {return after;}

	after = after.split(".")[0];
	return after;
}


function get_scene_name_data(name)
{
	var after = name;

	name = name.split("");

	var data = ["", "", "", ""]; //ep, sh, v, after
	
	var in_digit = false;
	var pointer = 0;

	for (var i = 0 ; i < name.length; i++) {
		var letter = name[i];
		if (is_digit(letter)) {
			if (in_digit == false) {in_digit = true;}
			data[pointer] += letter;
		} else {
			if (in_digit == true) {
				in_digit = false;
				pointer += 1;
				if (pointer > 2) {break;}
			}
		}
	}

	after = get_after(after, data);
	
	for (var i = 0 ; i < data.length; i++) {
		data[i] = Number(data[i]);
	}

	data[3] = after;
	return data;
}


function start_save_scene_new_version()
{	
	var current_name = scene.currentVersionName()
	var n_data = get_scene_name_data(current_name);
	var new_scene_name = get_full_scene_name(n_data[0], n_data[1], n_data[2], n_data[3]);

	if (current_name == new_scene_name) {return;}

	scene.beginUndoRedoAccum("save_scene_new_version");
	new_scene_name = get_full_scene_name(n_data[0], n_data[1], (1), n_data[3]);
	var is_saved = scene.saveAsNewVersion(new_scene_name, true);
	var txt = "error : new version not saved";
	if (is_saved) {
		txt = "new version saved : " + new_scene_name;
		confo_CMP_write_nodes();
	}
	scene.saveAll();
	MessageLog.trace(txt);
	MessageBox.information(txt);
	scene.endUndoRedoAccum();
}


function get_all_backdrops(p_node)
{
	var bd_list = Backdrop.backdrops(p_node);
	var c_nodes = node.subNodes(p_node);
	if (c_nodes.length == 0) {return bd_list;}

	for (var i = 0 ; i < c_nodes.length ; i++) {
		var c_node = c_nodes[i];
		if (node.type(c_node) != "GROUP") {continue;}
		var c_bd_list = get_all_backdrops(c_node);
		if (c_bd_list.length == 0) {continue;}
		bd_list = bd_list.concat(c_bd_list);
	}
	
	return bd_list;
}


function get_backdrops(names)
{	
	var bd_list = [];	
	var all_bd = get_all_backdrops(node.root());

	for (var i = 0 ; i < all_bd.length ; i++) {
		var bd = all_bd[i];
		if (names.indexOf(bd.title.text) == -1) {continue;}
		bd_list.push(bd);
	}
	
	return bd_list;
}

function isToAvoid(name, avoidList)
{
	for (var i = 0 ; i < avoidList.length ; i++) {
		if (name.indexOf(avoidList[i]) != -1) {return true;}
	}
	return false;
}

function setEnable_backdrop(bd, bool_enable) 
{
	var to_keep = get_config()["nodes_to_keep"];
	try {
		var bd_nodes = Backdrop.nodes(bd);
	} catch (err) {
		MessageLog.trace("Error in setEnable_backdrop, line 267 (bd: " + bd + ", "+JSON.stringify(bd)+"): " + err);
		return false;
	}
	for (var i = 0 ; i < bd_nodes.length ; i++) {
		var bd_node = bd_nodes[i];
		var nodeName = node.getName(bd_node);
		if (isToAvoid(nodeName, to_keep)) {continue;}
		node.setEnable(bd_node, bool_enable);
	}
	return true;
}

function disable_backdrops()
{	
	scene.beginUndoRedoAccum("disable_backdrops");
	var to_disable = get_config()["backdrops_to_disable"];	

	var bds = get_backdrops(to_disable);
	var error = false;
	for (var b = 0 ; b < bds.length ; b++) {
		error = setEnable_backdrop(bds[b], false);
	}
	var info = "backdrops disabled "
	if (error) {info += "(with Error !)";}
	info += ":\n" + to_disable.join(", ");
	MessageLog.trace(info);
	MessageBox.information(info);
	scene.endUndoRedoAccum();
}


function check_scene_params()
{
	var config = get_config()["scene_check"];
	var fps = scene.getFrameRate();
	var res_x = scene.currentResolutionX();
	var res_y = scene.currentResolutionY();
	var report = [];
	
	if (fps != config["fps"]) {
		report.push("scene fps = " + fps + " (schould be \'" + config["fps"] + "\')");
	}
	if (res_x != config["res_x"]) {
		report.push("scene x resolution = " + res_x + " (schould be \'" + config["res_x"] + "\')");
	}
	if (res_y != config["res_y"]) {
		report.push("scene y resolution = " + res_y + " (schould be \'" + config["res_y"] + "\')");
	}

	report = report.join("\n");

	if (report != "") {
		report = "Scene check bad repport :\n\n" + report;
		report += "\n(Remet tes settings gamin !)";
		MessageBox.warning(report);
	}
}


function setDisplay(displayName)
{

	if (displayName == "Display All"){
		node.setGlobalToDisplayAll();
		MessageLog.trace("Current display set on: "+displayName);
		return;
	}
	
	var dispNodes = node.getNodes(["DISPLAY"]);
	
	for (var i = 0; i < dispNodes.length; i++) {
		var dispNode = dispNodes[i];
		if (node.getName(dispNode) == displayName) {
			node.setAsGlobalDisplay(dispNode);
			MessageLog.trace("Current display set on: "+displayName);
			return;
		}
	}
	MessageLog.trace("Can't set the current display on: "+displayName+" (display node not found)");
}


function init_work_setup()
{	
	check_scene_params()
	disable_nonCMP_write_node();
    disable_backdrops();
	scene.setColorSpace("sRGB");
	setDisplay("05_COMPO_D");
    start_save_scene_new_version();
	confo_CMP_write_nodes();
	add_prcmp_write();
	Action.perform("onActionCleanPaletteLists()");
	MessageLog.trace("initial setup done");
}