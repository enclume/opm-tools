/*
	Use the function "send_scene_to_render_farm" to send the current .xstage file to the render farm.
	author : Guillaume Geelen
	version : 0.10
*/

function get_config()
{
	var config_path = "O:/_SCRIPTS/opm-tools/TB_scripts/config.json";
	var configfile = new File(config_path);
	configfile.open(FileAccess.ReadOnly);
	var content = configfile.read();
	configfile.close();
	
	try {
  		content = JSON.parse(content);
	}
	catch(err) {
		error = "Error when trying to JSON parse config.json\n -> " + err;
		MessageLog.trace(error);
 		MessageBox.critical(error);
		return NaN;
	}
	return content;
}

function int_to_string(digit, str_length)
{
	digit = digit.toString();

	while (digit.length < str_length) {
		digit = "0" + digit;
	}
	
	return digit;
}

function farmDownDialog()
{
	var farmDownD = new Dialog();
	farmDownD.title = "Warning";
	var bodyText = new Label();
	bodyText.text = "The render farm is not active.\nSend anyway (and wait until farm activation) or not ?";
	farmDownD.add( bodyText );
	farmDownD.cancelButtonText = "Send anyway";
	farmDownD.okButtonText = "Do not send";
	return farmDownD.exec();
}

function optionDialog(job)
{
	var optionD = new Dialog();
	optionD.title = "Render Options";
	optionD.width = 500;

	var bodyText = new Label();
	bodyText.text = "current job: "+JSON.stringify(job);

	var uiname = new LineEdit();
	uiname.text = job.nameui;
	uiname.label = "job ui name";

	var prio = new  SpinBox();
	prio.value = 0;
	prio.label = "priority";

	var rendertype = new ComboBox();
	rendertype.editable = false;
	rendertype.itemList = ["CMP", "prcmp"];
	//rendertype.itemList = ["CMP"];
	rendertype.currentItem = "CMP";
	rendertype.label = "render type"

	optionD.add(uiname);
	optionD.add(prio);
	optionD.add(rendertype);
	//optionD.add(bodyText);
	optionD.okButtonText = "Save";
	optionD.cancelButtonText = "Cancel";
	
	if (optionD.exec()) {
		job.priority = prio.value
		job.nameui = uiname.text
		job["variables"]["-preRenderScript"] = "O:\\_SCRIPTS\\opm-tools\\TB_scripts\\renderfarm\\preRender_CMP.js"
		if (rendertype.currentItem != "CMP") {
			job["variables"]["-preRenderScript"] = "O:\\_SCRIPTS\\opm-tools\\TB_scripts\\renderfarm\\preRender_PRCMP.js"
		}
	}
}


function checkSceneDialog(report)
{
	var checkSceneD = new Dialog();
	checkSceneD.title = "Warning";
	var bodyText = new Label();
	bodyText.text = report;
	checkSceneD.add(bodyText);
	checkSceneD.cancelButtonText = "Render anyway";
	checkSceneD.okButtonText = "Do not render";
	return !checkSceneD.exec(); //return True if render anyway
}


function check_scene_params()
{
	var config = get_config()["scene_check"];
	var fps = scene.getFrameRate();
	var res_x = scene.currentResolutionX();
	var res_y = scene.currentResolutionY();
	var report = [];
	
	if (fps != config["fps"]) {
		report.push("scene fps = " + fps + " (schould be \'" + config["fps"] + "\')");
	}
	if (res_x != config["res_x"]) {
		report.push("scene x resolution = " + res_x + " (schould be \'" + config["res_x"] + "\')");
	}
	if (res_y != config["res_y"]) {
		report.push("scene y resolution = " + res_y + " (schould be \'" + config["res_y"] + "\')");
	}

	report = report.join("\n");
	
	if (report != "") {
		report = "Scene check bad repport :\n\n" + report;
		report += "\n(Remet tes settings gamin !)";
		return checkSceneDialog(report);
	}
	return true;
}


function send_scene_to_render_farm()
{	
	scene.setColorSpace("sRGB");
	var render = check_scene_params();
	if (!render) {
		MessageLog.trace("render canceled");
		return;
	}
	var root_farm_path = "P:\\laFerme";
	var ipsharefile = new File(root_farm_path + "\\hostIP_shared");
	if (!ipsharefile.exists) {
		if (farmDownDialog()) {return;} //if farmDownDialog return true, cancel operations
	}
	var job = {
		name: "", 
		nameui: ""+scene.currentVersionName(), 
		script: "launch_TB_render.py", 
		priority: 0, 
		variables: {}, 
		requirements: {}, 
		others: {}, 
		progress: {start: 0, end: 100}};
	
	job["variables"]["-TB_path"] = "C:\\Program Files (x86)\\Toon Boom Animation\\Toon Boom Harmony 22 Premium\\win64\\bin";
	job["variables"]["-scene_path"] = scene.currentProjectPathRemapped() + "\\" + scene.currentVersionName() + ".xstage";
	job["variables"]["-preRenderScript"] = "O:\\_SCRIPTS\\opm-tools\\TB_scripts\\renderfarm\\preRender_CMP.js";
	job["progress"]["start"] = scene.getStartFrame();
	job["progress"]["end"] = scene.getStopFrame();
	 
	var job_collector_path = root_farm_path + "\\jobs\\collector";
	var job_collector = new Dir(job_collector_path);
	var listDir = job_collector.entryList("*");

	var job_name = "TB_render_";
	var nbr = 1;

	while (listDir.indexOf(job_name + int_to_string(nbr, 3) + ".json") != -1) {nbr += 1;}

	job["name"] = job_name + int_to_string(nbr, 3);

	optionDialog(job);

	var jobfile = new File(job_collector_path + "\\" + job["name"] + ".json");
	jobfile.open(FileAccess.WriteOnly);
	job = JSON.stringify(job);	
	jobfile.write(job);

	//MessageLog.trace(job_collector.absPath);
    MessageBox.information("current version \"" + scene.currentVersionName() + "\" send to the render farm");
}