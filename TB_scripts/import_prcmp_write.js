/*
	Use the function "add_prcmp_write" to import the precomp write node from the library folder
	-> K:/_LIBRARY/pre comp/_export.tpl  : this path should exist
	author : Guillaume Geelen
	version : 1.0
*/


function get_prcmp_path() {
	var path = node.add(node.root(), "precomp", "WRITE", 0, 0, 0);
	//MessageLog.trace(path);
	node.deleteNode(path);
	return path;
}


function get_cmp() {
	var wnodes = node.getNodes(["WRITE"]);
	
	for (var i = 0; i < wnodes.length; i++) {
		if (node.getName(wnodes[i])=="COMPO_DEF") {
			//MessageLog.trace(wnodes[i]);
			return wnodes[i];
		}
	}
	return false;
}

function get_PRCMP_write_node()
{
	var w_nodes = node.getNodes(["WRITE"]);
	var prcmp_nodes = [];
	for (var i = 0 ; i < w_nodes.length ; i++) {
		var w_node = w_nodes[i];
		var w_name = node.getName(w_node);
		if (w_name.search("precomp") == -1) {continue;}
		prcmp_nodes.push(w_node);
	}
	return prcmp_nodes;
}


function get_config()
{
	var config_path = "O:/_SCRIPTS/opm-tools/TB_scripts/config.json";
	var configfile = new File(config_path);
	configfile.open(FileAccess.ReadOnly);
	var content = configfile.read();
	configfile.close();
	
	try {
  		content = JSON.parse(content);
	}
	catch(err) {
		error = "Error when trying to JSON parse config.json\n -> " + err;
		MessageLog.trace(error);
 		MessageBox.critical(error);
		return NaN;
	}
	return content;
}


function set_node_attr(attrnode, config)
{
	for (var a = 0 ; a < Object.keys(config).length ; a++) {
		var attr_name = Object.keys(config)[a];
		var attr_value = config[attr_name][1];
		
		if (attr_value.toString().indexOf("{version_name}") != -1) {
			attr_value = attr_value.replace("{version_name}", scene.currentVersionName());
		}
		if (attr_value.toString().indexOf("{version_name_short}") != -1) {
			var v_name_short = scene.currentVersionName(); //OPM_101_SHxxx
			v_name_short = v_name_short.split("_");
			while (v_name_short[v_name_short.length-1].indexOf("SH") == -1) {
				v_name_short.pop();
			}
			v_name_short = v_name_short.join("_")
			attr_value = attr_value.replace("{version_name_short}", v_name_short);
		}

		var attr = node.getAttr(attrnode, 1, attr_name);
		attr.setValue(attr_value);
	}
}


function confo_PRCMP_write_nodes()
{	
	var prcmp_nodes = get_PRCMP_write_node();
	var config = get_config();
	for (var i = 0 ; i < prcmp_nodes.length ; i++) {
		var prcmp_node = prcmp_nodes[i];
		var n_name = node.getName(prcmp_node);
		if (n_name.search("precomp") == -1) {continue;}
		set_node_attr(prcmp_node, config["precomp"]);
	}
}

function add_prcmp_write() {
	scene.beginUndoRedoAccum("add_prcmp_write");

	var cmp = get_cmp();
	if (cmp == false) {
		var error = "can't find COMPO_DEF node, abort import add_prcmp_write";
		MessageLog.trace(error);
		MessageBox.warning(error);
		return false;
	}
	var source = node.srcNode(cmp, 0);
	var group = node.parentNode(cmp);
	var prcmp = get_prcmp_path();
	var srcPath = "K:/_LIBRARY/pre comp/_export.tpl";
	copyPaste.pasteTemplateIntoGroup(srcPath, group, 0);
	node.setCoord(prcmp, (node.coordX(cmp)+node.width(cmp)+10), node.coordY(cmp));
	node.link(source, 0, prcmp, 0);

	confo_PRCMP_write_nodes()
	
	scene.endUndoRedoAccum();
	return true;
}

	


