# HarmonyPremium cmd

Command line things to use with Toon Boom Harmony Premium

## Table of contents
- [How to construct a command line](#how-to-construct-a-command-line)
- [Usefull exemples](#usefull-exemples)
  * [Run a script in a scene without loading the UI](#run-a-script-in-a-scene-without-loading-the-ui)
  * [Render a scene without loading the UI](#render-a-scene-without-loading-the-ui)
- [CMD Arguments](#cmd-arguments)
  * [Normal mode](#normal-mode)
    + [-?|-h|-help|--help](#----h--help---help)
    + [-batch](#-batch)
    + [-camera](#-camera)
    + [-colorrec](#-colorrec)
    + [-combine](#-combine)
    + [-compfields](#-compfields)
    + [-compile](#-compile)
    + [-script](#-script)
    + [-debug](#-debug)
    + [-drawing](#-drawing)
    + [-langdir](#-langdir)
    + [-lang](#-lang)
    + [-link_palette](#-link-palette)
    + [-local_cache](#-local-cache)
    + [-local_cache_timeout](#-local-cache-timeout)
    + [-pencil](#-pencil)
    + [-preRenderScript](#-prerenderscript)
    + [-postRenderScript](#-postrenderscript)
    + [-preRenderInlineScript](#-prerenderinlinescript)
    + [-postRenderInlineScript](#-postrenderinlinescript)
    + [-readonly](#-readonly)
    + [-renderThread](#-renderthread)
    + [-repcolor](#-repcolor)
    + [-res | -resolution](#-res----resolution)
    + [-scene](#-scene)
    + [-second](#-second)
    + [-template](#-template)
    + [-thumbnails](#-thumbnails)
    + [-user](#-user)
    + [-v](#-v)
  * [Database mode](#database-mode)
    + [-env](#-env)
    + [-frames | -comp](#-frames----comp)
    + [-job](#-job)
    + [-lockAssets](#-lockassets)
    + [-lockScene](#-lockscene)
    + [-paint](#-paint)
    + [-version](#-version)




## How to construct a command line
If harmonyPremium is not in your PATH, first navigate to the "bin" folder of your installation directory

e.g.:
```
User>cd C:\Program Files (x86)\Toon Boom Animation\Toon Boom Harmony 22 Premium\win64\bin
```

You can now use the "harmonyPremium" first argument

e.g.:
```
...bin>harmonyPremium -help
```
(this command will show you all the possible arguments)

## Usefull exemples
### Run a script in a scene without loading the UI
```
harmonyPremium [path to your scene.xstage] -batch -compile [path to your script.js]
```

### Render a scene without loading the UI
```
harmonyPremium [path to your scene.xstage] -batch 
```

you can add :
+ "-preRenderScript [path to your preScript.js]" to run a script before the render
+ "-postRenderScript [path to your postScript.js]" to run a script after the render

the full command line will be :
```
harmonyPremium [path to your scene.xstage] -batch -preRenderScript [path to your preScript.js] -postRenderScript [path to your postScript.js]
```


## CMD Arguments
a list of all possible arguments

### Normal mode
#### -?|-h|-help|--help
description : will show you the cmd help

#### -batch
description : use it to not load the UI

#### -camera               
*camera*

#### -colorrec             
*YES | NO*

#### -combine

#### -compfields           
*PAL | NTSC*

#### -compile              
*qt_script_file*

#### -script               
*inline script string*

#### -debug

#### -drawing

#### -langdir              
*directory*

#### -lang                 
*language*

#### -link_palette                                 
description : Link palettes to model (to be used with -template)

#### -local_cache          
*path*             
description : Path for local cache in database mode. Default location is used if omitted.

#### -local_cache_timeout  
timeout                 
description : Timeout in sec for local cache in batch mode. Default 60 sec, 0 for no timeout.

#### -pencil
#### -preRenderScript      
*qt_script_file*
description : use *[the command line] -preRenderScript [the path to your script.js]* to execute a script inside TB before the render

#### -postRenderScript     
*qt_script_file*
description : use *[the command line] -preRenderScript [the path to your script.js]* to execute a script inside TB after the render

#### -preRenderInlineScript        
*inline script string*

#### -postRenderInlineScript       
*inline script string*

#### -readonly                                     
description : (only works in database mode or with -compile/-script)

#### -renderThread         
*number_of_rendering_thread*

#### -repcolor             
*red green blue alpha*

#### -res | -resolution    
*name | res_X res_Y res_Fov*

#### -scene                
*scene*

#### -second

#### -template             
*template_path*

#### -thumbnails                                   
description :  Render template thumbnails (to be used with -template)

#### -user                 
*user*

#### -v

### Database mode
#### -env                  
*environment*             
(database mode)

#### -frames | -comp       
*from_frame to_frame*     
(database mode)

#### -job                  
*job*                     
(database mode)

#### -lockAssets                                   
(database mode)

#### -lockScene                                    
(database mode)

#### -paint                                        
(database mode)

#### -version              
*version*                 
(database mode)
