# OPM Tools

Tools for the "Operation Medinah" enclume project


## Folders
- harmonyPremium_cmd : command line things to use with Toon Boom Harmony Premium
- BG_scripts : scripts usefull for BG team
- TB_scripts : scripts used inside Toon Boom (e.g. : with a button)
- other_scripts : other usefull scrips for the OPM project

## BG_scripts
### confo_bg_psd
photoshop scripts for psd confo files

## TB_scripts
### The config.json file
The file is placed in the TB_scripts folder.
There are variables in this file used by the TB scripts. 
An human user can easly edit this file to adapte the script variables 

### saveAs_newVersion.js
Use the function "save_scene_new_version" to save a new version (incrementally) of the current scene according to the OPM pipe.

### render_cmp_write.js
Use the function "start_cmp_render" to launch the render write node dialogue (with confo_CMP_write_nodes and disable_nonCMP_write_node).

### disable_backdrops.js
Use the function "disable_backdrops" to disables all backdrops whose name is given in the config.json file

## other_scripts
### order_new_item_fromDardj.py
take new shot folders in the "in" directory and put there at the right place 