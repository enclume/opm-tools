import datetime
import os

PLT_DIR = "K:\\EP 100\\_RIGS\\palettes_script"
ROOT_PATH = "K:"

class Log:
    allPrinted = []
    errorL = []
    all_logs = {"logs": allPrinted, "errors": errorL}

    def write(self, path: str) -> None:
        if not os.path.exists(path):
            raise ValueError("given path does not exist")
        
        while True:
            time_info = datetime.datetime.now()
            time_info = time_info.strftime("%Y%m%d_%H-%M-%S")

            path = f"{path}\\_LOGS_{time_info}"
            
            if not os.path.exists(path):
                os.makedirs(path)
                print("write folder created")
                break

        header = f"logs for replace_palettes.py ({time_info})"

        for log in self.all_logs:
            content = "".join(self.all_logs[log])
            if not content: continue
            file = path + f"\\replace_palettes_{log}_{time_info}.txt"

            content = header + "\n" + f"{log.upper()}:\n" + content

            content += f"\n_End of repport\n"

            with open(file, "w") as f:
                f.write(content)

            print(f"{log} write at {file}")
            

    def get_newContent(self, *values, sep: str | None = " ", end: str | None = "\n") -> str:
        newContent = [str(x) for x in values]
        return sep.join(newContent) + end

    def print(self, *values, sep: str | None = " ", end: str | None = "\n") -> str: 
        print(*values, sep=sep, end=end)
        newContent = self.get_newContent(*values, sep=sep, end=end)
        self.allPrinted.append(newContent)
        return newContent
        
    def log(self, *values, sep: str | None = " ", end: str | None = "\n") -> str: 
        return self.print(*values, sep=sep, end=end)

    def error(self, *values, sep: str | None = " ", end: str | None = "\n") -> str:
        newContent = self.print(*values, sep=sep, end=end)
        self.errorL.append(newContent)
        return newContent
    
log = Log()

class PltLineId:
    def __init__(self, idtf: list[str] | None = None) -> None:
        self.idtf: list[str] = []

        if idtf:
            for item in idtf:
                self.append(item)

    def append(self, item: str) -> None:
        self.idtf.append(item)

    def __eq__(self, other: object) -> bool:
        if type(self) != type(other):
            return False
        
        if len(self.idtf) != len(other.idtf):
            return False
        
        for item in self.idtf:
            if item not in other.idtf:
                return False

        return True
    

class PltLine:
    def __init__(self, string_line: str) -> None:
        self.string_line = string_line
        self.identifiers = None
        self.rgba: list[str] = []

        self.is_valid = self.convert_string_line(self.string_line)

    
    def __str__(self) -> str:
        valid = "valid"
        if not self.is_valid:
            valid = "not " + valid

        return f"PltLine ({valid}): " + self.string_line.strip()
        return "PltLine: " + " ".join(self.identifiers.idtf) + " | rgba(" + ", ".join(self.rgba) + ") -> " + valid

    def convert_string_line(self, str_line: str) -> bool:
        str_line = str_line.strip()
        self.identifiers = PltLineId()
        self.rgba: list[int] = []

        splitted: list[str] = []
        item = None
        for char in str_line:
            if char in [" "]:
                if item != None: splitted.append(item)
                item = None
                continue

            if item == None: item = ""
            item += char

        if item:
            splitted.append(item)
            item = None

        if len(splitted) < 6: 
            return False
        
        for item in splitted[-4:]:
            if not item.isdigit():
                return False
            self.rgba.append(str(int(float(item))))

        for item in splitted[:-4]:
            self.identifiers.append(str(item))
            
        return True



class Plt:
    def __init__(self, name: str, dir_path: str) -> None:
        self.name: str = name
        self.dir_path: str = dir_path
        self.path: str = self.dir_path + "\\" + self.name

        with open(self.path) as f:
            self.content = f.readlines()

        self.lines: list[PltLine] = self.get_plt_content(plt_fcontent=self.content)
        

    def __str__(self) -> str:
        return f"palette \"{self.name}\" ({len(self.lines)} rgba lines)"

    def get_plt_content(self, *, plt_file: str | None = None, plt_fcontent: list[str] | None = None) -> list[PltLine]:
        if plt_fcontent:
            content = plt_fcontent 
        elif not plt_file:
            content = self.content
        else:
            with open(plt_file) as f:
                content = f.readlines()

        lines = []

        for line in content:
            if line.isspace(): continue
            if line.startswith("ToonBoomAnimationInc "): continue

            plt_line = PltLine(string_line=line)
            #print(plt_line)
            
            if not plt_line.is_valid: continue
            lines.append(plt_line)

        return lines
    

    def __eq__(self, other: object) -> bool:
        if type(self) != type(other):
            return False
        
        if "".join(other.content) == "".join(self.content):
            return True
        
        # if len(other.lines) != len(self.lines):
        #     return False
        
        for sline in self.lines:
            for oline in other.lines:
                if sline.identifiers == oline.identifiers:
                    if sline.string_line != oline.string_line:
                        return False
                    break
        
        return True


    def write_plt(self, badFile: str) -> (str | None):
        """
        args:
            - badFile: the path to the file to modify
        """

        try:
            with open(badFile) as f:
                badContent = f.readlines()
        except Exception as e:
            error = f"can\'t open the file ({badFile}) because of: {e}"
            log.error(error)
            return error

        if badContent == self.content:
            log.print("palette equal")
            return
        
        baddir = badFile.split("\\")
        badname = baddir.pop(-1)
        baddir = "\\".join(baddir)
        
        badplt = Plt(badname, baddir)

        if badplt == self:
            log.print("palette equal")
            return


        nbr_replaced = 0

        for sline in self.lines:
            for i, bline in enumerate(badContent):
                if not bline.startswith(sline.identifiers.idtf[0]): continue
                if sline.string_line == bline: continue
                bline = PltLine(bline)
                if sline.identifiers != bline.identifiers: continue
                
                #log.print("replace:", badContent[i].strip(), "->", sline.string_line.strip())

                #badContent[i] = sline.string_line.strip() + " (replaced)\n"
                badContent[i] = sline.string_line
                nbr_replaced += 1

        log.print(f"palette not equal, overwrite lines (nbr: {nbr_replaced})")
        
        #return #before writing for dev purpose

        try:
            with open(badFile, "w") as f:
                f.writelines(badContent)
        except Exception as e:
            error = f"can\'t write the file ({badFile}) because of: {e}"
            log.error(error)
            return error




def process_palette(plt: Plt):
    log.print(plt)
    ind = "  "

    for ep in os.listdir(ROOT_PATH):
        indent = ind
        if not (ep.startswith("EP ") and ep[3:].isdigit() and len(ep[3:]) == 3):
            continue

        ep_path = ROOT_PATH + "\\" + ep
        if not os.path.isdir(ep_path): continue

        log.print(f"{indent}ep dir:", ep)

        all_scene_path = ep_path + "\\3_SCENES HARMONY"
        if not os.path.exists(all_scene_path): 
            log.print(f"{indent}there is no 3_SCENES HARMONY folder\n")
            continue
        
        no_plt_dir = []
        palette_not_in = []

        for sh in os.listdir(all_scene_path):
            indent = ind + ind
            if not (sh.startswith("OPM_")):
                continue

            sh_path = all_scene_path + "\\" + sh
            if not os.path.isdir(sh_path): continue

            sh_plt_path = sh_path + "\\palette-library"
            if not os.path.exists(sh_plt_path): 
                no_plt_dir.append(sh)
                continue
            
            if plt.name not in os.listdir(sh_plt_path):
                palette_not_in.append(sh)
                continue
            
            log.print(f"{indent}->", sh, end=": ")

            #TODO look if there is the same palette inside

            #TODO replace content
            plt.write_plt(f"{sh_plt_path}\\{plt.name}")

        log.print()
        if no_plt_dir:
            log.print("shot visited, but palette-library folder not present:")
            log.print(no_plt_dir)
            log.print()

        if palette_not_in:
            log.print("shot visited, but palette not present:")
            log.print(palette_not_in)
            log.print()


def main():
    try:
        for name in os.listdir(PLT_DIR):
            if not name.endswith(".plt"): continue
            if os.path.isdir(f"{PLT_DIR}\\{name}"): continue
            plt = Plt(name, PLT_DIR)
            process_palette(plt)
    except Exception as e:
        error = f"error when executing main: {e}"
        log.error(error)


def write_logs():
    try:
        log.write(PLT_DIR)
    except Exception as e:
        input("error at writting logs:", e)


if __name__ == "__main__":
    os.system("cls")
    print("___START___\n")

    main()
 
    write_logs()
    
    print("\n____END____")