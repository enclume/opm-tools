import os
import shutil

root_path = "K:"

"""K:\\EP 101\\3_SCENES HARMONY"""

def get_last_def_render(sh_path: str):
    render_path = f"{sh_path}\\frames"
    if not os.path.exists(render_path): return

    render_file = [-1, None]
    for item in os.listdir(render_path):
        if not (item.startswith("OPM_") and item.endswith("_DEF.mov")): continue
        item_v = item.split("_")[-2][1:]
        try:
            item_v = int(float(item_v))
        except Exception as e:
            space = "   "
            print(f"error for item \"{item}\". Can\'t find the version ?")
            print(f"{space}(-> look for a version right before the \"_DEF.mov\")")
            print(f"{space}(-> here, find \"{item_v}\"")
            print(f"{space}(-> error code: {e}")
            continue

        if item_v > render_file[0]:
            item_path = f"{render_path}\\{item}"
            render_file = [item_v, item_path]

    return render_file[-1]


def copy_render_to_collector(render_file: str, collector_path: str):
    render_name = render_file.split("\\")[-1]
    collector_name = collector_path.split("\\")[-1]

    if render_name in os.listdir(collector_path):
        print(" ", f"{render_name} already in", collector_name)
        return
    
    print(" ", render_name, "-> copying in", collector_name)

    try:
        shutil.copy2(render_file, collector_path)
    except Exception as err:
        print(f"ERROR, can't copy because of : {err}")


def loop_through_ep(ep: str, ep_path: str):
    print(f"_{ep}:")

    scenes_path = f"{ep_path}\\3_SCENES HARMONY"
    if not os.path.exists(scenes_path): 
        print(" ", "no \"3_SCENES HARMONY\" folder here\n")
        return
    
    renders_folder = f"{scenes_path}\\_rendu_DEF"
    if not os.path.exists(renders_folder):
        print(" ", "create \"_rendu_DEF\" folder")
        os.makedirs(renders_folder)

    for sh in os.listdir(scenes_path):
        if not sh.startswith("OPM_"): continue
        sh_path = f"{scenes_path}\\{sh}"

        if not (os.path.exists(sh_path) and os.path.isdir(sh_path)): continue

        last_render = get_last_def_render(sh_path)
        if not last_render:
            print(" ", f"{sh}: no render file")
            continue
        copy_render_to_collector(last_render, renders_folder)

    print()


def loop_through_root(root_path: str):
    for ep in os.listdir(root_path):
        if not (ep.startswith("EP ") and ep[3:].isdigit()): continue

        ep_path = f"{root_path}\\{ep}"
        if not (os.path.exists(ep_path) and os.path.isdir(ep_path)): continue

        loop_through_ep(ep, ep_path)


def main():
    loop_through_root(root_path)

        










if __name__ == "__main__":
    os.system("cls")
    print("___START___\n")


    try:
        main()
    except Exception as err:
        input(f"Plantage de script, y\'a un p\'tit problème :\n{err}")


    print("\n____END____")