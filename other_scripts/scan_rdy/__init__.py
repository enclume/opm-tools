import gazu
import getpass
import os


root_path = "K:\\"

def get_tbh_path(ep_path):
    return f"{ep_path}\\3_SCENES HARMONY"

def get_psd_path(ep_path):
    return f"{ep_path}\\4_CONFO_BG"


def connect_to_gz(host: str, username: str, password: str):
    """description TODO"""
    gazu.set_host(host) #kitsu

    try: 
        gazu.log_in(username, password) #kitsu
    except:
        print("not connected to kitsu" )
        return False

    gazu.cache.enable() #kitsu

    print("connected to kitsu")
    return True


def update_matches(matches: dict, ep: str, sh: str = None, tbh: str = None, psd: str = None):
    #print("update_matches:", ep, sh, tbh, psd)
    if not ep in matches:
        matches.update({ep: {}})
    
    if not sh: return
    if not sh in matches[ep]:
        matches[ep].update({sh: [None, None]})

    if tbh: 
        matches[ep][sh][0] = tbh
    if psd: 
        matches[ep][sh][1] = psd


def get_tbh_data(name: str):
    name = name.split("_")
    ep = name[1]
    sh = name[2][2:]
    if name[-1] != name[2]:
        sh += name[-1]
    return ep, sh


def get_psd_data(name: str):
    """OPM101_BGC_SH001_chambreMalekInt_CONFO.psd"""
    """OPM101_BGC_SH032A_alger_baie_V02.psb_CONFO.psd"""
    name = name.split("_")
    ep = name[0][-3:]
    sh = name[2][2:]
    return ep, sh


def get_all_tbh(tbh_path: str, matches: dict):
    for item in os.listdir(tbh_path):
        if not item.startswith("OPM_"): continue

        item_path = f"{tbh_path}\\{item}"
        if not os.path.isdir(item_path): continue
        if not os.listdir(item_path): continue

        ep, sh = get_tbh_data(item)
        if not(ep and sh):
            print(f"error for {item}: need both ep and sh (ep: {ep}, sh: {sh})")
            continue

        update_matches(matches, ep, sh, tbh=item_path)


def get_all_psd(psd_path: str, matches: dict):
    for item in os.listdir(psd_path):
        if not item.startswith("OPM"): continue
        if not item[-4:] in [".psd", ".psb"]: continue
        if not "CONFO" in item: continue

        item_path = f"{psd_path}\\{item}"
        if not os.path.isfile(item_path): continue

        ep, sh = get_psd_data(item)
        if not(ep and sh):
            print(f"error for {item}: need both ep and sh (ep: {ep}, sh: {sh})")
            continue

        update_matches(matches, ep, sh, psd=item_path)


def get_gzproject_name():
    return "OPERATION MEDINAH"

def get_gzproject():
    return gazu.project.get_project_by_name(get_gzproject_name())

def get_gzep(ep_nbr: str):
    return gazu.shot.get_episode_by_name(get_gzproject(), f"OPM_{ep_nbr}")

def get_gzsh(gz_ep: dict, sh_nbr: str):
    all_sh = gazu.shot.all_shots_for_episode(gz_ep)
    for sh in all_sh:
        if sh["name"] == f"SH{sh_nbr}":
            return sh
    return None

def get_gztask(entity: dict, task_name: str):
    all_task = gazu.task.all_tasks_for_shot(entity)
    for task in all_task:
        if task["task_type_name"] == task_name:
            return task
    return None


def main():
    host="https://kitsu-opmed.andarta-pictures.com/api"
    connected = False


    while not connected :
        username= input("User: ")
        password= getpass.getpass(prompt="Password (what is written will not be displayed): ")
        for i in range(len(password)): print("*", end="")
        print()
        connected = connect_to_gz(host, username, password)
        if not connected:
            print("\n_Try again ?")

    '''try:
        connect_to_gz(host, username, password)
    except Exception as err:
        print("can't connect to kitsu !")
        print("err:", err)
        input()
        raise Exception(err)'''

    matches = {}
    for ep_dir in os.listdir(root_path):
        if not(ep_dir.startswith("EP ") and ep_dir[-3:].isdigit()): 
            continue

        ep_path = f"{root_path}{ep_dir}"
        tbh_path = get_tbh_path(ep_path)
        psd_path = get_psd_path(ep_path)

        if not os.path.exists(tbh_path):
            #print(f"{ep_dir}: tbh path does not exist ({tbh_path})")
            continue
        if not os.path.exists(psd_path):
            #print(f"{ep_dir}: psd path does not exist ({psd_path})")
            continue

        """ep:{sh:[tbh, psd]}"""

        get_all_tbh(tbh_path, matches) 
        get_all_psd(psd_path, matches)

    all_files = matches.copy()
    matches = []
    m_nbr = 0

    for ep in all_files:
        for sh in  all_files[ep]:
            item_match = all_files[ep][sh]
            if item_match[0] and item_match[1]:
                #print(f"match for EP{ep}_SH{sh}")
                m_nbr += 1
                matches.append([ep, sh, item_match[0], item_match[1]])

    print(f"\n{m_nbr} matches detected !\n")

    for match in matches:
        ep, sh, tbh, psd = match
        #print(match)

        gz_ep = get_gzep(ep)
        gz_sh = get_gzsh(gz_ep, sh)
        gz_task = get_gztask(gz_sh, "PRE-COMP")

        gz_status = gazu.task.get_task_status_by_name(gz_task["task_status_name"])
        gz_RDY = gazu.task.get_task_status_by_short_name("READY")

        alowed_tasks_status = ["todo", "NOT RDY"] #-> go to READY

        if gz_status["short_name"] not in alowed_tasks_status:
            continue

        gazu.task.add_comment(gz_task, gz_RDY)
        print(f"set EP{ep}_SH{sh} to READY")

        """
        PRE-COMP
        Compositing
        """


    

if __name__ == "__main__":
    print("don\'t run this file, but the scan_rdy.py instead")
    input()