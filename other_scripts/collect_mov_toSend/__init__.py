import datetime
import shutil
import gazu
import getpass
import os


root_path = "K:\\"
collector_root_path = root_path + "_ENVOI\\"

def get_tbh_path(ep_path) -> str:
    return f"{ep_path}\\3_SCENES HARMONY"

def get_psd_path(ep_path) -> str:
    return f"{ep_path}\\4_CONFO_BG"

def connect_to_gz(host: str, username: str, password: str):
    """description TODO"""
    gazu.set_host(host) #kitsu

    try: 
        gazu.log_in(username, password) #kitsu
    except:
        print("not connected to kitsu" )
        return False

    gazu.cache.enable() #kitsu

    print("connected to kitsu")
    return True


def update_matches(matches: dict, ep: str, sh: str = None, tbh: str = None, psd: str = None):
    #print("update_matches:", ep, sh, tbh, psd)
    if not ep in matches:
        matches.update({ep: {}})
    
    if not sh: return
    if not sh in matches[ep]:
        matches[ep].update({sh: [None, None]})

    if tbh: 
        matches[ep][sh][0] = tbh
    if psd: 
        matches[ep][sh][1] = psd


def get_tbh_data(name: str):
    name = name.split("_")
    ep = name[1]
    sh = name[2][2:]
    if name[-1] != name[2]:
        sh += name[-1]
    return ep, sh


def get_psd_data(name: str):
    """OPM101_BGC_SH001_chambreMalekInt_CONFO.psd"""
    """OPM101_BGC_SH032A_alger_baie_V02.psb_CONFO.psd"""
    name = name.split("_")
    ep = name[0][-3:]
    sh = name[2][2:]
    return ep, sh


def get_all_tbh(tbh_path: str, matches: dict):
    for item in os.listdir(tbh_path):
        if not item.startswith("OPM_"): continue

        item_path = f"{tbh_path}\\{item}"
        if not os.path.isdir(item_path): continue
        if not os.listdir(item_path): continue

        ep, sh = get_tbh_data(item)
        if not(ep and sh):
            print(f"error for {item}: need both ep and sh (ep: {ep}, sh: {sh})")
            continue

        update_matches(matches, ep, sh, tbh=item_path)


def get_all_psd(psd_path: str, matches: dict):
    for item in os.listdir(psd_path):
        if not item.startswith("OPM"): continue
        if not item[-4:] in [".psd", ".psb"]: continue
        if not "CONFO" in item: continue

        item_path = f"{psd_path}\\{item}"
        if not os.path.isfile(item_path): continue

        ep, sh = get_psd_data(item)
        if not(ep and sh):
            print(f"error for {item}: need both ep and sh (ep: {ep}, sh: {sh})")
            continue

        update_matches(matches, ep, sh, psd=item_path)


def get_gzproject_name():
    return "OPERATION MEDINAH"

def get_gzproject():
    return gazu.project.get_project_by_name(get_gzproject_name())

def get_gzep(ep_nbr: str):
    return gazu.shot.get_episode_by_name(get_gzproject(), f"OPM_{ep_nbr}")

def get_gzsh(gz_ep: dict, sh_nbr: str):
    all_sh = gazu.shot.all_shots_for_episode(gz_ep)
    for sh in all_sh:
        if sh["name"] == f"SH{sh_nbr}":
            return sh
    return None

def get_gztask(entity: dict, task_name: str):
    all_task = gazu.task.all_tasks_for_shot(entity)
    for task in all_task:
        if task["task_type_name"] == task_name:
            return task
    return None


def get_matches() -> dict:
    all_files = {}
    for ep_dir in os.listdir(root_path):
        if not(ep_dir.startswith("EP ") and ep_dir[-3:].isdigit()): 
            continue

        ep_path = f"{root_path}{ep_dir}"
        tbh_path = get_tbh_path(ep_path)
        psd_path = get_psd_path(ep_path)

        if not os.path.exists(tbh_path):
            #print(f"{ep_dir}: tbh path does not exist ({tbh_path})")
            continue
        if not os.path.exists(psd_path):
            #print(f"{ep_dir}: psd path does not exist ({psd_path})")
            continue

        """ep:{sh:[tbh, psd]}"""

        get_all_tbh(tbh_path, all_files) 
        get_all_psd(psd_path, all_files)

    matches = []
    m_nbr = 0

    for ep in all_files:
        for sh in  all_files[ep]:
            item_match = all_files[ep][sh]
            if item_match[0] and item_match[1]:
                m_nbr += 1
                matches.append([ep, sh, item_match[0], item_match[1]])

    print(f"\n{m_nbr} matches detected !\n")
    return matches


def get_all_wfa(ep_dir) -> list[str]:
    """get all wfa and ok-lead shots for an episode (the ok-lead ones are set to wfa)"""
    ep_nbr = ep_dir[-3:]
    shots = []

    gz_ep = get_gzep(ep_nbr)
    all_gz_sh = gazu.shot.all_shots_for_episode(gz_ep)

    gz_WFA = gazu.task.get_task_status_by_short_name("wfa")
    # gz_OKL = gazu.task.get_task_status_by_short_name("OK LEAD")

    for gz_sh in all_gz_sh:
        gz_sh_name: str = gz_sh["name"]

        if len(gz_sh_name) > 5:
            gz_sh_name = gz_sh_name[:5] + "_" + gz_sh_name[5:]        

        gz_task = get_gztask(gz_sh, "Compositing")
        gz_status = gazu.task.get_task_status_by_name(gz_task["task_status_name"])
        

        if gz_status["short_name"] not in ["wfa", "OK LEAD"]:
            continue
        
        '''if gz_status["short_name"] == "OK LEAD":
            #gazu.task.add_comment(gz_task, gz_WFA)
            print(f"set EP{ep_nbr}_{gz_sh_name} to wfa")'''

        shots.append(gz_sh_name)
        '''break #TODO to remove (dev thing)'''
    
    return shots


def get_mov_path(sh_path: str) -> (str | None):
    frames_path = sh_path + "\\frames"
    if not os.path.exists(frames_path):
        print(f"no \"frame\" folder for \"{sh_path}\"")
        return
    
    all_def = []
    for item in os.listdir(frames_path):
        item_path = frames_path + "\\" + item
        if not os.path.isfile(item_path):
            continue
        if not item.endswith("DEF.mov"):
            continue
        all_def.append(item_path)

    if not all_def:
        return
    
    all_def.sort()
    return all_def[-1]


def get_collDir():
    while True :
        timeInfo = datetime.datetime.now()
        timeInfo = timeInfo.strftime("%Y%m%d_%H-%M-%S")

        collDir = collector_root_path + f"collect_mov_{timeInfo}"

        if not os.path.exists(collDir):
            return timeInfo, collDir

def copy_mov(mov_path: str, collEp_path: str):
    shutil.copy2(mov_path, collEp_path)


def main():
    if not os.path.exists(root_path):
        print(f"the path \"{root_path}\" does not exist, abort")
        input()
        return
    
    if not os.path.exists(collector_root_path):
        os.makedirs(collector_root_path)

    host="https://kitsu-opmed.andarta-pictures.com/api"
    connected = False

    while not connected :
        username= input("User: ")
        password= getpass.getpass(prompt="Password (what is written will not be displayed): ")
        for i in range(len(password)): print("*", end="")
        print()
        connected = connect_to_gz(host, username, password)
        if not connected:
            print("\n_Try again ?")

    '''try:
        connect_to_gz(host, username, password)
    except Exception as err:
        print("can't connect to kitsu !")
        print("err:", err)
        input()
        raise Exception(err)'''
    
    print()

    to_copy: dict[str, list[str]] = {}
    mov_nbr = 0

    for ep_dir in os.listdir(root_path):
        if not(ep_dir.startswith("EP ") and ep_dir[-3:].isdigit()): 
            continue

        ep_path = f"{root_path}{ep_dir}"
        tbh_path = get_tbh_path(ep_path)

        if not os.path.exists(tbh_path):
            #print(f"{ep_dir}: tbh path does not exist ({tbh_path})")
            continue

        """ep:{sh:[tbh, psd]}"""

        print(f"{ep_dir} ({tbh_path})")
        shots = get_all_wfa(ep_dir)



        """COLLECT ALL .MOV"""
        ep_nbr = ep_dir[-3:]

        collEpDir = f"OPM{ep_nbr}"

        for sh in shots:
            tbh_name = f"OPM_{ep_nbr}_{sh}"
            sh_path = tbh_path + "\\" + tbh_name
            if not os.path.exists(sh_path):
                continue

            mov_path = get_mov_path(sh_path)

            #print(mov_path)

            if mov_path is None:
                continue
            
            if collEpDir not in to_copy:
                to_copy.update({collEpDir: []})

            to_copy[collEpDir].append(mov_path)
            mov_nbr += 1


        '''if mov_nbr > 0:
            break #TODO to remove (dev thing)'''
    
    
    print(f"\nkitsu collect finished: {mov_nbr} .mov to copy")


    timeInfo, collDir = get_collDir()

    print()
    
    for ep in to_copy:
        collEpDir = collDir + "\\" + ep
        if not os.path.exists(collEpDir):
            os.makedirs(collEpDir)
        for mov_path in to_copy[ep]:
            copy_mov(mov_path, collEpDir)
            print("copying:", mov_path, "->", collEpDir)
    

if __name__ == "__main__":
    print("don\'t run this file, but the collect_mov_toSend.py instead")
    input()