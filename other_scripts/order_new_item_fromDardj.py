version = "1.3 | 2024-03-08"

import datetime
import os
import shutil
import zipfile



"""
S:\COMPO\TO_ENCLUME\[EP101]\02_PLANS_ANIMES_FROM_ANDARTA\[20240308]

S:\COMPO\TO_ENCLUME\ dossier d'épisode \02_PLANS_ANIMES_FROM_ANDARTA\ sous dossier d'arrivée
"""



#root_dir = "K:\\" #Old stuff
fromdir = "S:\\COMPO\\TO_ENCLUME"
todir = "K:"
logs = {"info": [], "error": []}

def get_from_ep_dir(ep_dir: str):
    return ep_dir + "\\02_PLANS_ANIMES_FROM_ANDARTA"


def get_ep_dir(ep: int) -> str:
    """get the episode folder path"""
    return f"{todir}\\EP {ep:03d}"


def get_shot_name(ep: int, sh: int, after: str = "") -> str:
    """get the shot name"""
    return f"OPM_{ep:03d}_SH{sh:03d}{after}"


def get_sh_dir(ep: int, sh: int, after: str = "") -> str:
    """get the shot folder path"""
    ep_dir = get_ep_dir(ep)
    sh_name = get_shot_name(ep, sh, after)
    return f"{ep_dir}\\3_SCENES HARMONY\\{sh_name}"


def get_all_numbers(string: str, getafter: bool = True):
    """get all numbers in a string (as a list of int)"""
    nbrs = []
    current = None
    after = ""

    for char in string:
        if char.isdigit():
            if current is None:
                current = ""
            current += char
            after = ""

        else:  
            if current is not None:
                nbrs.append(int(current))
                current = None
            after += char

    if current is not None :
        nbrs.append(int(current))
        current = None

    if getafter :
        return nbrs, after
    return nbrs


def get_shot_data(shot_name: str):
    """get shot data (episode number, shot number)"""
    nbrs, after = get_all_numbers(shot_name)

    if len(nbrs) < 2 :
        #print(f"get_shot_data : can\'t get data from \"{shot_name}\" ->(nbrs: {nbrs})")
        return False
    ep = nbrs[0]
    sh = nbrs[1]
    return {"ep": ep, "sh": sh, "after": after}


def unzip(pathin: str, pathout: str, item_name: str):
    """Unzip a file at a path to an other path. Add a folder nammed with given item name if there are more than one folder/file at the zip root"""
    with zipfile.ZipFile(pathin, 'r') as zObject:  
        atroot = []
        for item in zObject.namelist():
            item_path = item.split("/")
            while "" in item_path : item_path.remove("")
            if len(item_path) == 1:
                atroot.append(item)
            if len(atroot) > 1:
                break
            
        if not atroot :
            return

        if len(atroot) > 1 or not atroot[0].endswith("/"):
            pathout += f"\\{item_name}"

        zObject.extractall(path=pathout) 


def process_item(item: str, pdir):
    current_name = item
    current_path = pdir + "\\" + item 
    unzip_path = None
    if not os.path.isdir(current_path):            
        if not item.endswith(".zip"):
            error = f"bad file format, only support \".zip\" or folder (item : {item})"
            return error
        current_name = current_name[:-4]
        unzip_path = True

    name_data = get_shot_data(current_name)
    if name_data == False :
        error = f"can't get numeric data from name (item : {item})"
        return error
    
    pipe_name = get_shot_name(**name_data)
    pipe_path = get_sh_dir(**name_data)

    if current_name != pipe_name:
        error = f"the item name is not in pipe (name : {current_name}, in pipe name : {pipe_name})" 
        return error
    
    if os.path.exists(pipe_path):
        error = f"the item already exist on the server (item : {item}, exist at \"{pipe_path}\")"
        return error

    print("\npipe_name    :", pipe_name)
    print("current_path :", current_path)
    print("pipe_path    :", pipe_path)
    print("to unzip     :", bool(unzip_path))

    if unzip_path:
        unzip_path = "\\".join(pipe_path.split("\\")[:-1])
        try:
            unzip(current_path, unzip_path, pipe_name)
        except Exception as err:
            error = f"can't unzip the item because of: {err} (current_path: {current_path}, unzip_path: {unzip_path})"
            return error
        print("file unziped")
    else:
        shutil.copytree(src=current_path, dst=pipe_path)
        print("file copied")
        
    return False


def write_logs(logs: dict):
    logsInfo: list = logs["info"]
    logsError: list = logs["error"]
    
    logtime = datetime.datetime.now()
    logtimename = logtime.strftime("%Y-%m-%d_%Hh%M-%Ss")
    logname = f"LOGS_order_new_item_fromDardj_{logtimename}"
    logsInfo.insert(0, f"\n___Info(s):")
    logsInfo = "\n".join(logsInfo)

    if logsError:
        logsError.insert(0, f"\n___Error(s):")
        logsError = "\n".join(logsError)
    else:
        logsError = "\n___Error(s):\nNothing"

    logcontent = [f"Logs from order_new_item_fromDardj.py (execution time : {logtime})", logsInfo, "", logsError, "\nlogs end\n\n\n"]
    logcontent = "\n".join(logcontent)

    logpath = fromdir + "\\" + logname + ".txt"

    #print("logs : ")
    #print(logcontent)

    with open(logpath, "a") as f :
        f.write(logcontent)

    print(f"\nwrite logs ->({logpath})")


def loop_in_subitems(item: str, item_path: str, logs: dict, go_deeper = False):
    count = 0
    error_count = 0
    for subitem in os.listdir(item_path):
        if "PLANS" in subitem and go_deeper:
            loop_in_subitems(f"{item}\\{subitem}", f"{item_path}\\{subitem}", logs)
            continue
        
        error = process_item(subitem, item_path)
        count += 1
        if not error : continue
        error_count += 1
        error_line = f"{item}\\{subitem} : {error}"
        print("\nAbort |", error_line)
        logs["error"].append(error_line)
    
    smallitem_path = item_path.split("\\")
    while not smallitem_path[0].startswith("EP"): smallitem_path.pop(0)
    smallitem_path[1] = smallitem_path[1][:5] + "..." + smallitem_path[1][-5:]
    smallitem_path = "\\".join(smallitem_path)
    logs["info"].append(f"{smallitem_path} : Traité    ({count} item(s), {error_count} error(s))")


def main():
    for ep_item in os.listdir(fromdir):
        ep_item_path = fromdir + "\\" + ep_item
        if not os.path.isdir(ep_item_path): continue
        if not (ep_item.startswith("EP") and ep_item[2:].isdigit() and len(ep_item[2:]) == 3):
            error = f"Bad naming for the folder \"{ep_item}\" (should be \"EPxxx\", with \"x\" as digit)"
            print(error)
            continue    

        print(f"___{ep_item}:")

        itemsFolderPath = get_from_ep_dir(ep_item_path)

        for item in os.listdir(itemsFolderPath):
            if "rangé" in item or "range" in ep_item : continue

            item_path = itemsFolderPath + "\\" + item
            if not os.path.isdir(item_path): continue

            print(f"start processing items in {item}")
            print(f"({item_path})")

            loop_in_subitems(item, item_path, logs, True)

            print(f"{item_path} is now rangé !")
            os.rename(item_path, f"{item_path}_rangé")
            print(f"-> ({item_path}_rangé)")

        print()



if __name__ == "__main__":
    
    os.system("cls")
    print("___START___\n")
    try:
        main()
    except Exception as err:
        input(f"Plantage de script, y\'a un p\'tit problème :\n{err}")

    #unzip("O:\\_COMPOSITING\\_FROM DARJEELING\\testzip.zip", "O:\\_COMPOSITING\\_SCRIPTS", "testest")
    write_logs(logs)

    print("\n____END____")