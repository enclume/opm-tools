# TODO
- Script confo PSD
- Traitement des flux de sortie
- Gestion/classement interne


## Script confo PSD
### function generic
delete(toKeep: list[str]) -> non récursif, juste pour les tops layers
mergeGroup()
ungroup()
replaceInName(old: str, new: str)

### line
keep [FIELD, CH, BG LINE] and delete others
si groupes in field -> merge groupe par groupe and rename
sinon -> merge all layers in field and rename

ungroup([FIELD, BG LINE])
replaceInNames(" ", "_")

### color
keep [BG COLOR] and delete others
ungroup([BG COLOR])
replaceInNames(" ", "_")
all names startswith ["BG_", "FX_"]


## Gestion/classement interne
-> order_valide_bg.py


## Traitement des flux de sortie
TODO


