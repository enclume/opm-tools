version = "0.1 | 2024-03-07"

import datetime
import json
import os
import shutil

root = "O:\\"

configFile = root + "_SCRIPTS\\opm-tools\\BG_scripts\\config_bg.json"

fromDir = root + "_PROD BG\\4.BG VALIDES A CLASSER"
fromDir_colo = fromDir + "\\COLO"
fromDir_line = fromDir + "\\LINE"

toDir = root + "_BANQUE BG"
toDir_colo = toDir #+ "\\COLO"
toDir_line = toDir #+ "\\LINE"
cat_colo = "COLO"
cat_line = "LINE"

#for test purpose
fromDir_linetest = fromDir + "\\LINEtest"
toDir_linetest = toDir + "\\LINEtest"


def raiseCriticalError(errors: list):
    print("Critical error, stop script execution:")
    print("\n".join(errors))
    input("\n[ENTER] > close the script")
    raise Exception("Critical error")



def verifAllPath():
    allPath = [configFile, fromDir_colo, fromDir_line]
    errors = []

    for path in allPath:
        if os.path.exists(path): continue
        errors.append(f"ERROR (verifAllPath): path does not exist (path: {path})")

    if errors: raiseCriticalError([errors])


def get_config(path) -> dict:
    with open(path) as f:
        content = f.read()

    try:
        content = json.loads(content)["order_valide_bg"]
    except Exception as err:
        error = f"crash in get config: {err}"
        raiseCriticalError([error])
    return content


def find_correspondance(item: str, config: dict) -> str:
    item_corrs: list[dict] = []
    corresp = config["correspondances"]
    for key in corresp:
        if key in item:
            item_corrs.append({key: corresp[key]})

    if not item_corrs:
        error = "no correspondance found for this item"
        print(f"    {error}")
        return False, error
    
    if len(item_corrs) > 1:
        error = f"multiple correspondances found for this item ({item_corr})"
        print(f"    {error}")
        return False, error
    
    item_corr: dict = item_corrs[0]
    key = list(item_corr.keys())[0]
    value = list(item_corr.values())[0]

    print(f"    correspondance: {key} -> {value}")
    return True, value


def order_items(fromdir: str, todir: str, cat: str, config: dict):
    print("order item in \"", "\\".join(fromdir.split("\\")[-2:]), "\"", sep="")

    errors = []

    for item in os.listdir(fromdir):
        if item in ["Thumbs.db"]: continue
        print("\n ->", item)
        valid, corresp = find_correspondance(item, config)

        if not valid:
            errors.append(f"ERROR for {item}: {corresp}")
            continue

        copyfrom = fromdir + "\\" + item
        copyTo = todir + "\\" + corresp + "\\" + cat
        if not os.path.exists(copyTo):
            os.makedirs(copyTo)

        print(f"    copy item from {fromdir}")
        print(f"                to {copyTo}")

        try:
            shutil.copy2(copyfrom, copyTo)
            os.remove(copyfrom)
            print("    file copied and removed from source directory")
        except Exception as e:
            errors.append(f"ERROR for {item}: can't finalize copy and remove because of: {e}")
            print(f"    can't finalize copy and remove because of:\n    {e}")

    return errors


def write_logs(logs: list):
    if not logs : return
    
    logtime = datetime.datetime.now()
    logtimename = logtime.strftime("%Y-%m-%d_%Hh%M-%Ss")
    logname = f"LOGS_order_valide_bg_{logtimename}"
    logs.insert(0, f"Logs from order_valide_bg.py (execution time : {logtime})\n")
    logs.append("\nlogs end\n\n\n")
    logcontent = "\n".join(logs)

    logpath = fromDir + "\\" + logname + ".txt"

    with open(logpath, "a") as f :
        f.write(logcontent)

    print(f"\nwrite logs ->({logpath})")


def main():
    verifAllPath()
    config = get_config(configFile)
    errors = []
    errors.extend(order_items(fromDir_colo, toDir_colo, cat_colo, config))
    errors.extend(order_items(fromDir_line, toDir_line, cat_line, config))
    #errors.extend(order_items(fromDir_linetest, toDir_linetest, config))

    write_logs(errors)
    


if __name__ == "__main__":
    os.system("cls")
    print(f"___START order_valide_bg.py (version: {version})\n")
    try:
        main()
    except Exception as err:
        error = f"Crash in main function: {err}"
        raiseCriticalError([error])
    print("\n___END of execution\n")
   