# ***** BEGIN GPL LICENSE BLOCK *****
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
# ***** END GPL LICENCE BLOCK *****
#
# (c) 2023, Paul Jadoul, Cedric Nicolas,  Guillaume Geelen

'''
Description TODO
'''

import os.path
from typing import Union

import libs.database_access as database_access
import libs.dependencies as dep
import libs.logger as logger
import libs.config as config

# set logger
log = logger.get_logger()


if not dep.is_dep_in_path():
    try:
        import gazu #kitsu
        database_access.utils.print_framed("Gazu is already loaded, Be carefull")
    except:
        pass

if not dep.is_dep_in_path():
    dep.add_dep_in_path()


def get_supported_version() -> list[int] :
    return [0, 9, 11]

#kitsu
def get_current_version() -> list[int] :
    import gazu #kitsu
    version = gazu.__version__
    version = version.split(".")
    for i, item in enumerate(version):
        version[i] = int(item)
    return version


def is_bridge_current_version_supported() -> bool :
    ''' test if the imported gazu python api is supported  '''

    current_v = get_current_version()
    supported_v = get_supported_version()

    if len(current_v) != len(supported_v) :
        log.warning(f"the gazu current and supported versions does'nt have the same length (current: {current_v}, supported: {supported_v}) ")
        return False

    for i, item in enumerate(current_v) :
        if item < supported_v[i] :
            return False
    return True


try :
    import gazu #kitsu
except :
    database_access.db_access_startup()
    import gazu #kitsu

try :
    from ...dependencies import gazu
except:
    pass

#kitsu 
def get_db_current_user() :
    return gazu.client.get_current_user() #kitsu


def is_db_auth() -> bool :
    ''' return True if a db session exist '''
    try:
        get_db_current_user()
        return True
    except:
        return False

#kitsu 
def db_login(host, username, password, use_db_cache = True):
    gazu.set_host(host) #kitsu
    gazu.log_in(username, password) #kitsu
    if use_db_cache :
        gazu.cache.enable() #kitsu

#kitsu 
def db_end_user_session():
    gazu.log_out() #kitsu
    gazu.cache.clear_all() #kitsu

#kitsu 
def gazu_clear_cache():
    gazu.cache.disable() #kitsu
    gazu.cache.clear_all() #kitsu
    gazu.cache.enable() #kitsu

#kitsu 
def gazu_update_cache(use_cache):
    if use_cache :
        gazu.cache.enable() #kitsu
        return
    gazu.cache.disable() #kitsu
    gazu.cache.clear_all() 


#kitsu
def get_project_by_name(project_name: str) -> dict :
    return gazu.project.get_project_by_name(project_name) #kitsu

#kitsu
def get_shot_by_name(seq: str, shot: str):
    project = database_access.get_project()

    seq = gazu.shot.get_sequence_by_name(project, seq) #kitsu
    shot = gazu.shot.get_shot_by_name(seq, shot) #kitsu
    return shot

#kitsu
def get_project() :
    """get the project based on the config project name"""
    project_name = config.get_gzproject_name()
    project = gazu.project.get_project_by_name(project_name) #kitsu
    if not project: raise ValueError(f"Not valid Database Project ({project_name} is not in db) ")
    return project

#kitsu
def get_asset_types_name() -> list :
    '''
    get asset types
    '''

    if not is_db_auth():
        raise Exception(f"ERROR : Not logged to db")
    
    project = get_project() #kitsu
    all_type_raw = gazu.asset.all_asset_types_for_project(project) #kitsu
    all_type = []
    for each in all_type_raw:
        all_type.append(each["name"])
    all_type = sorted(all_type)

    return all_type


#kitsu
def get_assets_name(asset_type :str) -> list :
    '''
    get assets name for a given type
    '''

    if not is_db_auth():
        raise Exception(f"ERROR : Not logged to db")
    
    project = get_project() #kitsu
    type_id = gazu.asset.get_asset_type_by_name(asset_type) #kitsu
    all_names_raw = gazu.asset.all_assets_for_project_and_type(project, type_id) #kitsu
    all_names = []
    for each in all_names_raw:
        all_names.append(each["name"])
    all_names = sorted(all_names)

    return all_names


#kitsu
def get_asset_tasks_name(asset_name: str) -> list :
    '''
    get assets task for a given name
    '''

    if not is_db_auth():
        raise Exception(f"ERROR : Not logged to db")
    
    project = get_project() #kitsu
    asset_id = gazu.asset.get_asset_by_name(project, asset_name) #kitsu

    all_tasks_raw = gazu.task.all_tasks_for_asset(asset_id) #kitsu
    # all_tasks_raw = gazu.asset.all_assets_for_project_and_type(project, type_id)
    all_tasks = []
    for each in all_tasks_raw:
        all_tasks.append(each["task_type_name"])
    all_tasks = sorted(all_tasks)

    return all_tasks

#kitsu
def add_preview_to_db(preview_path: Union[str, list], task_id: dict, 
                      comments: Union[int, str], revision: str = None) :
    ''' 
    add preview(s) to the DB

    Args:
        - preview_path: a path or a list of path of preview file
        - task_id:
        - comments:
        - revision:
    '''
    all_preview_path = preview_path
    if type(preview_path) is str :
        all_preview_path = [preview_path]
        
    for each_preview_path in all_preview_path:
        if not os.path.exists(each_preview_path):
            print(f"Flux : file preview - file NOT exist {each_preview_path=}")
            continue

        gazu_revision = None
        if revision :
            gazu_revision = int(revision)
            print(f"Flux : Force preview revision to {revision} for {each_preview_path}")

        #kitsu
        gazu.task.add_preview(
            task_id,
            comments,
            each_preview_path,
            revision = gazu_revision)

#kitsu
def get_task_id_from_asset(asset_name: str, task_type: str) -> dict :
    project = database_access.get_project()
    task_type_id = gazu.task.get_task_type_by_name(task_type) #kitsu
    asset_id = gazu.asset.get_asset_by_name(project, asset_name) #kitsu
    return gazu.task.get_task_by_name(asset_id, task_type_id) #kitsu

#kitsu
def get_task_id_from_shot(seq: str, shot: str, task_type: str) -> dict :
    project = database_access.get_project()
    task_type_id = gazu.task.get_task_type_by_name(task_type) #kitsu
    seq_id = gazu.shot.get_sequence_by_name(project, seq) #kitsu
    shot_id = gazu.shot.get_shot_by_name(seq_id, shot) #kitsu
    return gazu.task.get_task_by_name(shot_id, task_type_id) #kitsu

#kitsu
def get_task_statut_by_name(status_name: str) :
    status_id = gazu.task.get_task_status_by_short_name(status_name) #kitsu
    if not status_id :
        status_id = gazu.task.get_task_status_by_name(status_name) #kitsu
    if not status_id :
        error = f"Cannot set status_id (status_name : {status_name})"
        log.error(error)
        raise Exception(error)
    return status_id

#kitsu
def add_comment_to_db(task, status, comment):
    return gazu.task.add_comment(task, status, comment) #kitsu


def set_comment_db(current_item: dict, task_status: str, comment: str, 
                   preview_path: Union[str, list] = None, revision: Union[int, str] = None) :
    ''' 
    description TODO 

    Args:
        - current_item: the current item dictionnary
        - task_status: the task status name
        - comment: (in formated way, like comment || v1, r1, etc...)
        - preview_path: if set, the preview(s) will uploaded to kitsu
        - revision:
    '''
    # get status
    status_id = get_task_statut_by_name(task_status)

    task_id = database_access.get_task_id_from_current_item(current_item)

    # set comment
    comments = add_comment_to_db(task_id, status_id, comment)

    if not preview_path : 
        return True

    add_preview_to_db(preview_path, task_id, comments, revision)

#kitsu
def get_comments_db(task_id: dict, only_last: bool, db_reversed: bool = True):
    if only_last :
        comments = [gazu.task.get_last_comment_for_task(task_id)] #kitsu
    else:
        comments = gazu.task.all_comments_for_task(task_id) #kitsu

    if db_reversed : comments.reverse()
    return comments

#kitsu
def get_sequences_name() -> list[str]:
    """
    get all sequence of the projects

    Returns:
        list of str of srq names
    """
    project = database_access.get_project()

    seq_raw = gazu.shot.all_sequences_for_project(project) #kitsu

    all_seq = []
    for each in seq_raw:
        all_seq.append(each["name"])
    all_type = sorted(all_seq)

    return all_seq

#kitsu
def get_shots_name(seq) -> list[str] :
    """
    get all shots for a given seq

    Args:
        seq: 

    Returns:
        list of str: 

    """
    project = database_access.get_project()

    seq = gazu.shot.get_sequence_by_name(project, seq) #kitsu

    shots_raw = gazu.shot.all_shots_for_sequence(seq["id"]) #kitsu
    all_shots = []
    for each in shots_raw:
        all_shots.append(each["name"])
    all_shots = sorted(all_shots)

    return all_shots

#kitsu
def get_shot_tasks_name(seq_name, shot_name, filter: bool = False) -> list[str] :
    """
    get all task for given shot
    Args:
        - seq:
        - shot: 
    """
    shot = get_shot_by_name(seq_name, shot_name)

    tasks = gazu.task.all_tasks_for_shot(shot) #kitsu

    all_tasks = []
    for task in tasks:
        all_tasks.append(task["task_type_name"])
    all_tasks = sorted(all_tasks)

    if not filter :
        return all_tasks
    
    current_item = {"seq": seq_name, "shot": shot_name, "category": "shots", "task": None}
    all_tasks_filtered = []
    for task in all_tasks :
        current_item["task"] = task
        #base_folder = file_access.get_item_path2(current_item, only_base_folder=True)
        base_folder = "" #TODO

        if base_folder and seq_name in base_folder and shot_name in base_folder : #TODO improve this
            all_tasks_filtered.append(task)

    return all_tasks_filtered


#kitsu
def get_shot_infos(seq, shot) -> dict :
    """
    TODO
    Args:
        - seq:
        - shot:

    Returns:
        a dict with all shots infos
    """
    shot = get_shot_by_name(seq, shot)

    if not shot : return False

    return shot['data']

#kitsu
def get_shot_url(seq, shot) -> list[str] :
    ''' get the kitsu url of a shot (to be able to open it in an internet browser) '''

    shot = get_shot_by_name(seq, shot)

    url = gazu.shot.get_shot_url(shot) #kitsu

    return url

#kitsu
def get_asset_url(asset_name: str) -> list[str] :
    ''' get the kitsu url of an asset (to be able to open it in an internet browser) '''

    project = database_access.get_project()

    asset_id = gazu.asset.get_asset_by_name(project, asset_name) #kitsu
    url = gazu.asset.get_asset_url(asset_id) #kitsu

    return url
