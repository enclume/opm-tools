# ***** BEGIN GPL LICENSE BLOCK *****
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
# ***** END GPL LICENCE BLOCK *****
#
# (c) 2023, Paul Jadoul, Cedric Nicolas,  Guillaume Geelen

''' utils functions for database access '''

import json
import os
from datetime import datetime


import libs.logger as logger

log = logger.get_logger()

def print_framed(string: str, frame_item: str = "*", border_width = 3) :
    ''' description TODO '''

    side = ""
    line = ""
    for i in range(border_width) :
        side += frame_item
    for i in range(len(string)) :
        line += frame_item

    line = side + line + side

    final_print = line + "\n" + side + line + side + "\n" + line
    
    print(final_print)
    return final_print


def get_version_from_comment(comment: str) -> dict :
    ''' description TODO '''

    if "||" in comment :
        comment = comment.split("||")[-1]
    comment = comment.split("/")

    version_dict = {}
    for item in comment :
        item = item.split(":")
        version_dict.update({item[0]:int(item[-1])})

    return version_dict


def get_formated_comment(comment: str) -> list :
    """
    get comment from db in formatted way [comment, versionning] for future parse

    Args:
        - comment: comment in string, in format 'comment || v1 / r1 / w1'

    Returns:
        [comment, versionning, revision_from]
    """

    #begin
    comment_part = comment.split("||")

    if len(comment_part) > 2 :
        raise ValueError("Bad comment format")
    if not len(comment_part) > 1 :
        return [comment, None, None]
    
    if not comment_part[1] :
        log.error(f"comment after \"||\" is : {comment_part[1]}")
        return [comment, None, None]
    
    #handle " - From " part
    revision_from = comment_part[1].split(" - From ")

    if len(revision_from) == 2 :
        comment_part[1] = revision_from[0]
        revision_from = revision_from[1]
    else :
        revision_from = None
    
    #handle "versionning" part of the return
    versionning = get_version_from_comment(comment_part[1])

    #handle "revision_from" part of the return
    if revision_from :
        revision_from = revision_from.split(":")[-1]

    return [comment_part[0], versionning, revision_from]


def set_comment_local(local_path: str, status: str, comment: str, user: str = "") :
    ''' 
    write comment on disk 

    Args:
        - status: ...
        - comment: (in formated way, like comment || v1, r1, etc...)
        - user: ...
        - local_path: path where to write
    '''
    now = datetime.now() # dd/mm/YY H:M:S
    time = now.strftime("%Y-%m-%dT%H:%M:%S")
    comment_raw = ["fluxMeta", comment, status, time, user]
    with open(local_path, 'a') as f:
        f.write(json.dumps(comment_raw) + "\n")


def get_comment_local(local_path: str):
    ''' 
    get comment from local disk 

    Args:
        - local_path: the local path to the comment's file
    '''
    if not os.path.exists(local_path) :
        log.error(f"the local path does'nt exist (local_path: {local_path})")
        return
    #print(f"get_comment_local, local_path : {local_path}")
    comments = []
    with open(local_path, 'r') as f:
        for line in f:
            line_data = line.rstrip()
            line_data = json.loads(line_data)
            comments.append(line_data)

    return comments