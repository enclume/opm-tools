import datetime
import shutil
import os

import libs.database_access.shotgun_bridge as sg_db
import libs.database_access.gazu_bridge as gz_db

"""
all tasks: BGL ROUGH, BGL CLEAN, BGC
-> filter by statut: oki

on obtient un item :

item data : 
version_name = OPM102_BG_SH013_rueLyesMalek_BGL CLEAN_v005
task_name = BGL CLEAN
"""

tasks = {"BGL ROUGH": [], "BGL CLEAN": [], "BGC": []}
debug = False

psd_ext = [".psd", "psb"]


def connect_to_sg():
    """description TODO"""
    sg_manager = sg_db.db_login(
        host="https://skermwest.shotgrid.autodesk.com", 
        username="", 
        password="")
    
    print("connected to shotgrid")
    

def connect_to_gz():
    """description TODO"""
    gz_db.db_login(host="https://kitsu-opmed.andarta-pictures.com/api", username="svl95.pro@gmail.com", password="Prod_OPM/5")
    print("connected to kitsu")


def get_tasks_for_statut(task_content, statut_code):
    """
    Args:
        - task_content: "BGL ROUGH, BGL CLEAN, BGC"
        - statut_code: "oki"

    Returns:
        all task with the statut
    """
    filters = [['content', 'is', task_content], ["sg_status_list", "is", statut_code], sg_db.get_project_id_filter()]
    field = None
    field = ['type', 'id', 'sg_status_list', 'content', 'entity', 'project', 'sg_versions', 'step', 'sibling_tasks', 'split_durations', 'cached_display_name']
    #field = ['sg_status_list']
    return sg_db.get_entities("Task", filters, field, False)


def get_last_task_version(task):
    """description TODO

    Args:
        - task: TODO
    """

    versions: list = task["sg_versions"]
    if not versions:
        return False

    v = 0
    last_v = ""

    for version in versions:
        version_name = version['name']
        version_v = int(version_name[-3:])
        if version_v > v :
            last_v = version

    return last_v


def collect_tasks():
    """collect oki last task version in sg"""
    status_oki = sg_db.get_task_statut_by_name("OKI")
    to_return = []
    errors = []
    for task_content in tasks:
        tasks[task_content] = get_tasks_for_statut(task_content, status_oki["code"])
        print(task_content, ":", len(tasks[task_content]), "item(s)")

        for task in tasks[task_content]:
            lastversion = get_last_task_version(task)
            if lastversion is False:
                error = "can't get last sg version for this task: "+task_content+" on "+task["entity"]["name"]
                print(f"ERROR: {error}")
                errors.append(error)
                continue

            to_return.append({
                "sg_task": task,
                "sg_version": lastversion,
                "sg_version_name" : lastversion["name"],
                "sg_task_name": task_content
                })
            
    print()
    return to_return, errors


def get_gz_asset(asset_name):
    """description TODO

    Args:
        - asset_name: TODO
    """

    project = gz_db.get_project()
    asset_type = gz_db.gazu.asset.get_asset_type_by_name('BG')
    return gz_db.gazu.asset.get_asset_by_name(project["id"], asset_name, asset_type["id"])


def get_gz_asset_task(asset, task_name: str):
    """description TODO

    Args:
        - asset: TODO
        - task_name: TODO
    """

    all_task = gz_db.gazu.task.all_tasks_for_asset(asset)
    for task in all_task:
        if task["task_type_name"] == task_name:
            return task
    return None


def get_asset_name(asset_sg_version: str):
    '''get the asset name based on the asset_sg_version '''
    data = asset_sg_version.split("_")
    ep = data[0]
    sh = data[2]
    after = []
    for d in data[3:]: 
        if d.startswith("BG"): break
        after.append(d)
    after = "_".join(after)

    name = [ep, "BG", sh, after]
    return "_".join(name)


def compare_with_comment(version_name, task):
    """vérifier dernier commentaire "WFA" que la version soit bien plus haute > sinon ERREUR"""
    all_comments = gz_db.gazu.task.all_comments_for_task(task)
    
    for comment in all_comments:
        if comment["task_status"]["short_name"] != "wfa": continue
        text = comment["text"]

        if not text: continue

        try:
            if not (text[-4] == "v" and text[-3:].isdigit()): 
                error = f"can't find version in comment (version_name: {version_name}, task: {task})"
                return error
            
            if int(text[-3:]) >= int(version_name[-3:]):
                error = f"sg_version older than kitsu comment version (version_name: {version_name}, task: {task})"
                return error
        except Exception as err:
            error = f"crash in compare_with_comment: {err}"
            return error
        
    return False


def get_comment_data(sg_version_name: str):
    '''get the comment data based on the sg_version name'''
    sg_version_name = sg_version_name.split("_")
    comment_data: list= sg_version_name[:3]
    comment_data.append([])

    for i, item in enumerate(sg_version_name[3:]):
        if item.startswith("BG"): 
            for enditem in sg_version_name[3+i:]:
                comment_data.append(enditem)
            break
        comment_data[3].append(item)
    
    comment_data[3] = "_".join(comment_data[3])

    #print("comment_data:", comment_data)
    return comment_data


def get_preview_dir(comment_data: list):
    '''get the path to the preview folder'''
    path = "O:\\projects\\om\\work\\asset\\"
    path += "_".join(comment_data[:4])
    path += "\\design\\"
    path += comment_data[4]
    path += "\\vault\\"
    path += "_".join(comment_data)
    return path


def get_preview_png(comment_data: list, preview_path: str = None): 
    '''get the file path to the .png corresponding to the given comment_data'''
    if preview_path is None :
        preview_path = get_preview_dir(comment_data)
    fileName = comment_data.copy()
    fileName[1] = "BG"
    fileName.append("preview")
    fileName = "_".join(fileName)

    path = preview_path + "\\" + fileName

    ext = [".png", ".jpg"]
    for ex in ext:
        if os.path.exists(f"{path}{ex}"):
            return f"{path}{ex}"
    return f"{path}.can't find the extention"


def get_preview_psd(comment_data: list, preview_path: str = None): 
    '''get the file path to the .psd file corresponding to the given comment_data'''
    if preview_path is None :
        preview_path = get_preview_dir(comment_data)
    fileName = "_".join(comment_data)

    path = preview_path + "\\" + fileName
    
    ext = [".psd", ".psb"]
    for ex in ext:
        if os.path.exists(f"{path}{ex}"):
            return f"{path}{ex}"
        
    return f"{path}.can't find the extention"


def publish_preview_for_task(item: dict):
    '''publish a preview for the given item task on Kitsu and set the sg & kitsu task statut on wfa'''

    """Uploader la thumb
    Publish revision avec commentaire : [nom de la SG version avec L ou C en fontion de la tache] (virer numéro révision sur Kitsu)
    Changer statut en WFA (sur Kitsu et SG)"""
    '''
    comment = "[nom de la SG version avec L ou C en fontion de la tache]" 
    comment = "OPM102_BG_SH013_rueLyesMalek_BGL CLEAN_v005" #BAD sg_version_name
    comment = "OPM102_BGL_SH013_rueLyesMalek_BGL CLEAN_v005"
    '''
    # get rigth comment
    try:
        comment_data = get_comment_data(item["sg_version_name"])
    except Exception as err:
        error = f"can\'t get comment data (err: {err})"
        return error

    if item["sg_task_name"] == "BGC": comment_data[1] = "BGC"
    else: comment_data[1] = "BGL"

    comment = "_".join(comment_data)

    preview_path = get_preview_dir(comment_data)
    preview_png = get_preview_png(comment_data, preview_path)
    """OPM102_BG_SH021_chambreMalekInt_BGL CLEAN_v004_preview.png"""
    preview_psd = get_preview_psd(comment_data, preview_path)
    """OPM102_BGL_SH021_chambreMalekInt_BGL CLEAN_v004.psd"""

    if not os.path.exists(preview_path):
        error = f"preview folder path does not exist (path: {preview_path})"
        return error
    if not os.path.exists(preview_png):
        error = f"preview png does not exist (path: {preview_png})"
        return error
    if not os.path.exists(preview_psd):
        error = f"psd file does not exist (path: {preview_psd})"
        return error
    
    item.update({"psd_file": preview_psd, "comment": comment})

    if not debug:
        print(f"publish kitsu revision for item (statut: wfa, comment: {comment}, previewFile : {preview_png})")
        wfa_statut = gz_db.gazu.task.get_task_status_by_short_name("wfa")
        gz_db.gazu.task.publish_preview(item["gz_task"], wfa_statut, comment=comment, preview_file_path=preview_png)

    if not debug:
        print("update shotgrid statut")
        status_wfa = sg_db.get_task_statut_by_name("wfa")
        sg_db.update_entity("Task", item["sg_task"]["id"], {'sg_status_list': status_wfa["code"]})


def update_dbitem(item: dict):
    """item = dict 
        - sg_version_name: OPM102_BG_SH013_rueLyesMalek_BGL CLEAN_v005
        - sg_task_name: BGL CLEAN
    """
    #item = {"sg_version_name": "OPM102_BG_SH219_salon_philippe_BGC_v001", "sg_task_name": "BGL CLEAN"}
    item.update({"asset_name": get_asset_name(item["sg_version_name"])})

    gz_asset = get_gz_asset(item["asset_name"])
    gz_task = get_gz_asset_task(gz_asset, item["sg_task_name"])
    gz_task_statut = gz_db.gazu.task.get_task_status(gz_task["task_status_id"])

    print("asset, task, statut: ", item["asset_name"], ", ", gz_task["task_type_name"], ", ", gz_task_statut["short_name"], sep="")

    if gz_task_statut["short_name"] not in ["todo", "RTK", "REUT", "WARNING"]: # -> gz_task_statut=WARNING ?
        error = f"asset task statut not supported (asset: "+item["asset_name"]+", statut: "+gz_task_statut["short_name"]+")"
        error += f""
        print("| Abort |", error)
        return error

    if gz_task_statut["short_name"] in ["RTK", "REUT", "WARNING"]:
        error = compare_with_comment(item["sg_version_name"], gz_task)
        if error:
            print("| Abort |", error)
            return error
        
    item.update({"gz_task": gz_task})
        
    error = publish_preview_for_task(item)
    if error:
        print("| Abort |", error)
        return error
    return


def write_file(path, content):
    """description TODO

    Args:
        - path: TODO
        - content: TODO
    """

    if not os.path.isfile(path) and False:
        raise Exception(f"given path is not a file path (path: {path})")
    if os.path.exists(path):
        raise Exception(f"given path already exist (path: {path})")
    with open(path, "w") as f :
        f.write(content)

def write_info(path, info: list, time_info):
    """description TODO

    Args:
        - path: TODO
        - info: TODO
        - time_info: TODO
    """

    path += "\\_logsInfo.txt"
    content = info.copy()
    if not content:
        content = ["there is no info"]
    content.insert(0, f"Info log for send_bg_shots.py (time info: {time_info})\n")
    content.append("\n___End of log")
    content = "\n".join(content)

    write_file(path, content)
    print(f"write info logs ->({path})")


def write_error(path, error: list, time_info):
    """description TODO

    Args:
        - path: TODO
        - error: TODO
        - time_info: TODO
    """

    if not error:
        print("no error logs to write")
        return
    path += "\\_logsError.txt"

    content = error.copy()
    content.insert(0, f"Error log for send_bg_shots.py (time info: {time_info})\n")
    content.append("\n___End of log")
    content = "\n".join(content)

    write_file(path, content)
    print(f"write error logs ->({path})")


def write_mail(path: str, mailContent: dict, time_info):
    """write a single mail_OPMxxx.txt

    Args:
        - path: the path where to write the mail.txt
        - mail: a dict with the mail content ({task:[psds]})
        - time_info: time informations (YmD_H-M-S)
    """
    if not path.endswith(".txt"):
        path += "\\_mail.txt"

    content = []

    for key in mailContent:
        if not mailContent[key]: 
            continue
        
        content.append(f"{key} :")

        for i, item in enumerate(mailContent[key]):
            tab = f"{i+1}"
            while len(tab) < 4 :
                tab = f" {tab}"

            content.append(f"{tab}. {item}")

        content.append("")

    content.insert(0, f"Mail generated by send_bg_shots.py (time info: {time_info})\n")
    content.append("\n___End of mail")
    content = "\n".join(content)

    write_file(path, content)
    print(f"write mail ->({path})")


def write_mails(path: str, time_info: str):
    """write all mail_OPMxxx.txt files

    Args:
        - path: the root write dir path where ep folder are
        - time_info: time informations 
    """

    """OPM102_BGL_SH021_chambreMalekInt_BGL CLEAN_v004.psd"""

    for ep in os.listdir(path):
        ep_dir = path + "\\" + ep
        if not os.path.isdir(ep_dir):
            continue
        if not ep.startswith("OPM"):
            continue
        
        write_path = f"{ep_dir}\\_mail_{ep}.txt"
        psds = {}

        for item in os.listdir(ep_dir):
            task = item.split("_")[-2]
            if task not in psds:
                psds.update({task:[]})
            psds[task].append(item)
        
        try:
            write_mail(write_path, psds, time_info)
        except Exception as err:
            print(f"fatal error: {err}")
            input()
            raise Exception(err)



def transfert_psd(fromPath, toPath):
    """use shutil.copy2 to copy a psd file from path to destination path

    Args:
        - fromPath: the path from (a file)
        - toPath: the path to (a directory)
    """
    try:
        shutil.copy2(fromPath, toPath)
    except Exception as err:
        return f"(transfert_psd except) {err}"
    

def transfert_psds(path: str, psds: list[str]):
    """Transfert all given psds to the script export directory

    Args:
        - path: the base root write dir path
        - psds: a list of psd to transfert
    """

    """OPM102_BGL_SH021_chambreMalekInt_BGL CLEAN_v004.psd"""

    print(f"start copying PSDs to {path}")
    errors = []    
    infos = []
        
    for i, psd in enumerate(psds):
        print(f"PSD {i+1}/{len(psds)}")
        if not psd.endswith(".psd") and not psd.endswith(".psb"):
            error = f"(transfert_psds) not a psd path (path: {psd})"
            print(error)
            errors.append(error)
            continue

        ep = psd.split("\\")[-1]
        ep = ep.split("_")[0]
        if not(ep.startswith("OPM") and ep[-3:].isdigit()):
            error = f"(transfert_psds) bad ep name, ep={ep} (psd: {psd})"
            print(error)
            errors.append(error)
            continue

        ep_dir = f"{path}\\{ep}"
        if not os.path.exists(ep_dir):
            os.makedirs(ep_dir)
            infos.append(f"(transfert_psds) ep folder created : \"{ep}\" (path: {ep_dir})")


        error = transfert_psd(psd, ep_dir)

        if error: 
            print(error)
            errors.append(error)
            
    print()
    return errors, infos


def write_logs_transfert_psd(path: str, logs: dict, psds: list):
    """description TODO

    Args:
        - path: the write path
        - logs: TODO
        - psds: TODO
    """

    while not os.path.exists(path):
        print(f"given write path does not exist (path: \"{path}\")")
        path = input("enter a valid path: ")
    
    time_info = datetime.datetime.now()
    time_info = time_info.strftime("%Y%m%d_%H-%M-%S")
    path += f"\\{time_info}"
    while os.path.exists(path):
        path += "_bis"
    
    os.makedirs(path)
    errors, infos = transfert_psds(path, psds)

    if errors: 
        logs["error"].extend(errors)

    if infos: 
        logs["info"].extend(infos)

    write_info(path, logs["info"], time_info)
    write_error(path, logs["error"], time_info)
    #write_mail(path, logs["mail"], time_info)
    write_mails(path, time_info)

    


def main():
    """Main function for sending BG shot"""

    logs = {"error": [], "info": [], "mail":{"BGL ROUGH": [], "BGL CLEAN": [], "BGC": []}}
    psds = []
    write_path = "O:\\_PROD BG\\1.SARAH\\_LIVRAISON"

    connect_to_sg()
    items, errors = collect_tasks()
    logs["error"].extend(errors)
    connect_to_gz()

    for i, item in enumerate(items):
        print(f"\n({i+1}/{len(items)})", end=" ")

        try:
            error = update_dbitem(item)
        except Exception as err:
            error = f"can't do update_dbitem for item={item}. err: {err}"
            print("\n| Abort |", error)

        if error:
            logs["error"].append(error)
            continue

        logs["info"].append("item \""+item["asset_name"]+"\" processed successfully (task: "+item["sg_task_name"]+")")
        logs["mail"][item["sg_task_name"]].append(item["comment"])
        psds.append(item["psd_file"])
    
    print()
    write_logs_transfert_psd(write_path, logs, psds)    