# ***** BEGIN GPL LICENSE BLOCK *****
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
# ***** END GPL LICENCE BLOCK *****
#
# (c) 2023, Paul Jadoul, Cedric Nicolas,  Guillaume Geelen

'''
logging related stuff
'''

import logging

def get_logger(logger_name: str = "logger") -> logging.Logger :
    '''
    get or create a logger

    Args:
        logger_name : the name of the logger to get/create
    '''

    logger = logging.getLogger(logger_name)
    logger.setLevel(logging.ERROR)
    # logger.setLevel(logging.DEBUG)
    if not logger.hasHandlers():
        stream_handler = logging.StreamHandler()

        # TODO better name if name is __init__
        formatter = logging.Formatter('\n|%(name)-12s- %(levelname)s in %(funcName)s (%(filename)s, %(lineno)d) : %(message)s\n(%(pathname)s)')
        stream_handler.setFormatter(formatter)
        logger.addHandler(stream_handler)

    return logger


def get_all_loggers() :
    ''' get all loggers in a list '''
    return [logging.getLogger(name) for name in logging.root.manager.loggerDict]


def set_level_all_loggers(level: str = "INFO") :
    ''' 
    set the level of all loggers to the given level 

    Args:
        level: the level string to set (-> DEBUG - INFO - ERROR)
    '''
    level = logging.getLevelName(level.upper())
    for logger in get_all_loggers() :
        logger.setLevel(level)


if __name__ == "__main__" :
    import os
    os.system("cls")

    #test_logger_01 = get_logger()
    #test_logger_02 = get_logger("azerty")
    #test_logger_03 = get_logger("yolo")

    #print(get_all_loggers())

    log = get_logger()

    log.error("test")
