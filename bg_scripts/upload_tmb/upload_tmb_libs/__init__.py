"""libs for upload tmp"""
import datetime
import shutil
import os
import gazu

import upload_tmb_libs.database_access.gazu_bridge as gz_db

class Log:
    name = "upload_tmb"
    allPrinted = []
    errorL = []
    all_logs = {"logs": allPrinted, "errors": errorL}

    def write(self, path: str) -> None:
        """
        Args:
            - path: the path to a dir where to write
        """
        if not os.path.exists(path):
            raise ValueError("given path does not exist")
        
        while True:
            time_info = datetime.datetime.now()
            time_info = time_info.strftime("%Y%m%d_%H-%M-%S")

            path = f"{path}\\_LOGS_{time_info}"
            
            if not os.path.exists(path):
                os.makedirs(path)
                print("write folder created")
                break

        header = f"logs for {self.name}.py ({time_info})"

        for log in self.all_logs:
            content = "".join(self.all_logs[log])
            if not content: continue
            file = path + f"\\{self.name}_{log}_{time_info}.txt"

            content = header + "\n" + f"{log.upper()}:\n" + content

            content += f"\n_End of repport\n"

            with open(file, "w") as f:
                f.write(content)

            print(f"{log} write at {file}")
            

    def get_newContent(self, *values, sep: str | None = " ", end: str | None = "\n") -> str:
        newContent = [str(x) for x in values]
        return sep.join(newContent) + end

    def print(self, *values, sep: str | None = " ", end: str | None = "\n") -> str: 
        print(*values, sep=sep, end=end)
        newContent = self.get_newContent(*values, sep=sep, end=end)
        self.allPrinted.append(newContent)
        return newContent
        
    def log(self, *values, sep: str | None = " ", end: str | None = "\n") -> str: 
        return self.print(*values, sep=sep, end=end)

    def error(self, *values, sep: str | None = " ", end: str | None = "\n") -> str:
        newContent = self.print(*values, sep=sep, end=end)
        self.errorL.append(newContent)
        return newContent
    
log = Log()

debug = False

def connect_to_gz():
    """description TODO"""
    gz_db.db_login(host="https://kitsu-opmed.andarta-pictures.com/api", username="svl95.pro@gmail.com", password="Prod_OPM/5")
    log.print("connected to kitsu")


def get_gz_asset(asset_name):
    """description TODO

    Args:
        - asset_name: TODO
    """

    project = gz_db.get_project()
    asset_type = gz_db.gazu.asset.get_asset_type_by_name('BG')
    return gz_db.gazu.asset.get_asset_by_name(project["id"], asset_name, asset_type["id"])



def get_gz_shot(ep, sh):
    """description TODO

    Args:
        - asset_name: TODO
    """
    return gz_db.get_shot_by_name(f"OPM_{ep}", f"SH{sh}")



def get_gz_asset_task(asset, task_name: str):
    """description TODO

    Args:
        - asset: TODO
        - task_name: TODO
    """

    all_task = gz_db.gazu.task.all_tasks_for_asset(asset)
    for task in all_task:
        if task["task_type_name"] == task_name:
            return task
    return None


def get_gz_shot_task(shot, task_name: str):
    """description TODO

    Args:
        - asset: TODO
        - task_name: TODO
    """

    all_task = gz_db.gazu.task.all_tasks_for_shot(shot)
    for task in all_task:
        if task["task_type_name"] == task_name:
            return task
    return None


def get_sh_data(name: str, isLine = False) -> tuple[str, str, str, str | None]:
    name = name.split(".")[:-1]
    name = ".".join(name)
    name = name.split("_")
    ep = sh = after = v = None

    if not isLine:
        ep = name[0][3:]
        sh = name[2][2:]
    else:
        ep = name[1]
        sh = name[2][2:]

    if not isLine:
        for i, item in enumerate(name):
            if not(item[0] in ["v"] and item[1:].isdigit()):
                continue

        for i, item in enumerate(reversed(name)):
            if not(item[0] in ["v"] and item[1:].isdigit()):
                continue
            after = name[3:-i-2]
            after = "_".join(after)
            v = item[1:]
            break

    return ep, sh, after, v


def OLD_get_task(name: str) -> (str | None):
    task = None
    if "_BGC_" in name:
        task = "CONFO BG COLO"
    if "_BGL_" in name:
        if task is not None:
            return
        task = "CONFO BG LINE"
    return task

def get_task(name: str) -> (str | None):
    if "_BGC_" in name:
        return "CONFO BG COLO"
    return "CONFO BG LINE"


def main():
    """Main function for uploading thumbnails"""

    tmb_dir = "O:\\_PROD BG\\7.VIGNETTES TO KITSU"
    # recuperer les png
    # pour chaque, envoyer sur kitsu et passer en done

    # OPM102_BGC_SH224_BAB_placeTroisHorloges_BGC_v002_CONFO.png
    # OPM102_BG_SH224_BAB_placeTroisHorloges
    
    try:
        connect_to_gz()
    except:
        print("can't connect to kitsu")
        input()

    for tmb in os.listdir(tmb_dir):
        if not tmb.endswith(".png"): continue
        try:
            task = get_task(tmb)
            log.print("tmb:", tmb, f"(task: {task})")
            if task is None:
                log.error(f"can't find task for {tmb}")
                continue

            tmb_path = tmb_dir + "\\" + tmb
            if not os.path.isfile(tmb_path): continue

            ep, sh, after, v = get_sh_data(tmb, task=="CONFO BG LINE")

            if ep == None:
                log.error(f"error for {tmb}: can\'t find episode info")
                continue
            if sh == None:
                log.error(f"error for {tmb}: can\'t find shot info")
                continue

            #gz_asset_name = f"OPM{ep.zfill(3)}_BG_SH{sh.zfill(3)}_{after}"
            gz_shot = get_gz_shot(ep, sh)
            #gz_task = get_gz_shot_task(gz_shot, "CONFO BG COLO")
            gz_task = get_gz_shot_task(gz_shot, task)

            done_statut = gz_db.gazu.task.get_task_status_by_short_name("DONE")
            gz_db.gazu.task.publish_preview(gz_task, done_statut, comment=tmb, preview_file_path=tmb_path)

            os.remove(tmb_path)
            log.print(" -> preview uploaded")
        except Exception as e:
            log.error(f"error in main for {tmb}:", e)


    log.write(tmb_dir)
