import sys

def test_python_version():
    supported = [3, 10, 11]
    current = [sys.version_info.major, 
               sys.version_info.minor, 
               sys.version_info.micro]
    for i, item in enumerate(current):
        if item < supported[i]:
            print(f"warning, current python version ({current}) lower than the supported ({supported})")
            return False
        elif item > supported[i]:
            print(f"warning, current python version ({current}) higger than the supported ({supported})")
            return False
    return True

if not test_python_version():
    input()
    raise

try:
    import pip
    pip.main(["install", "pip", "--upgrade"])

    try:
        import requests
    except:
        pip.main(["install", "requests"])

    try:
        import gazu
    except:
        pip.main(["install", "gazu"])
        import gazu

    import upload_tmb_libs
    import os
except Exception as err:
    print("import err:", err)
    input()
    raise



version = "0.1 | 2024-04-11"



def test_gz_version():
    supported = [0, 10, 1]
    supported_str = "0.10.1"
    currentV = gazu.__version__.split(".")
    for i, item in enumerate(currentV):
        item = int(item)
        if item < supported[i]:
            print(f"warning, current gazu version ({gazu.__version__}) lower than the supported ({supported_str})")
            try:
                pip.main(["install", "gazu", "--upgrade"])
            except:
                pass
            return False
        elif item > supported[i]:
            print(f"warning, current gazu version ({gazu.__version__}) higger than the supported ({supported_str})")
            return False
    return True


if __name__ == "__main__":
    os.system("cls")
    print("___START upload_tmb.py\n")

    if False:
        print("script in maintenance")
        input()
        raise

    try :
        test_py = test_python_version()
    except Exception as err:
        print("err:", err)
        input()


    
    test_gz_version()

    #TODO remove for production
    if False:
        upload_tmb_libs.main()
    else:
        try:
            upload_tmb_libs.main()
        except Exception as err:
            print("\n\nScript Crash !")
            print("error:", err)
            input()

    print("\n___END\n")