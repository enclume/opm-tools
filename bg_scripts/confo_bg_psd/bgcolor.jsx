var scriptVersion = "1.3 | 2024-05-29";
var sourcePath = "O:\\_SCRIPTS\\opm-tools\\BG_scripts\\confo_bg_psd\\bgcolor.jsx";

#include "packages.jsx";

// setup stuff
var defaultRulerUnits = preferences.rulerUnits;
preferences.rulerUnits = Units.PIXELS;

////////
// app
////////
function main(autoClose)
{
    var original = app.activeDocument; // keep ref of original
    var doc = duplicateActiveDocument();

    removeCroppedPixel(doc);
    var bgColorName = findRightLayerName(["BG COLOR", "BG_COLOR"]);

    var topLayers = app.activeDocument.layers;
    var toKeep = [bgColorName, "STB"];
    deleteLayers(topLayers, toKeep);

    unlockAllLayers();

    //var bgLayer = flatPersp(bgColorName); // -> searchAndMerge(["perspective", "persp"], bgLayer);
    var bg_colo = getLayer(bgColorName);
    var toUngroup = getLayers(bgColorName, bg_colo.layers);
    for (var i = 0; i < toUngroup.length; i++) {
        ungroupGivenLayer(toUngroup[i]);
    }

    mergeSubGroups(bg_colo);
    renameGroupChildren(bg_colo, getNewName_BGCOLO);
    ungroupLayer(bgColorName);
    
    // save _CONFO
    saveConfo(original);
    
    
    if(autoClose) {
        original.close(SaveOptions.DONOTSAVECHANGES);
        doc.close(SaveOptions.DONOTSAVECHANGES);
    }
}

function isUpToDate(versionSelf, sourcePath) {
    try {
        var sourceFile = new File(sourcePath);
        if (!sourceFile.exists) {return;}
        sourceFile.open("r");
        var content = sourceFile.read().split("\n");
        var sourceVersion = false;

        for (var i = 0; i < content.length; i++) {
            if (startswith(content[i], ["var version = \""])) {
                sourceVersion = content[i].split("\"")[1];
                sourceVersion = Number(sourceVersion.split(" | ")[0]);
                break;
            }
        }

        if (!sourceVersion) {return;}

        versionSelf = Number(versionSelf.split(" | ")[0]);

        if (sourceVersion > versionSelf) {
            alert("the file need an update (current version: "+versionSelf+", new version: "+sourceVersion+")\nYou can find the file up to date here: "+sourcePath);
        }
    }
    catch (err) {
        log.info("(isUpToDate) check update failed because of :\n"+err);
        return;
    }
}

isUpToDate(scriptVersion, sourcePath);

main(true);