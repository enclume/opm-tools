// 2024-02-13

#include "packages.jsx";

// setup stuff

var defaultRulerUnits = preferences.rulerUnits;
preferences.rulerUnits = Units.PIXELS;

////////
// app
////////

function main(autoClose)
{
    var original = app.activeDocument; // keep ref of original
    var docu = duplicateActive(); // copy

    // remove cropped pixel
    docu.selection.selectAll();
    var bound = docu.selection.bounds;
    cropToSelectionPixels (bound[1], bound[0], bound[3], bound[2], true)
    
    unlockAllLayers();
    
    searchLayerAndDelete("CARTOUCHE");
    searchLayerAndDelete("CARTOUCHES");
    searchLayerAndDelete("NOTE");
    searchLayerAndDelete("NOTES");
    searchLayerAndDelete("REF");
    searchLayerAndDelete("REFS");

    searchAndHide("STB");
    searchAndHide("CH");

    searchLayerAndDelete("BG LINE");
        
    var bgLayer = flatPersp("BG COLOR");

    if(mergeBGs(bgLayer))
        return;
    
    // save _CONFO
    saveConfo(original);
    
    if(autoClose)
    {
        original.close(SaveOptions.DONOTSAVECHANGES);
        docu.close(SaveOptions.DONOTSAVECHANGES);
    }
    
}

main(true);