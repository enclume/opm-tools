// 2024-03-05

#include "packages.jsx";

// setup stuff
var defaultRulerUnits = preferences.rulerUnits;
preferences.rulerUnits = Units.PIXELS;

////////
// app
////////
function main(autoClose)
{
    var original = app.activeDocument; // keep ref of original
    var doc = duplicateActiveDocument();

    removeCroppedPixel(doc);
    unlockAllLayers();

    var topLayers = app.activeDocument.layers;
    var toKeep = ["BG COLOR"];
    deleteLayers(topLayers, toKeep);

    //var bgLayer = flatPersp("BG COLOR"); // -> searchAndMerge(["perspective", "persp"], bgLayer);
    var bg_colo = getLayer("BG COLOR")
    mergeSubGroups(bg_colo)
    renameGroupChildren(bg_colo, getNewName_BGCOLO)
    ungroupLayer("BG COLOR");
    
    // save _CONFO
    saveConfo(original);
    
    if(autoClose) {
        original.close(SaveOptions.DONOTSAVECHANGES);
        doc.close(SaveOptions.DONOTSAVECHANGES);
    }
}

main(true);