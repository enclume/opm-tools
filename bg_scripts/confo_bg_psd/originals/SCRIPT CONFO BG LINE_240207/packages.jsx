// 2024-02-14

// https://github.com/mikechambers/CC-Utils/blob/master/photoshop/scripts/CreateNewLayer/Create%20New%20Layer.jsx

var USE_ERROR = false; // quand ça pète
var USE_VERBOSE = false; // avoir plus d'info

// quand ça pète
function loga(content)
{
    if(!USE_ERROR) return;
    alert(content);
}

// pour info
function log(content)
{
    if(!USE_VERBOSE) return;
    alert(content);
}

function cropSelection()
{
    executeAction( charIDToTypeID( "Crop" ), new ActionDescriptor(), DialogModes.NO );
}

function deselectLayers()
{
    var desc01 = new ActionDescriptor(); 
    var ref01 = new ActionReference(); 
    ref01.putEnumerated( charIDToTypeID('Lyr '), charIDToTypeID('Ordn'), charIDToTypeID('Trgt') ); 
    desc01.putReference( charIDToTypeID('null'), ref01 ); 
    executeAction( stringIDToTypeID('selectNoLayers'), desc01, DialogModes.NO ); 
}

/// create a new folder with document name
/// and returns the path
///
function solveOutputFolder(refDocument)
{
    if(refDocument == null)
    {
        alert("no document ?");
        return;
    }

    //alert("document name ? "+refDocument.name);

    var folderName = refDocument.name.replace(".psd", "");
    var folderPath = refDocument.path+"/"+folderName+"/";

    //alert("output folder : "+folderPath);

    //create output folder
    var folder = Folder(folderPath);
    if(!folder.exists) folder.create();

    return folderPath;
}


function savePNG(fileName)
{

    var pngOpts = new ExportOptionsSaveForWeb;
    pngOpts.format = SaveDocumentType.PNG
    pngOpts.PNG8 = false; 
    pngOpts.transparency = true; 
    pngOpts.interlaced = false; 
    pngOpts.quality = 100;

    var path = fileName+".png";

    //alert("saved to file : "+path);

    var file = new File(path);

    app.activeDocument.exportDocument(file, ExportType.SAVEFORWEB, pngOpts);


}

function searchAndMerge(layerName, owner)
{

    var lyr  = searchLayer(layerName, owner);

    if(lyr == null) return null;
    
    isVisible = lyr.visible;

    lyr = mergeLayer(lyr);

    lyr.visible = isVisible;
    
    return lyr;
}

function setLayersVisibility(ctx, visible)
{
    for(var i = 0; i < ctx.layers.length; i++)
    {
        ctx.layers[i].visible = visible;
    }
}

function mergeLayer(layer)
{
    app.activeDocument.activeLayer = layer;
    mergeDown();

    return layer;
}

function searchAndHide(layerName, owner)
{
    if(owner == undefined) owner = app.activeDocument;

    var lyr = searchLayer(layerName, owner);
    if(lyr != null)
    {
        lyr.visible = false;
    }
}

function unlockLayer(layer) {
  app.activeDocument.activeLayer = layer;
  if(app.activeDocument.activeLayer.isBackgroundLayer ) app.activeDocument.activeLayer.name = 'From Background';
  if(app.activeDocument.activeLayer.allLocked) app.activeDocument.activeLayer.allLocked = false;
  if(app.activeDocument.activeLayer.pixelsLocked && app.activeDocument.activeLayer.kind != LayerKind.TEXT) app.activeDocument.activeLayer.pixelsLocked = false;
  if(app.activeDocument.activeLayer.positionLocked) app.activeDocument.activeLayer.positionLocked = false;
  if(app.activeDocument.activeLayer.transparentPixelsLocked && app.activeDocument.activeLayer.kind != LayerKind.TEXT) app.activeDocument.activeLayer.transparentPixelsLocked = false;
}

function searchLayerAndDelete(layerName, owner)
{
    if(owner == undefined) owner = app.activeDocument;

    var lyr = searchLayer(layerName, owner);

    if(lyr == undefined) return false;

    for(var i = 0; i < lyr.layers.length; i++)
    {
        unlockLayer (lyr.layers [i]);
    }

    lyr.remove();

    return true;
}

function searchLayer(layerName, owner)
{
    if(owner == undefined) owner = app.activeDocument;

    if(owner == undefined)
    {
        loga("undefined owner ? "+layerName);
        return null;
    }

    var layers = owner.layers;

    if(layers == undefined)
    {
        log(layerName+" => no layers on "+owner.name);
        return null;
    }

    if(layers.length <= 0)
    {
        //log("0x layers on doc : "+owner.name);
        return null;
    }
    
    for(var i = 0; i < layers.length; i++)
    {
        var lyr = layers[i];
        
        //log(lyr.name+" vs "+layerName);

        if(lyr.name == layerName)
        {
            
            return lyr;
        }
    }

    log("couldn't find : "+layerName);

    return null;
}

function fetchNeighborLayers(layer)
{
    var layers = [];
    var parent = layer.parent;

    //alert(layer+" < "+parent);

    for(var i = 0; i < parent.layers.length; i++)
    {
        var lyr = parent.layers[i];
        if(!lyr.visible) continue;
        if(lyr == layer) continue;

        layers.push(lyr);
    }

    return layers;
}

function resizeCanvas(doc, padding)
{
    doc.trim();

    //give room around
    doc.resizeCanvas(UnitValue(doc.width + padding,"px"),UnitValue(doc.height + padding,"px"));
}



var deadEnds = [];

function gatherAllEndLayers(base, hideEachLayer)
{

    //alert(base.name);

    //if(win != null) win.text = "gathering dead ends ("+deadEnds.length+")";

    //hide everything
    if(hideEachLayer)
    {
        //alert("hiding "+base.name);
        base.visible = false;
    }

    //don't add '![name]' layers
    if(base.name[0] == "!") return;

    if(isLayerDeadEnd(base))
    {
        //alert("added "+base.name);
        deadEnds.push(base);
        return;
    }

    var lys = base.layers;

    //alert(base.name+" has "+lys.length+" children");

    for(var i = 0; i < lys.length; i++)
    {

        //alert("  L ("+i+") "+base.name);

        gatherAllEndLayers(lys[i], hideEachLayer);
    }

    return deadEnds;
}

function gatherAllLayersFrom(base, onlyVisible)
{
    var list = [];

    //folder !
    if(base.layers != null)
    {
        //alert("found folder "+base.name+" checking children ...");

        //each elements of folder
        for(i = 0; i < base.layers.length; i++)
        {
            var result = gatherAllLayersFrom(base.layers[i], onlyVisible);

            //adding each found layers to original array
            for(j = 0; j < result.length; j++){
                list.push(result[j]);
            }
        }

        //alert(" ... folder "+base.name+" gave "+list.length+" layers");
    }
    else
    {

        var toAdd = true;

        //do not include document
        if(base == app.activeDocument)
        {
            toAdd = false;
        }

        if(onlyVisible && !base.visible)
        {
            toAdd = false;
        }

        if(toAdd)
        {
            //alert(" ... adding layer : "+base.name);
            list.push(base);
        }
        else
        {
            //alert(" ... skipping layer : "+base.name)
        }

    }

    return list;
}


function add_new_layer(name)
{
    var doc = activeDocument;
    var layers = doc.artLayers;
    var newLayer = layers.add();
    newLayer.name = name;
    return newLayer;
}


function ungroup_layer(glayer_name)
{
    var glayer = searchLayer(glayer_name);
    var clayers = glayer.layers;
    var clayers_names = [];
    for (i = 0; i < clayers.length; i++) {
        clayers_names.push(clayers[i].name);
    }

    for (i = 0; i < clayers_names.length; i++) {
        var clayer = glayer.layers.getByName(clayers_names[i])
        clayer.move(glayer, ElementPlacement.PLACEBEFORE);
    }

    glayer.remove();
    return;
}




// hide/show active layer and ALL parents
//
function setLayerVisible(layer, visible)
{
    if(layer == null)
    {
        alert("no layer ?");
        return;
    }

    layer.visible = visible;
    var parent = layer.parent;
    while(parent != null)
    {
        parent.visible = visible;
        parent = parent.parent;
    }
}

// return true : has no other layer in children
//
function isLayerDeadEnd(base)
{
    //une layer qui a pas d'enfant est un calque
    if(base.layers == null) return true;

    var output = true;

    //si on trouve une layer enfant qui n'en a pas elle même c'est qu'on est au bout
    //si un des enfants a des enfants c'est qu'on est pas au bout
    for (i = 0; i < base.layers.length; i++)
    {
        if(base.layers[i].layers != null) output = false;
    }

    return output;
}





// speed up : https://community.adobe.com/t5/photoshop-ecosystem-discussions/is-there-any-way-to-prevent-ui-updating-while-jsx-script-is-running-in-photoshop/td-p/10059472


// https://theiviaxx.github.io/photoshop-docs/Photoshop/ArtLayer.html


function saveConfo(original)
{
    var outputPath = original.name.replace(".psd", "");
    outputPath += "_CONFO";
    outputPath = original.path + "/" + outputPath;
    with(original) saveAs(File(outputPath))
}


// duplicate active and make copy active
function duplicateActive()
{
    var copy = app.activeDocument.duplicate();
    app.activeDocument = copy;
    
    //refresh();

    copy.activeLayer = copy.layers[0];

    //refresh();

    // https://community.adobe.com/t5/photoshop-ecosystem-discussions/activelayer-returns-a-layer-when-no-layer-is-selected/m-p/10090865
    // make first layer active
    try { activeDocument.activeLayer.link(activeDocument.activeLayer) }
    catch(e) { /*happens when the background is a single layer*/ }

    //refresh();

    return copy;
}

/// search for BG.. layers and merge each
///
function mergeBGs(bgLayer)
{
    //var bgLayer = searchLayer(baseLayerName);
    
    if(bgLayer == undefined)
    {
        log(baseLayerName+" => undefined");
        return;
    }

    app.activeDocument.activeLayer = bgLayer;

    for(var i = 0; i < bgLayer.layers.length; i++)
    {
        var lyr = bgLayer.layers[i];

        var split = null;
        var splitChar = null;

        // start with "BG"
        if(lyr.name.indexOf("_") > -1) splitChar = "_";
        else if(lyr.name.indexOf(" ") > -1) splitChar = ' ';
        else
        {
            log("_ ? "+lyr.name.indexOf("_"))
            log("  ? "+lyr.name.indexOf(" "))
        }

        log(lyr.name+" ? "+splitChar);

        // NOT split-able
        if(splitChar == null)
            continue;

        split = lyr.name.split(splitChar);

        var prefix = split[0].toLowerCase();
        
        log("#"+i+"  > "+lyr.name+"  > "+prefix);

        // NOT starting with "bg"
        if(prefix != "bg")
            continue;

        // flatten
        mergeLayer(lyr);

        // rename (keep original naming)
        lyr.name = "BG";
        for(var j = 1; j < split.length; j++)
        {
            lyr.name += "_"+split[j];
        }

        log("#"+i+" > renamed   > "+lyr.name);
                
    }

    return false;
}

function cropCartouche()
{
    var docu = app.activeDocument;
    var crop = searchLayer(docu,"crop");

    if(crop == null && cartouche != null) 
    {
        // GENERATE crop

        //alert(cartouche)
        cartouche.translate(1,-1)

        // find negative space of cartouche
        docu.activeLayer = cartouche;
        selectVisibleLayer();
        docu.selection.invert();

        // https://theiviaxx.github.io/photoshop-docs/Photoshop/ArtLayer.html#artlayer
        crop = docu.artLayers.add()
        crop.name = "crop"

        docu.activeLayer = crop;

        var colorRef = new SolidColor();
        colorRef.rgb.red = 0;
        colorRef.rgb.green = 0;
        colorRef.rgb.blue = 0;

        docu.selection.fill(colorRef, ColorBlendMode.NORMAL, 100, false);

        cartouche.remove();
    }


    if(crop != null)
    {
        log("cropping");

        docu.activeLayer = crop;
        //selectVisibleLayer();
        magicWand(2,2)

        var bound = docu.selection.bounds

        // and remove cropped pixel
        cropToSelection (bound[1], bound[0], bound[3], bound[2], false)

        crop.remove();
    }

}

function rasterize(lyr, owner)
{
    aLyr = null;

    for(var i = 0; i < owner.layers.length; i++)
    {
        if(lyr == owner.layers[i])
        {
            aLyr = owner.artLayers[i];
        }
    }

    if(aLyr != null)
    {
        aLyr.rasterize(RasterizeType.ENTIRELAYER);
        aLyr.visible = false;
    }

    return aLyr;
}

function flatPersp(bgName)
{

    var bgLayer = searchLayer(bgName);

    if(bgLayer != undefined)
    {
        // https://community.adobe.com/t5/photoshop-ecosystem-discussions/how-to-rasterize-smart-object-layers-with-scripting/td-p/11268099

        var lyr = searchAndMerge("perspective", bgLayer);
        if(lyr != null) rasterize(lyr, bgLayer);
        
        lyr = searchAndMerge("pers", bgLayer);
        if(lyr != null) rasterize(lyr, bgLayer);

    }
    
    return bgLayer;
}


function unlockAllLayers()
{
    var doc = app.activeDocument;

    var topLayer = doc.layers[0];
    doc.activeLayer = topLayer;

    log("unlocking " + doc.name + " layers x" + doc.layers.length);

    do 
    {
        unlockLayer();
        selectLayerBelow();
    }
    while(topLayer != doc.activeLayer);

}


function unlockLayer() 
{
    var doc = app.activeDocument;

    if(doc.activeLayer.isBackgroundLayer) 
        doc.activeLayer.name = 'From Background';

    if(doc.activeLayer.allLocked) 
        doc.activeLayer.allLocked = false;

    if(doc.activeLayer.pixelsLocked && doc.activeLayer.kind != LayerKind.TEXT) 
        doc.activeLayer.pixelsLocked = false;

    if(doc.activeLayer.positionLocked) 
        doc.positionLocked = false;

    if(doc.activeLayer.transparentPixelsLocked && doc.activeLayer.kind != LayerKind.TEXT) 
        doc.activeLayer.transparentPixelsLocked = false;

}





























/// MATRIX
///

function selectLayerBelow() {
  var desc = new ActionDescriptor();
  var ref = new ActionReference();
  ref.putEnumerated( charIDToTypeID( "Lyr " ), charIDToTypeID( "Ordn" ), charIDToTypeID( "Bckw" ) );
  desc.putReference( charIDToTypeID( "null" ), ref );
  desc.putBoolean( charIDToTypeID( "MkVs" ), false );
  executeAction( charIDToTypeID( "slct" ), desc, DialogModes.NO );
}


// https://github.com/LeZuse/photoshop-scripts/blob/master/default/Flatten%20All%20Layer%20Effects.jsx

///////////////////////////////////////////////////////////////////////////////
// 
// Function: makeLayerBelow
// Usage: Creates a new layer below with the target layers name
// Input: targetName. the name of the layer we want to create a new layer below
// Return: <none>
///////////////////////////////////////////////////////////////////////////////
function makeLayerBelow(targetName) {
    try {
        var id829 = charIDToTypeID( "Mk  " );
            var desc169 = new ActionDescriptor();
            var id830 = charIDToTypeID( "null" );
                var ref105 = new ActionReference();
                var id831 = charIDToTypeID( "Lyr " );
                ref105.putClass( id831 );
            desc169.putReference( id830, ref105 );
            var id832 = stringIDToTypeID( "below" );
            desc169.putBoolean( id832, true );
            var id833 = charIDToTypeID( "Usng" );
                var desc170 = new ActionDescriptor();
                var id834 = charIDToTypeID( "Nm  " );
                desc170.putString( id834, targetName );
            var id835 = charIDToTypeID( "Lyr " );
            desc169.putObject( id833, id835, desc170 );
        executeAction( id829, desc169, DialogModes.NO );
    }catch(e) {
        ; // do nothing
    }
}

///////////////////////////////////////////////////////////////////////////////
// Function: selectPreviousLayer
// Usage: Selects the layer above the selected layer, adding that layer to the current layer selection
// Input: <none> Must have an open document
// Return: <none>
///////////////////////////////////////////////////////////////////////////////
function selectPreviousLayer() {
    try {
        var idslct = charIDToTypeID( "slct" );
            var desc9 = new ActionDescriptor();
            var idnull = charIDToTypeID( "null" );
                var ref8 = new ActionReference();
                var idLyr = charIDToTypeID( "Lyr " );
                var idOrdn = charIDToTypeID( "Ordn" );
                var idFrwr = charIDToTypeID( "Frwr" );
                ref8.putEnumerated( idLyr, idOrdn, idFrwr );
            desc9.putReference( idnull, ref8 );
            var idselectionModifier = stringIDToTypeID( "selectionModifier" );
            var idselectionModifierType = stringIDToTypeID( "selectionModifierType" );
            var idaddToSelection = stringIDToTypeID( "addToSelection" );
            desc9.putEnumerated( idselectionModifier, idselectionModifierType, idaddToSelection );
            var idMkVs = charIDToTypeID( "MkVs" );
            desc9.putBoolean( idMkVs, false );
        executeAction( idslct, desc9, DialogModes.NO );
    }catch(e) {
        ; // do nothing
    }
}

///////////////////////////////////////////////////////////////////////////////
// Function: mergeDown
// Usage: merges the currently selected layers into one layer. If only one layer is selected it merges the current layer down into the layer below
// Input: <none> Must have an open document
// Return: <none>
///////////////////////////////////////////////////////////////////////////////
function mergeDown() {
    try {
        var id828 = charIDToTypeID( "Mrg2" );
            var desc168 = new ActionDescriptor();
        executeAction( id828, desc168, DialogModes.NO );
    }catch(e) {
        ; // do nothing
    }
}



function selectVisibleLayer()
{
    var id1268 = charIDToTypeID( "setd" );
    var desc307 = new ActionDescriptor();
    var id1269 = charIDToTypeID( "null" );
    var ref257 = new ActionReference();
    var id1270 = charIDToTypeID( "Chnl" );
    var id1271 = charIDToTypeID( "fsel" );
    ref257.putProperty( id1270, id1271 );
    desc307.putReference( id1269, ref257 );
    var id1272 = charIDToTypeID( "T   " );
    var ref258 = new ActionReference();
    var id1273 = charIDToTypeID( "Chnl" );
    var id1274 = charIDToTypeID( "Chnl" );
    var id1275 = charIDToTypeID( "Trsp" );
    ref258.putEnumerated( id1273, id1274, id1275 );
    desc307.putReference( id1272, ref258 );
    executeAction( id1268, desc307, DialogModes.NO )
}

// return : array of layers (selected in editor)
//
function getSelectedLayers()
{
    var idGrp = stringIDToTypeID( "groupLayersEvent" );
    var descGrp = new ActionDescriptor();
    var refGrp = new ActionReference();
    refGrp.putEnumerated(charIDToTypeID( "Lyr " ),charIDToTypeID( "Ordn" ),charIDToTypeID( "Trgt" ));
    descGrp.putReference(charIDToTypeID( "null" ), refGrp );
    executeAction( idGrp, descGrp, DialogModes.ALL );
    var resultLayers=new Array();
    for (var ix=0;ix<app.activeDocument.activeLayer.layers.length;ix++){resultLayers.push(app.activeDocument.activeLayer.layers[ix])}
    var id8 = charIDToTypeID( "slct" );
    var desc5 = new ActionDescriptor();
    var id9 = charIDToTypeID( "null" );
    var ref2 = new ActionReference();
    var id10 = charIDToTypeID( "HstS" );
    var id11 = charIDToTypeID( "Ordn" );
    var id12 = charIDToTypeID( "Prvs" );
    ref2.putEnumerated( id10, id11, id12 );
    desc5.putReference( id9, ref2 );
    executeAction( id8, desc5, DialogModes.NO );
    return resultLayers;
}


// https://community.adobe.com/t5/photoshop-ecosystem-discussions/script-with-magic-wand-tool/m-p/5446467
function magicWand(x,y,tolerance,anit_alias,contiguous,sampleAll)
{

    if(arguments.length < 2) return;// make sure have x,y - other arguments are optional

    if(undefined == tolerance) var tolerance = 100;// set defaults of optional arguments

    if(undefined == anit_alias) var anit_alias = true;

    if(undefined == contiguous) var contiguous = true;

     if(undefined == sampleAll) var sampleAll = false;

    var desc = new ActionDescriptor();

        var ref = new ActionReference();

        ref.putProperty( charIDToTypeID('Chnl'), charIDToTypeID('fsel') );

    desc.putReference( charIDToTypeID('null'), ref );

        var positionDesc = new ActionDescriptor();

        positionDesc.putUnitDouble( charIDToTypeID('Hrzn'), charIDToTypeID('#Rlt'), x );// in pixels

        positionDesc.putUnitDouble( charIDToTypeID('Vrtc'), charIDToTypeID('#Rlt'), y );

    desc.putObject( charIDToTypeID('T   '), charIDToTypeID('Pnt '), positionDesc );

    desc.putInteger( charIDToTypeID('Tlrn'), tolerance);

    desc.putBoolean( charIDToTypeID('Mrgd'), sampleAll );

    if(!contiguous) desc.putBoolean( charIDToTypeID( 'Cntg' ), false );

    desc.putBoolean( charIDToTypeID('AntA'), anit_alias );

    executeAction( charIDToTypeID('setd'), desc, DialogModes.NO );

};

function cropToSelection(top, left, bottom, right)
{
    var idCrop = charIDToTypeID( "Crop" );
    var desc11 = new ActionDescriptor();
    var idT = charIDToTypeID( "T   " );
    var desc12 = new ActionDescriptor();
    var idTop = charIDToTypeID( "Top " );
    var idPxl = charIDToTypeID( "#Pxl" );
    desc12.putUnitDouble( idTop, idPxl, top );
    var idLeft = charIDToTypeID( "Left" );
    var idPxl = charIDToTypeID( "#Pxl" );
    desc12.putUnitDouble( idLeft, idPxl,left );
    var idBtom = charIDToTypeID( "Btom" );
    var idPxl = charIDToTypeID( "#Pxl" );
    desc12.putUnitDouble( idBtom, idPxl, bottom );
    var idRght = charIDToTypeID( "Rght" );
    var idPxl = charIDToTypeID( "#Pxl" );
    desc12.putUnitDouble( idRght, idPxl, right );
    var idRctn = charIDToTypeID( "Rctn" );
    desc11.putObject( idT, idRctn, desc12 );
    var idAngl = charIDToTypeID( "Angl" );
    var idAng = charIDToTypeID( "#Ang" );
    desc11.putUnitDouble( idAngl, idAng, 0.000000 );
    var idDlt = charIDToTypeID( "Dlt " );
    desc11.putBoolean( idDlt, false );
    var idcropAspectRatioModeKey = stringIDToTypeID( "cropAspectRatioModeKey" );
    var idcropAspectRatioModeClass = stringIDToTypeID( "cropAspectRatioModeClass" );
    var idtargetSize = stringIDToTypeID( "targetSize" );
    desc11.putEnumerated( idcropAspectRatioModeKey, idcropAspectRatioModeClass, idtargetSize );
    executeAction( idCrop, desc11, DialogModes.NO );
}


// Courtesy of Chuck Uebele
function cropToSelectionPixels(top, left, bottom, right, retainPixels) {

    var origRuler = app.preferences.rulerUnits;
    app.preferences.rulerUnits = Units.PIXELS;

    var idCrop = charIDToTypeID("Crop");
    var desc11 = new ActionDescriptor();
    var idT = charIDToTypeID("T   ");
    var desc12 = new ActionDescriptor();
    var idTop = charIDToTypeID("Top ");
    var idPxl = charIDToTypeID("#Pxl");
    desc12.putUnitDouble(idTop, idPxl, top);
    var idLeft = charIDToTypeID("Left");
    var idPxl = charIDToTypeID("#Pxl");
    desc12.putUnitDouble(idLeft, idPxl, left);
    var idBtom = charIDToTypeID("Btom");
    var idPxl = charIDToTypeID("#Pxl");
    desc12.putUnitDouble(idBtom, idPxl, bottom);
    var idRght = charIDToTypeID("Rght");
    var idPxl = charIDToTypeID("#Pxl");
    desc12.putUnitDouble(idRght, idPxl, right);
    var idRctn = charIDToTypeID("Rctn");
    desc11.putObject(idT, idRctn, desc12);
    var idAngl = charIDToTypeID("Angl");
    var idAng = charIDToTypeID("#Ang");
    desc11.putUnitDouble(idAngl, idAng, 0.000000);
    var idDlt = charIDToTypeID("Dlt ");
    desc11.putBoolean(idDlt, retainPixels); // delete cropped pixels = true | false
    var idcropAspectRatioModeKey = stringIDToTypeID("cropAspectRatioModeKey");
    var idcropAspectRatioModeClass = stringIDToTypeID("cropAspectRatioModeClass");
    var idtargetSize = stringIDToTypeID("targetSize");
    desc11.putEnumerated(idcropAspectRatioModeKey, idcropAspectRatioModeClass, idtargetSize);
    executeAction(idCrop, desc11, DialogModes.NO);

    app.preferences.rulerUnits = origRuler;
}