// 2024-02-13

#include "packages.jsx";

// setup stuff

var defaultRulerUnits = preferences.rulerUnits;
preferences.rulerUnits = Units.PIXELS;

////////
// app
////////

function main(autoClose)
{
    var original = app.activeDocument; // keep ref of original
    var docu = duplicateActive(); // copy

    // remove cropped pixel
    docu.selection.selectAll();
    var bound = docu.selection.bounds;
    cropToSelectionPixels (bound[1], bound[0], bound[3], bound[2], true)
    
    unlockAllLayers();

    var top_layers = app.activeDocument.layers;
    var to_delete = [];
    var to_keep = ["BG LINE", "FIELD", "STB", "CH"];
    for (i = 0; i < top_layers.length; i++) {
        var layer = top_layers[i];

        var is_to_keep = false
        
        for (j = 0; j < to_keep.length; j++) {
            if (layer.name === to_keep[j]) {
                is_to_keep = true;
                break;
            }
        }

        if (is_to_keep) {continue;}
        to_delete.push(layer.name);
    }

    //log("layer to \"searchLayerAndDelete\"" + to_delete.toString())

    for (i = 0; i < to_delete.length; i++) {
        searchLayerAndDelete(to_delete[i]);
    }

    /*
    searchLayerAndDelete("CARTOUCHE");
    searchLayerAndDelete("CARTOUCHES");
    searchLayerAndDelete("NOTE");
    searchLayerAndDelete("NOTES");
    searchLayerAndDelete("REF");
    searchLayerAndDelete("REFS");
    searchLayerAndDelete("FIELD");
    searchLayerAndDelete("STB");
    searchLayerAndDelete("CH");
    searchLayerAndDelete("PROPS");
    */
    /* old stuff
    searchAndHide("STB");
    searchAndHide("CH");
    */
    /*
    searchLayerAndDelete("BG LINE");
    */    
    var bgLayer = flatPersp("BG LINE");
    //var bgLayer = flatPersp("PROPS");

    if(mergeBGs(bgLayer))
        return;

    //ungroup_layer("BG LINE")
    
    // save _CONFO
    saveConfo(original);
    
    if(autoClose)
    {
        original.close(SaveOptions.DONOTSAVECHANGES);
        docu.close(SaveOptions.DONOTSAVECHANGES);
    }
    
}

main(true);