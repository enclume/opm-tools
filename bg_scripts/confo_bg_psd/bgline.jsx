var scriptVersion = "1.4 | 2024-05-30";
var sourcePath = "O:\\_SCRIPTS\\opm-tools\\BG_scripts\\confo_bg_psd\\bgline.jsx";

#include "packages.jsx";


function groupFieldLayers(doc, layerColl) {
    var fLayer = getLayer("FIELD");
    var layers = [];
    for (var i = 0; i < layerColl.length; i++) {
        var layer = layerColl[i];
        if (startswith(layer.name, ["FIELD", "field", "Field"]) && layer != fLayer) {
            layers.push(layer);
        }
    }

   
    if (fLayer === null) {
        var group = doc.layerSets.add();
        group.name = "FIELD";
        ref = group.layerSets.add();
        for (var i = 0; i < layers.length; i++) {
            var layer = layers[i];
            layer.move(ref, ElementPlacement.PLACEBEFORE);
        }
        ref.remove()
        return;
    }

    if (!isGroupLayer(fLayer)) {
        alert("can't group all \"FIELD_\" Layers");
        return;
    }

    ref = fLayer.layerSets.add();
    for (var i = 0; i < layers.length; i++) {
        var layer = layers[i];
        layer.move(ref, ElementPlacement.PLACEBEFORE);
    }
    ref.remove()
}


function extractFieldLayers(fLayer) 
{
    if (fLayer === null || fLayer === undefined) {
		alert("can't find \"FIELD\" layer !");
		return;
	}
    var isMaster = false;
    var layers = fLayer.layers;

    // test if is a field master case (multiples subGroups)
    for (var i = 0; i < layers.length; i++) {
        var layer = layers[i];
        layer = getLayer(layer.name, layers);
        if (layer.layers == undefined) {continue;}
        if (layer.layers.length < 1) {continue;} 
        isMaster = true;
        break;
    }

    if (isMaster) {
        // delete all non group children ?
        mergeSubGroups(fLayer);
        renameGroupChildren(fLayer, getNewName_FIELD);
        ungroupLayer("FIELD");
        return;
    }

    var topSubLayer = mergeSubLayers(fLayer);
    var shNbr = getShotNumber();
	if (topSubLayer) {
		topSubLayer.name = "FIELD_"+shNbr;
	}
    ungroupLayer("FIELD");
}


////////
// app
////////
function main(autoClose)
{
    // setup stuff
    var defaultRulerUnits = preferences.rulerUnits;
    preferences.rulerUnits = Units.PIXELS;
    var original = app.activeDocument; // keep ref of original
    var doc = duplicateActiveDocument();

    removeCroppedPixel(doc);
    
    var bgLineName = findRightLayerName(["BG LINE", "BG_LINE"]);
    var others = ["CH", "STB"]

    var topLayers = app.activeDocument.layers;

    groupFieldLayers(doc, topLayers);

    var toKeep = ["FIELD", bgLineName];
    for (var i = 0; i < others.length; i++) {toKeep.push(others[i]);}

    deleteLayers(topLayers, toKeep);

    unlockAllLayers();

    var bg_line = getLayer(bgLineName);
    mergeSubGroups(bg_line);
    ungroupLayer(bgLineName);


    var field = getLayer("FIELD");
    extractFieldLayers(field);


    for (var i = 0; i < others.length; i++) {
        var name = others[i];
        var glayer = getLayer(name)
        var subLayer = mergeSubLayers(glayer);    
        ungroupLayer(name);
		if (subLayer) {
			subLayer.name = name;
		}
    }

    // save as OPM_xxx_SHxxx.psd
    saveConfoLine(original);
    
    if(autoClose) {
        original.close(SaveOptions.DONOTSAVECHANGES);
        doc.close(SaveOptions.DONOTSAVECHANGES);
    }
}

function isUpToDate(versionSelf, sourcePath) {
    try {
        var sourceFile = new File(sourcePath);
        if (!sourceFile.exists) {return;}
        sourceFile.open("r");
        var content = sourceFile.read().split("\n");
        var sourceVersion = false;

        for (var i = 0; i < content.length; i++) {
            if (startswith(content[i], ["var version = \""])) {
                sourceVersion = content[i].split("\"")[1];
                sourceVersion = Number(sourceVersion.split(" | ")[0]);
                break;
            }
        }

        if (!sourceVersion) {return;}

        versionSelf = Number(versionSelf.split(" | ")[0]);

        if (sourceVersion > versionSelf) {
            alert("the file need an update (current version: "+versionSelf+", new version: "+sourceVersion+")\nYou can find the file up to date here: "+sourcePath);
        }
    }
    catch (err) {
        log.info("(isUpToDate) check update failed because of :\n"+err);
        return;
    }
}

isUpToDate(scriptVersion, sourcePath);

main(true);