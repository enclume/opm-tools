var scriptVersion = "1.7 | 2024-05-29";
var sourcePath = "O:\\_SCRIPTS\\opm-tools\\BG_scripts\\confo_bg_psd\\packages.jsx";

// https://github.com/mikechambers/CC-Utils/blob/master/photoshop/scripts/CreateNewLayer/Create%20New%20Layer.jsx

var LOG_ERROR = false; // quand ça pète
var LOG_INFO = false; // avoir plus d'info


/**
 * searches an array for an element value and returns its position.
 * @param {Array} array - the array to test
 * @param {*} value - the value to look for
 * @returns {Number} the first index of the value, -1 if not found
 */
function indexOf(array, value) {
    for (var i = 0; i < array.length; i++) {
        if (array[i] == value) {return i;}
    }
    return -1;
}


var log = {
    levels: ["debug", "info", "warning", "error", "critical"],
    level: 4,
    isInLevel: function(levelName) {
        indexLevel = indexOf(this.levels, levelName);
        if(indexLevel >= this.level) {return true;}
        return false;
    },
    setLevel: function(levelName) {
        newLevel = indexOf(this.levels, levelName);
        if (newLevel == -1) {return;}
        this.level = newLevel;
    },
    getLevel: function() {
        return this.levels[this.level];
    },
    getContext: function() {
        return "";
        var callStackLines = (new Error()).stack.split('\n');
        callStackLines.splice(0, 1);
        var callStack = callStackLines.join('\n') + '\n';
        return " ("+callStackLines[1]+")";
    },
    logprint: function(value, levelName) {
        if (!this.isInLevel(levelName)) {return;}
        context = this.getContext();
        value = levelName.toUpperCase()+context+" : "+ value;
        alert(value);
    },
    critical: function(value) {this.logprint(value, "critical");},
    error: function(value) {this.logprint(value, "error");},
    warning: function(value) {this.logprint(value, "warning");},
    info: function(value) {this.logprint(value, "info");},
    debug: function(value) {this.logprint(value, "debug");}
}

if (LOG_INFO) {
    log.setLevel("info");
} else if (LOG_ERROR) {
    log.setLevel("error");
}

function isUpToDate(versionSelf, sourcePath) {
    try {
        var sourceFile = new File(sourcePath);
        if (!sourceFile.exists) {return;}
        sourceFile.open("r");
        var content = sourceFile.read().split("\n");
        var sourceVersion = false;

        for (var i = 0; i < content.length; i++) {
            if (startswith(content[i], ["var version = \""])) {
                sourceVersion = content[i].split("\"")[1];
                sourceVersion = Number(sourceVersion.split(" | ")[0]);
                break;
            }
        }

        if (!sourceVersion) {return;}
        versionSelf = Number(versionSelf.split(" | ")[0]);

        if (sourceVersion > versionSelf) {
            alert("the file need an update (current version: "+versionSelf+", new version: "+sourceVersion+")\nYou can find the file up to date here: "+sourcePath);
        }
    }
    catch (err) {
        log.info("(isUpToDate) check update failed because of :\n"+err);
        return;
    }
}

isUpToDate(scriptVersion, sourcePath);


/**
 * duplicate the active document
 * @returns {Document} The duplicated photoshop document
 */
function duplicateActiveDocument() {
    var copy = app.activeDocument.duplicate();
    app.activeDocument = copy;
    copy.activeLayer = copy.layers[0];

    try {activeDocument.activeLayer.link(activeDocument.activeLayer);}
    catch(e) { /*happens when the background is a single layer*/ }
    return copy;
}

/**
 * Remove cropped pixel in the guiven photoshop document
 * @param {Document} - The photoshop document to crop
 */
function removeCroppedPixel(document) {
    document.selection.selectAll();
    var bound = document.selection.bounds;
    cropToSelectionPixels (bound[1], bound[0], bound[3], bound[2], true)
}

/**
 * get the shot number based on the current file name
 */
function getShotNumber() {
    var fileName = app.activeDocument.name;

    if (fileName.indexOf("SH") == -1) {
        log.critical("(getShotNumber) SH not in file name, can't get the shot number ->(fileName: "+fileName+")");
        return "NaN";
    }

    fileName = fileName.split("SH")[1];
    var shNbr = "";
    for (var i = 0; i < fileName.length; i++) {
        if (isNaN(Number(fileName[i]))) {break;}
        shNbr += fileName[i];
    }

    if (shNbr.length == 0) {
        log.critical("(getShotNumber) can't find a shot number ->(fileName: "+fileName+")");
        return "NaN";
    }

    log.info("(getShotNumber) find \""+shNbr+"\" as current file shot number")
    return shNbr;
}

/**
 * Add a new layer to the active photoshop document
 * @param {String} name - The name of the new layer
 * @returns {Layer}
 */
function addNewLayer(name) {
    var doc = activeDocument;
    var layers = doc.artLayers;
    var newLayer = layers.add();
    newLayer.name = name;
    return newLayer;
}

/**
 * Get a layer by his name in the active photoshop document
 * @param {String} layerName - The name of the layer
 * @param {Layers} layers - The layers collection (if not set, the activeDocument.layers collection)
 * @returns {Layer}
 */
function getLayer(layerName, layers) {   
    if (layers == undefined) {
        var doc = app.activeDocument;
        if (doc == undefined) {
            log.error("(getLayer) doc is undefined (layerName: "+layerName+")");
            return null;
        }
        layers = doc.layers;
    }

    if (layers == undefined) {
        log.info("(getLayer) "+layerName+" => no layers on "+doc.name);
        return null;
    }

    if (layers.length <= 0) {
        log.debug("(getLayer) 0x layers on doc : "+doc.name);
        return null;
    }
    
    for (var i = 0; i < layers.length; i++) {
        var layer = layers[i];
        log.debug(layer.name+" vs "+layerName);
        if(layer.name == layerName) {return layer;}
    }

    log.info("(getLayer) couldn't find : "+layerName);
    return null;
}

/**
 * Get all layers with given name in the given layer collection
 * 
 * @param {String} layerName - The name of the layer
 * @param {Layers} layers - The layers collection (if not set, the activeDocument.layers collection)
 * @returns {Layer[]}
 */
function getLayers(layerName, layers) {   
    if (layers == undefined) {
        var doc = app.activeDocument;
        if (doc == undefined) {
            log.error("(getLayer) doc is undefined (layerName: "+layerName+")");
            return null;
        }
        layers = doc.layers;
    }

    if (layers == undefined) {
        log.info("(getLayer) "+layerName+" => no layers on "+doc.name);
        return null;
    }

    if (layers.length <= 0) {
        log.debug("(getLayer) 0x layers on doc : "+doc.name);
        return null;
    }

    var foundLayers = [];
    for (var i = 0; i < layers.length; i++) {
        var layer = layers[i];
        log.debug(layer.name+" vs "+layerName);
        if (layer.name == layerName) {foundLayers.push(layer);}
    }

    log.debug("getLayers: "+foundLayers);
    return foundLayers;
}

/**
 * find the right layer name in a list of possible names
 * @param {String[]} names 
 */
function findRightLayerName(names) {
    var existings = [];
    for (var i = 0; i < names.length; i++) {
        var logLevelChanged = false;
        if (log.getLevel() == "info") {
            log.setLevel("error");
            logLevelChanged = true;
        }
        if (getLayer(names[i])){existings.push(names[i]);}
        if (logLevelChanged) {log.setLevel("info");}
    }
    if (existings.length == 0) {
        var error = "(findRightLayerName) no name valid ! (given: "+names+")";
        log.critical(error);
        throw new Error(error);
    }
    if (existings.length > 1) {
        var error = "(findRightLayerName) more than one name valid ! (given: "+names+", existing: "+existings+")";
        log.critical(error);
        throw new Error(error);
    }
    log.info("(findRightLayerName) find "+existings[0]+" valide name in "+names);
    return existings[0];
}

/**
 * unlock the given layer
 * @param {*} layer 
 */
function unlockLayer(layer) {
    app.activeDocument.activeLayer = layer;
    //alert("unlockLayer : "+app.activeDocument.activeLayer)
    if (app.activeDocument.activeLayer.isBackgroundLayer ) {
        app.activeDocument.activeLayer.name = 'From Background';
    }
    app.activeDocument.activeLayer.allLocked = false;
    app.activeDocument.activeLayer.positionLocked = false;
    
    if(app.activeDocument.activeLayer.kind != LayerKind.TEXT) {
        app.activeDocument.activeLayer.pixelsLocked = false;
        app.activeDocument.activeLayer.transparentPixelsLocked = false;
    }
    //alert("allLocked : "+app.activeDocument.activeLayer.allLocked+"\npositionLocked : "+app.activeDocument.activeLayer.positionLocked+"\nallLocked : "+app.activeDocument.activeLayer.pixelsLocked+"\nallLocked : "+app.activeDocument.activeLayer.transparentPixelsLocked+"\n")
}

function oldunlockLayer(layer) {
    app.activeDocument.activeLayer = layer;
    if(app.activeDocument.activeLayer.isBackgroundLayer ) {
        app.activeDocument.activeLayer.name = 'From Background';
    }
    if(app.activeDocument.activeLayer.allLocked) {
        app.activeDocument.activeLayer.allLocked = false;
    }
    if(app.activeDocument.activeLayer.pixelsLocked && app.activeDocument.activeLayer.kind != LayerKind.TEXT) {
        app.activeDocument.activeLayer.pixelsLocked = false;
    }
    if(app.activeDocument.activeLayer.positionLocked) {
        app.activeDocument.activeLayer.positionLocked = false;
    }
    if(app.activeDocument.activeLayer.transparentPixelsLocked && app.activeDocument.activeLayer.kind != LayerKind.TEXT) {
        app.activeDocument.activeLayer.transparentPixelsLocked = false;
    }
}

/**
 * Return True if the given layer is a group
 * @param {Layer} layer 
 * @returns {boolean}
 */
function isGroupLayer(layer) {
    if (layer.kind == "GROUP") {return true;}
    if (layer.layers == undefined) {return false;}
    if (layer.layers.length < 1) {return false;}
    return true;
}

/**
 * expand and unhide all layers in the active photoshop document
 * @param {Layer} root - the parent layer 
 */
function revealAllLayers(root) {
    if (root == undefined) {
        var root = app.activeDocument;
    }
    var layers = root.layers;
    for (var i = 0; i < layers.length; i++) {
        var layer = layers[i];
        layer = getLayer(layer.name, layers);
        layer.visible = true;
        app.activeDocument.activeLayer = layer;
        if (!isGroupLayer(layer)) {continue;}
        revealAllLayers(layer);
    }
}


/**
 * expand, unhide and unlock all layers in the active photoshop document
 * @param {Layer} root - the parent layer  
 */
function revealunlockAllLayers(root) {
    if (root == undefined) {root = app.activeDocument;}
    var layers = root.layers;
    for (var i = 0; i < layers.length; i++) {
        var layer = layers[i];
        layer = getLayer(layer.name, layers);
        layer.visible = true;
        app.activeDocument.activeLayer = layer;
        unlockLayer(layer);
        if (!isGroupLayer(layer)) {continue;}
        revealunlockAllLayers(layer);
    }
}


/**
 * unlock all layers in the active photoshop document
 */
function unlockAllLayers() {
    var root = app.activeDocument;
    revealunlockAllLayers(root);
    return;

    log.info("(unlockAllLayers) start revealling all layers (can take a while)")
    revealAllLayers(root)

    var doc = app.activeDocument;

    var topLayer = doc.layers[0];
    doc.activeLayer = topLayer;

    log.info("(unlockAllLayers) unlocking "+doc.layers.length+" layers in " + doc.name);

    do {
        doc.activeLayer.visible = true;
        unlockLayer(doc.activeLayer);
        selectLayerBelow();
    } 
    while(topLayer != doc.activeLayer);
}

/**
 * Delete a layer by his name in the active photoshop document
 * 
 * @param {String} layerName - The name of the layer
 * @returns {Boolean} True if the layer was found and deleted
 */
function deleteLayer(layerName) {
    var layer = getLayer(layerName);
    if (layer == undefined) {return false;}

    //if the layer is a group, unlock child layers
    if (isGroupLayer(layer)) {
        revealunlockAllLayers(layer);
    } else {
        layer.visible = true;
        app.activeDocument.activeLayer = layer;
        unlockLayer(layer);
    }
    app.activeDocument.activeLayer = layer;
    layer.remove();
    return true;
}

/**
 * Delete all given layers, 
 * but keep layers with name in toKeep name list
 * @param {Layers} layers - The layers collection
 * @param {String[]} toKeep - An array of layer names (string)
 */
function deleteLayers(layers, toKeep) {
    var toDelete = []; // Array of layer names to delete
    for (i = 0; i < layers.length; i++) {
        var l_name = layers[i].name;
        var is_to_keep = false;
        
        for (j = 0; j < toKeep.length; j++) {
            if (l_name === toKeep[j]) {
                is_to_keep = true;
                break;
            }
        }

        if (is_to_keep) {continue;}
        toDelete.push(l_name);
    }

    //second loop to avoid conflict with the given layer collection 
    for (i = 0; i < toDelete.length; i++) {
        deleteLayer(toDelete[i]);
    }
}

/**
 * replace a value by an other in a string
 * @param {string} str - the string
 * @param {string} value - the value to replace
 * @param {string} repl - the value to set in place of the old one
 * @returns {string} the new layer name
 */
function replaceInString(str, value, repl) {
    while (str.indexOf(value) != -1) {str = str.replace(value, repl);}
    return str;
}

/**
 * replace a value by an other in a layer name
 * @param {Layer} layer - the layer to rename
 * @param {string} value - the value to replace
 * @param {string} repl - the value to set in place of the old one
 * @returns {string} the new layer name
 */
function replaceInLayerName(layer, value, repl) {
    var name = replaceInString(layer.name, value, repl);
    layer.name = name;
    return name;
}

/**
 * Outputs all layers from the group layer and delete the group layer
 * @param {Layers} glayer_name - The group layer's name
 */
function ungroupLayer(glayer_name) {
    var glayer = getLayer(glayer_name);
    var clayers = glayer.layers;
    var clayers_names = [];
    for (i = 0; i < clayers.length; i++) {
        clayers_names.push(clayers[i].name);
    }

    for (i = 0; i < clayers_names.length; i++) {
        var clayer = glayer.layers.getByName(clayers_names[i]);
        clayer.move(glayer, ElementPlacement.PLACEBEFORE);
    }

    glayer.remove();
}

/**
 * Outputs all layers from the group layer and delete the group layer
 * @param {Layers} glayer - The group layer
 */
function ungroupGivenLayer(glayer) {
    var clayers = glayer.layers;
    var clayers_names = [];
    for (i = 0; i < clayers.length; i++) {
        clayers_names.push(clayers[i].name);
    }

    for (i = 0; i < clayers_names.length; i++) {
        var clayer = glayer.layers.getByName(clayers_names[i]);
        clayer.move(glayer, ElementPlacement.PLACEBEFORE);
    }

    glayer.remove();
}

/**
 * merge all sub-goup in the given group layer
 * @param {Layer} pgroup - the group layer 
 */
function mergeSubGroups(pgroup) {
    if (pgroup == undefined) {
        log.info("(mergeSubGroups) given pgroup is undefined");
        return;
    }

    app.activeDocument.activeLayer = pgroup;

    for (var i = 0; i < pgroup.layers.length; i++) {
        var layer = pgroup.layers[i];
        mergeLayer(layer);
    }
}

/**
 * merge all sub-layers in the given group layer
 * @param {Layer} pgroup - the group layer 
 * @returns {Layer} the merged sublayer
 */
function mergeSubLayers(pgroup) {
    if (pgroup == undefined) {
        log.info("(mergeSubGroups) given pgroup is undefined");
        return;
    }
	if (pgroup.layers.length == 0) {return;}
    app.activeDocument.activeLayer = pgroup;
    var topSubLayer = undefined;
    while (pgroup.layers.length > 1) {
        var layer = pgroup.layers[0];
        topSubLayer = mergeLayer(layer);
    }
    return getLayer(pgroup.layers[0].name, pgroup.layers);
}

/**
 * Return True if the given string start with one of the givens values
 * @param {String} str 
 * @param {String[]} values 
 * @returns {Boolean}
 */
function startswith(str, values) {
    for (var i = 0; i < values.length; i++) {
        if (str.indexOf(values[i]) == 0) {return true;}
    }
    return false;
}

/**
 * rename function for the BG COLOR group
 */
function getNewName_BGCOLO(name) {
    name = replaceInString(name, " ", "_");
    if (startswith(name, ["BG_", "FX_"])) {return name;}
    name = "BG_"+name;
    return name;
}

/**
 * rename function for the BG COLOR group
 */
function getNewName_FIELD(name) {
    name = replaceInString(name, " ", "_");
    
    if (startswith(name, ["FIELD_"])) {return name;}
    if (name.split("_").length == 2) {return "FIELD_"+name.split("_")[1];}

    return "FIELD_"+name;
}

/**
 * rename all child layers of a group with the guiven function
 * @param {*} pgroup - the parent group
 * @param {*} func - the rename function (must return the new layer name with the old one in entry)
 */
function renameGroupChildren(pgroup, func) {
    if (pgroup == undefined) {
        log.info("(renameGroupChildren) given pgroup is undefined");
        return;
    }

    for(var i = 0; i < pgroup.layers.length; i++) {
        var layer = pgroup.layers[i];
        layer.name = func(layer.name);
    }
}


/**
 *  TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO
 */


function cropSelection() {
    executeAction(charIDToTypeID("Crop"), new ActionDescriptor(), DialogModes.NO );
}

function deselectLayers() {
    var desc01 = new ActionDescriptor(); 
    var ref01 = new ActionReference(); 
    ref01.putEnumerated(charIDToTypeID('Lyr '), charIDToTypeID('Ordn'), charIDToTypeID('Trgt')); 
    desc01.putReference(charIDToTypeID('null'), ref01); 
    executeAction(stringIDToTypeID('selectNoLayers'), desc01, DialogModes.NO); 
}

/// create a new folder with document name
/// and returns the path
///
function solveOutputFolder(refDocument) {
    if (refDocument == null) {
        log.critical("(solveOutputFolder) no document ?");
        return;
    }

    log.debug("(solveOutputFolder) document name ? "+refDocument.name);

    var folderName = refDocument.name.replace(".psd", "");
    var folderPath = refDocument.path+"/"+folderName+"/";

    log.debug("(solveOutputFolder) output folder : "+folderPath);

    //create output folder
    var folder = Folder(folderPath);
    if(!folder.exists) {folder.create();}

    return folderPath;
}


function savePNG(fileName) {
    var pngOpts = new ExportOptionsSaveForWeb;
    pngOpts.format = SaveDocumentType.PNG;
    pngOpts.PNG8 = false; 
    pngOpts.transparency = true; 
    pngOpts.interlaced = false; 
    pngOpts.quality = 100;

    var path = fileName+".png";
    var file = new File(path);
    app.activeDocument.exportDocument(file, ExportType.SAVEFORWEB, pngOpts);
    log.debug("(savePNG) saved to file : "+path);
}

function searchAndMerge(layerName, owner) {
    var layer = searchLayer(layerName, owner);
    if(layer == null) {return null;}
    
    isVisible = layer.visible;

    layer = mergeLayer(layer);
    layer.visible = isVisible;
    return layer;
}

/**
 * Set the visibility for all layers in gLayer.layers
 * @param {Layer} gLayer - the parent group layer
 * @param {Boolean} visible - the visibility
 */
function setLayersVisibility(gLayer, visible) {
    for(var i = 0; i < gLayer.layers.length; i++) {
        gLayer.layers[i].visible = visible;
    }
}

/**
 * merge the given layer with the one below
 * @param {Layer} layer - the layer to merge
 * @returns the merged layer
 */
function mergeLayer(layer) {
    app.activeDocument.activeLayer = layer;
    mergeDown();
    return layer;
}

function searchAndHide(layerName, owner) {
    if(owner == undefined) {owner = app.activeDocument;}

    var layer = searchLayer(layerName, owner);
    if(layer != null) {layer.visible = false;}
}


function searchLayerAndDelete(layerName, owner) {
    if (owner == undefined) {owner = app.activeDocument;}

    var layer = searchLayer(layerName, owner);
    if (layer == undefined) {return false;}

    for (var i = 0; i < layer.layers.length; i++) {
        unlockLayer(layer.layers [i]);
    }

    layer.remove();
    return true;
}

function searchLayer(layerName, owner) {
    if (owner == undefined) {owner = app.activeDocument;}
    if (owner == undefined) {
        log.error("(searchLayer) undefined owner ? "+layerName);
        return null;
    }

    var layers = owner.layers;

    if (layers == undefined) {
        log.info("(searchLayer) "+layerName+" => no layers on "+owner.name);
        return null;
    }

    if (layers.length <= 0) {
        //log.info("(searchLayer) 0x layers on doc : "+owner.name);
        return null;
    }
    
    for (var i = 0; i < layers.length; i++) {
        var layer = layers[i];
        //log.info("(searchLayer) "+layer.name+" vs "+layerName);
        if(layer.name == layerName) {return layer;}
    }

    log.info("(searchLayer) couldn't find : "+layerName);
    return null;
}

function fetchNeighborLayers(layer) {
    var layers = [];
    var parent = layer.parent;

    log.debug("(fetchNeighborLayers) "+layer+" < "+parent);

    for (var i = 0; i < parent.layers.length; i++) {
        var layer = parent.layers[i];
        if (!layer.visible) continue;
        if (layer == layer) continue;

        layers.push(layer);
    }

    return layers;
}

function resizeCanvas(doc, padding) {
    doc.trim();
    //give room around
    doc.resizeCanvas(UnitValue(doc.width + padding,"px"),UnitValue(doc.height + padding,"px"));
}



function gatherAllEndLayers(base, hideEachLayer) {
    var deadEnds = [];

    log.debug("(gatherAllEndLayers) "+base.name);

    //if(win != null) {win.text = "gathering dead ends ("+deadEnds.length+")";}

    //hide everything
    if (hideEachLayer) {
        log.debug("(gatherAllEndLayers) hiding "+base.name);
        base.visible = false;
    }

    //don't add '![name]' layers
    if (base.name[0] == "!") {return;}

    if (isLayerDeadEnd(base)) {
        log.debug("(gatherAllEndLayers) added "+base.name);
        deadEnds.push(base);
        return;
    }

    var lys = base.layers;

    log.debug("(gatherAllEndLayers) "+base.name+" has "+lys.length+" children");

    for (var i = 0; i < lys.length; i++) {
        log.debug("(gatherAllEndLayers)   L ("+i+") "+base.name);
        gatherAllEndLayers(lys[i], hideEachLayer);
    }
    return deadEnds;
}

function gatherAllLayersFrom(base, onlyVisible) {
    var list = [];

    //folder !
    if (base.layers != null) {
        log.debug("(gatherAllLayersFrom) found folder "+base.name+" checking children ...");

        //each elements of folder
        for (var i = 0; i < base.layers.length; i++) {
            var result = gatherAllLayersFrom(base.layers[i], onlyVisible); //recursive

            //adding each found layers to original array
            for (var j = 0; j < result.length; j++) {
                list.push(result[j]);
            }
        }

        log.debug("(gatherAllLayersFrom)  ... folder "+base.name+" gave "+list.length+" layers");
        return list;
    }

    //do not include document and onlyVisible things 
    if ((base == app.activeDocument) || (onlyVisible && !base.visible)) {
        log.debug("(gatherAllLayersFrom)  ... skipping layer : "+base.name)
        return list;
    }

    log.debug("(gatherAllLayersFrom)  ... adding layer : "+base.name);
    list.push(base);
    return list;
}


// hide/show active layer and ALL parents
//
function setLayerVisible(layer, visible) {
    if (layer == null) {
        log.critical("(setLayerVisible) no layer ?");
        return;
    }

    layer.visible = visible;
    var parent = layer.parent;
    while (parent != null) {
        parent.visible = visible;
        parent = parent.parent;
    }
}

// return true : has no other layer in children
//
function isLayerDeadEnd(base) {
    //une layer qui a pas d'enfant est un calque
    if (base.layers == null) {return true;}

    //si on trouve une layer enfant qui n'en a pas elle même c'est qu'on est au bout
    //si un des enfants a des enfants c'est qu'on est pas au bout
    for (var i = 0; i < base.layers.length; i++) {
        if (base.layers[i].layers != null) {return false;}
    }
    return true;
}





// speed up : https://community.adobe.com/t5/photoshop-ecosystem-discussions/is-there-any-way-to-prevent-ui-updating-while-jsx-script-is-running-in-photoshop/td-p/10059472


// https://theiviaxx.github.io/photoshop-docs/Photoshop/ArtLayer.html


function saveConfo(original) {
    var outputPath = original.name.replace(".psd", "");
    outputPath += "_CONFO";
    outputPath = original.path + "/" + outputPath;
    with(original) saveAs(File(outputPath));
}


function saveConfoLine(original) {
    try {
        var outputPath = original.name.replace(".psd", "");
        outputPath = outputPath.split("_");

        var name = {"ep": undefined, "sh": undefined};

        for (var i = outputPath.length; i > 0; i--) {
            var part = outputPath[i-1];
            if (part.slice(0, 3) == "OPM") {name.ep = part;}
            if (part.slice(0, 2) == "SH") {name.sh = part;}
        }

        if (name.ep === undefined || name.sh === undefined) {
            alert("can't save as \" OPM_xxx_SHxxx.psd\" -> save as \"[name]_CONFO.psd\"\n(because of undefined ep or sh -> ep: "+name.ep+", sh: "+name.sh+")");
            saveConfo(original);
            return;
        }

        outputPath = "OPM_" + name.ep.slice(3) + "_" + name.sh;
        outputPath = original.path + "/" + outputPath;
        with(original) saveAs(File(outputPath));

    } catch (e) {
        alert("can't save as \" OPM_xxx_SHxxx.psd\" -> save as \"[name]_CONFO.psd\"\n(because of error: "+e+")");
        saveConfo(original);
    }
}


// duplicate active and make copy active
function duplicateActive() {
    var copy = app.activeDocument.duplicate();
    app.activeDocument = copy;
    
    //refresh();
    copy.activeLayer = copy.layers[0];
    //refresh();

    // https://community.adobe.com/t5/photoshop-ecosystem-discussions/activelayer-returns-a-layer-when-no-layer-is-selected/m-p/10090865
    // make first layer active
    try { activeDocument.activeLayer.link(activeDocument.activeLayer);} 
    catch(e) { /*happens when the background is a single layer*/ }

    //refresh();
    return copy;
}

/// search for BG.. layers and merge each
///
function mergeBGs(bgLayer) {
    //var bgLayer = searchLayer(baseLayerName);
    if (bgLayer == undefined) {
        log.info("(mergeBGs) "+baseLayerName+" => undefined");
        return;
    }

    app.activeDocument.activeLayer = bgLayer;

    for (var i = 0; i < bgLayer.layers.length; i++) {
        var layer = bgLayer.layers[i];

        var split = null;
        var splitChar = null;

        // start with "BG"
        if (layer.name.indexOf("_") > -1) {
            splitChar = "_";
        } else if(layer.name.indexOf(" ") > -1) {
            splitChar = ' ';
        } else {
            log.info("(mergeBGs) _ ? "+layer.name.indexOf("_"));
            log.info("(mergeBGs)   ? "+layer.name.indexOf(" "));
        }

        log.info("(mergeBGs) "+layer.name+" ? "+splitChar);

        // NOT split-able
        if (splitChar == null) {continue;}

        split = layer.name.split(splitChar);

        var prefix = split[0].toLowerCase();
        
        log.info("(mergeBGs) #"+i+"  > "+layer.name+"  > "+prefix);

        // NOT starting with "bg"
        if(prefix != "bg") {continue;}

        // flatten
        mergeLayer(layer);

        // rename (keep original naming)
        layer.name = "BG";
        for (var j = 1; j < split.length; j++) {
            layer.name += "_"+split[j];
        }

        log.info("(mergeBGs) #"+i+" > renamed   > "+layer.name);
    }
    return false;
}

function cropCartouche() {
    var docu = app.activeDocument;
    var crop = searchLayer(docu,"crop");

    if (crop == null && cartouche != null) {
        // GENERATE crop

        log.debug("(cropCartouche) "+cartouche);
        cartouche.translate(1,-1);

        // find negative space of cartouche
        docu.activeLayer = cartouche;
        selectVisibleLayer();
        docu.selection.invert();

        // https://theiviaxx.github.io/photoshop-docs/Photoshop/ArtLayer.html#artlayer
        crop = docu.artLayers.add();
        crop.name = "crop";

        docu.activeLayer = crop;

        var colorRef = new SolidColor();
        colorRef.rgb.red = 0;
        colorRef.rgb.green = 0;
        colorRef.rgb.blue = 0;

        docu.selection.fill(colorRef, ColorBlendMode.NORMAL, 100, false);

        cartouche.remove();
    }


    if (crop != null) {
        log.info("(cropCartouche) cropping");

        docu.activeLayer = crop;
        //selectVisibleLayer();
        magicWand(2,2);

        var bound = docu.selection.bounds;

        // and remove cropped pixel
        cropToSelection (bound[1], bound[0], bound[3], bound[2], false);

        crop.remove();
    }

}

function rasterize(layer, owner) {
    var aLayer = null;

    for (var i = 0; i < owner.layers.length; i++) {
        if(layer == owner.layers[i]) {aLayer = owner.artLayers[i];}
    }
    if (aLayer == null) {return aLayer;}
    
    aLayer.rasterize(RasterizeType.ENTIRELAYER);
    aLayer.visible = false;
    return aLayer;
}

function flatPersp(bgName) {
    var bgLayer = searchLayer(bgName);
    if(bgLayer == undefined) {return bgLayer;}

    // https://community.adobe.com/t5/photoshop-ecosystem-discussions/how-to-rasterize-smart-object-layers-with-scripting/td-p/11268099
    var layer = searchAndMerge("perspective", bgLayer);
    if(layer != null) {rasterize(layer, bgLayer);}
    
    layer = searchAndMerge("pers", bgLayer);
    if(layer != null) {rasterize(layer, bgLayer);}
    
    return bgLayer;
}





















/// MATRIX
///

function selectLayerBelow() {
    var desc = new ActionDescriptor();
    var ref = new ActionReference();
    ref.putEnumerated(charIDToTypeID("Lyr "), charIDToTypeID("Ordn"), charIDToTypeID("Bckw"));
    desc.putReference(charIDToTypeID("null"), ref);
    desc.putBoolean(charIDToTypeID("MkVs"), false);
    executeAction(charIDToTypeID("slct"), desc, DialogModes.NO);
}


// https://github.com/LeZuse/photoshop-scripts/blob/master/default/Flatten%20All%20Layer%20Effects.jsx

///////////////////////////////////////////////////////////////////////////////
// 
// Function: makeLayerBelow
// Usage: Creates a new layer below with the target layers name
// Input: targetName. the name of the layer we want to create a new layer below
// Return: <none>
///////////////////////////////////////////////////////////////////////////////
function makeLayerBelow(targetName) {
    try {
        var id829 = charIDToTypeID( "Mk  " );
        var desc169 = new ActionDescriptor();
        var id830 = charIDToTypeID( "null" );
        var ref105 = new ActionReference();
        var id831 = charIDToTypeID( "Lyr " );
        ref105.putClass( id831 );
        desc169.putReference( id830, ref105 );
        var id832 = stringIDToTypeID( "below" );
        desc169.putBoolean( id832, true );
        var id833 = charIDToTypeID( "Usng" );
        var desc170 = new ActionDescriptor();
        var id834 = charIDToTypeID( "Nm  " );
        desc170.putString( id834, targetName );
        var id835 = charIDToTypeID( "Lyr " );
        desc169.putObject( id833, id835, desc170 );
        executeAction( id829, desc169, DialogModes.NO );
    } catch(e) {return; /*do nothing*/}
}

///////////////////////////////////////////////////////////////////////////////
// Function: selectPreviousLayer
// Usage: Selects the layer above the selected layer, adding that layer to the current layer selection
// Input: <none> Must have an open document
// Return: <none>
///////////////////////////////////////////////////////////////////////////////
function selectPreviousLayer() {
    try {
        var idslct = charIDToTypeID( "slct" );
        var desc9 = new ActionDescriptor();
        var idnull = charIDToTypeID( "null" );
        var ref8 = new ActionReference();
        var idLyr = charIDToTypeID( "Lyr " );
        var idOrdn = charIDToTypeID( "Ordn" );
        var idFrwr = charIDToTypeID( "Frwr" );
        ref8.putEnumerated( idLyr, idOrdn, idFrwr );
        desc9.putReference( idnull, ref8 );
        var idselectionModifier = stringIDToTypeID( "selectionModifier" );
        var idselectionModifierType = stringIDToTypeID( "selectionModifierType" );
        var idaddToSelection = stringIDToTypeID( "addToSelection" );
        desc9.putEnumerated( idselectionModifier, idselectionModifierType, idaddToSelection );
        var idMkVs = charIDToTypeID( "MkVs" );
        desc9.putBoolean( idMkVs, false );
        executeAction( idslct, desc9, DialogModes.NO );
    } catch(e) {return; /*do nothing*/}
}

///////////////////////////////////////////////////////////////////////////////
// Function: mergeDown
// Usage: merges the currently selected layers into one layer. If only one layer is selected it merges the current layer down into the layer below
// Input: <none> Must have an open document
// Return: <none>
///////////////////////////////////////////////////////////////////////////////
/**
 * merges the currently selected layers into one layer. If only one layer is selected it merges the current layer down into the layer below
 */
function mergeDown() {
    try {
        var id828 = charIDToTypeID( "Mrg2" );
        var desc168 = new ActionDescriptor();
        executeAction( id828, desc168, DialogModes.NO );
    } catch(e) {return; /*do nothing*/}
}



function selectVisibleLayer() {
    var id1268 = charIDToTypeID( "setd" );
    var desc307 = new ActionDescriptor();
    var id1269 = charIDToTypeID( "null" );
    var ref257 = new ActionReference();
    var id1270 = charIDToTypeID( "Chnl" );
    var id1271 = charIDToTypeID( "fsel" );
    ref257.putProperty( id1270, id1271 );
    desc307.putReference( id1269, ref257 );
    var id1272 = charIDToTypeID( "T   " );
    var ref258 = new ActionReference();
    var id1273 = charIDToTypeID( "Chnl" );
    var id1274 = charIDToTypeID( "Chnl" );
    var id1275 = charIDToTypeID( "Trsp" );
    ref258.putEnumerated( id1273, id1274, id1275 );
    desc307.putReference( id1272, ref258 );
    executeAction( id1268, desc307, DialogModes.NO )
}

// return : array of layers (selected in editor)
//
function getSelectedLayers() {
    var idGrp = stringIDToTypeID( "groupLayersEvent" );
    var descGrp = new ActionDescriptor();
    var refGrp = new ActionReference();
    refGrp.putEnumerated(charIDToTypeID( "Lyr " ),charIDToTypeID( "Ordn" ),charIDToTypeID( "Trgt" ));
    descGrp.putReference(charIDToTypeID( "null" ), refGrp );
    executeAction( idGrp, descGrp, DialogModes.ALL );
    var resultLayers = new Array();
    for (var ix=0; ix<app.activeDocument.activeLayer.layers.length; ix++){resultLayers.push(app.activeDocument.activeLayer.layers[ix]);}
    var id8 = charIDToTypeID( "slct" );
    var desc5 = new ActionDescriptor();
    var id9 = charIDToTypeID( "null" );
    var ref2 = new ActionReference();
    var id10 = charIDToTypeID( "HstS" );
    var id11 = charIDToTypeID( "Ordn" );
    var id12 = charIDToTypeID( "Prvs" );
    ref2.putEnumerated( id10, id11, id12 );
    desc5.putReference( id9, ref2 );
    executeAction( id8, desc5, DialogModes.NO );
    return resultLayers;
}


// https://community.adobe.com/t5/photoshop-ecosystem-discussions/script-with-magic-wand-tool/m-p/5446467
function magicWand(x,y,tolerance,anit_alias,contiguous,sampleAll) {
    if (arguments.length < 2) {return;}// make sure have x,y - other arguments are optional
    if (undefined == tolerance) {var tolerance = 100;}// set defaults of optional arguments
    if (undefined == anit_alias) {var anit_alias = true;}
    if (undefined == contiguous) {var contiguous = true;}
    if (undefined == sampleAll) {var sampleAll = false;}

    var desc = new ActionDescriptor();

    var ref = new ActionReference();

    ref.putProperty( charIDToTypeID('Chnl'), charIDToTypeID('fsel') );

    desc.putReference( charIDToTypeID('null'), ref );

    var positionDesc = new ActionDescriptor();

    positionDesc.putUnitDouble( charIDToTypeID('Hrzn'), charIDToTypeID('#Rlt'), x );// in pixels

    positionDesc.putUnitDouble( charIDToTypeID('Vrtc'), charIDToTypeID('#Rlt'), y );

    desc.putObject( charIDToTypeID('T   '), charIDToTypeID('Pnt '), positionDesc );

    desc.putInteger( charIDToTypeID('Tlrn'), tolerance);

    desc.putBoolean( charIDToTypeID('Mrgd'), sampleAll );

    if(!contiguous) {desc.putBoolean( charIDToTypeID( 'Cntg' ), false );}

    desc.putBoolean( charIDToTypeID('AntA'), anit_alias );

    executeAction( charIDToTypeID('setd'), desc, DialogModes.NO );
}


/**
 * crop To Selection
 * 
 * @param {Number} top 
 * @param {Number} left 
 * @param {Number} bottom 
 * @param {Number} right 
 */
function cropToSelection(top, left, bottom, right) {
    var idCrop = charIDToTypeID( "Crop" );
    var desc11 = new ActionDescriptor();
    var idT = charIDToTypeID( "T   " );
    var desc12 = new ActionDescriptor();
    var idTop = charIDToTypeID( "Top " );
    var idPxl = charIDToTypeID( "#Pxl" );
    desc12.putUnitDouble( idTop, idPxl, top );
    var idLeft = charIDToTypeID( "Left" );
    var idPxl = charIDToTypeID( "#Pxl" );
    desc12.putUnitDouble( idLeft, idPxl,left );
    var idBtom = charIDToTypeID( "Btom" );
    var idPxl = charIDToTypeID( "#Pxl" );
    desc12.putUnitDouble( idBtom, idPxl, bottom );
    var idRght = charIDToTypeID( "Rght" );
    var idPxl = charIDToTypeID( "#Pxl" );
    desc12.putUnitDouble( idRght, idPxl, right );
    var idRctn = charIDToTypeID( "Rctn" );
    desc11.putObject( idT, idRctn, desc12 );
    var idAngl = charIDToTypeID( "Angl" );
    var idAng = charIDToTypeID( "#Ang" );
    desc11.putUnitDouble( idAngl, idAng, 0.000000 );
    var idDlt = charIDToTypeID( "Dlt " );
    desc11.putBoolean( idDlt, false );
    var idcropAspectRatioModeKey = stringIDToTypeID( "cropAspectRatioModeKey" );
    var idcropAspectRatioModeClass = stringIDToTypeID( "cropAspectRatioModeClass" );
    var idtargetSize = stringIDToTypeID( "targetSize" );
    desc11.putEnumerated( idcropAspectRatioModeKey, idcropAspectRatioModeClass, idtargetSize );
    executeAction( idCrop, desc11, DialogModes.NO );
}


/**
 * crop To Selection Pixels, Courtesy of Chuck Uebele
 * 
 * @param {Number} top 
 * @param {Number} left 
 * @param {Number} bottom 
 * @param {Number} right 
 * @param {Boolean} retainPixels 
 */
function cropToSelectionPixels(top, left, bottom, right, retainPixels) {

    var origRuler = app.preferences.rulerUnits;
    app.preferences.rulerUnits = Units.PIXELS;

    var idCrop = charIDToTypeID("Crop");
    var desc11 = new ActionDescriptor();
    var idT = charIDToTypeID("T   ");
    var desc12 = new ActionDescriptor();
    var idTop = charIDToTypeID("Top ");
    var idPxl = charIDToTypeID("#Pxl");
    desc12.putUnitDouble(idTop, idPxl, top);
    var idLeft = charIDToTypeID("Left");
    var idPxl = charIDToTypeID("#Pxl");
    desc12.putUnitDouble(idLeft, idPxl, left);
    var idBtom = charIDToTypeID("Btom");
    var idPxl = charIDToTypeID("#Pxl");
    desc12.putUnitDouble(idBtom, idPxl, bottom);
    var idRght = charIDToTypeID("Rght");
    var idPxl = charIDToTypeID("#Pxl");
    desc12.putUnitDouble(idRght, idPxl, right);
    var idRctn = charIDToTypeID("Rctn");
    desc11.putObject(idT, idRctn, desc12);
    var idAngl = charIDToTypeID("Angl");
    var idAng = charIDToTypeID("#Ang");
    desc11.putUnitDouble(idAngl, idAng, 0.000000);
    var idDlt = charIDToTypeID("Dlt ");
    desc11.putBoolean(idDlt, retainPixels); // delete cropped pixels = true | false
    var idcropAspectRatioModeKey = stringIDToTypeID("cropAspectRatioModeKey");
    var idcropAspectRatioModeClass = stringIDToTypeID("cropAspectRatioModeClass");
    var idtargetSize = stringIDToTypeID("targetSize");
    desc11.putEnumerated(idcropAspectRatioModeKey, idcropAspectRatioModeClass, idtargetSize);
    executeAction(idCrop, desc11, DialogModes.NO);

    app.preferences.rulerUnits = origRuler;
}