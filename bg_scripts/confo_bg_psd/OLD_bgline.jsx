var scriptVersion = "1.2 | 2024-03-07";
var sourcePath = "O:\\_SCRIPTS\\opm-tools\\BG_scripts\\confo_bg_psd\\bgline.jsx";

#include "packages.jsx";

// setup stuff
var defaultRulerUnits = preferences.rulerUnits;
preferences.rulerUnits = Units.PIXELS;


function extractFieldLayers(fLayer) 
{
    var isMaster = false;
    var layers = fLayer.layers;

    // test if is a field master case (multiples subGroups)
    for (var i = 0; i < layers.length; i++) {
        var layer = layers[i];
        layer = getLayer(layer.name, layers);
        if (layer.layers == undefined) {continue;}
        if (layer.layers.length < 1) {continue;} 
        isMaster = true;
        break;
    }

    if (isMaster) {
        // delete all non group children ?
        mergeSubGroups(fLayer);
        renameGroupChildren(fLayer, getNewName_FIELD);
        ungroupLayer("FIELD");
        return;
    }

    topSubLayer = mergeSubLayers(fLayer);
    var shNbr = getShotNumber();
    topSubLayer.name = "FIELD_"+shNbr;
    ungroupLayer("FIELD");
}


////////
// app
////////
function main(autoClose)
{
    var original = app.activeDocument; // keep ref of original
    var doc = duplicateActiveDocument();

    removeCroppedPixel(doc);
    

    var bgLineName = findRightLayerName(["BG LINE", "BG_LINE"]);

    var topLayers = app.activeDocument.layers;
    var toKeep = ["FIELD", "CH", "STB", bgLineName];
    deleteLayers(topLayers, toKeep);

    unlockAllLayers();

    var bg_line = getLayer(bgLineName);
    mergeSubGroups(bg_line);
    ungroupLayer(bgLineName);

    var field = getLayer("FIELD");
    extractFieldLayers(field);

    // save _CONFO
    saveConfo(original);
    
    if(autoClose) {
        original.close(SaveOptions.DONOTSAVECHANGES);
        doc.close(SaveOptions.DONOTSAVECHANGES);
    }
}

function isUpToDate(versionSelf, sourcePath) {
    try {
        var sourceFile = new File(sourcePath);
        if (!sourceFile.exists) {return;}
        sourceFile.open("r");
        var content = sourceFile.read().split("\n");
        var sourceVersion = false;

        for (var i = 0; i < content.length; i++) {
            if (startswith(content[i], ["var version = \""])) {
                sourceVersion = content[i].split("\"")[1];
                sourceVersion = Number(sourceVersion.split(" | ")[0]);
                break;
            }
        }

        if (!sourceVersion) {return;}

        versionSelf = Number(versionSelf.split(" | ")[0]);

        if (sourceVersion > versionSelf) {
            alert("the file need an update (current version: "+versionSelf+", new version: "+sourceVersion+")\nYou can find the file up to date here: "+sourcePath);
        }
    }
    catch (err) {
        log.info("(isUpToDate) check update failed because of :\n"+err);
        return;
    }
}

isUpToDate(scriptVersion, sourcePath);

main(true);